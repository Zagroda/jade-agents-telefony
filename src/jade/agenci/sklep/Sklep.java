package jade.agenci.sklep;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import java.net.URL;
import java.net.URLConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Natalia
 */
public class Sklep extends Agent {

  // inicjalizacja agenta
  protected void setup() {
    addBehaviour(new ZnajdzTelefon());
  }

  // Zachowania agenta
  public class ZnajdzTelefon extends CyclicBehaviour {
    
    String listaTelefonow = "";

    public void action() {
      ACLMessage msg = receive();
      
      if (msg == null) {
        block();
      } else {
        String zapytanie = msg.getContent();
        System.out.println("--- zapytanie => " + zapytanie);
        
        try {

          URL url = new URL("http://gsmshop.codenode.pl/WebApi/bazaTelefonow/" + zapytanie);
          URLConnection connection = url.openConnection();
          connection.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
          DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
          DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
          Document doc = dBuilder.parse(connection.getInputStream());
          doc.getDocumentElement().normalize();

          NodeList nodes = doc.getElementsByTagName("telefon");
          
          System.out.println("== Baza danych telefonów została wczytana...");

          for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {

              Element element = (Element) node;
              
              listaTelefonow += "\nMarka: " + getValue("marka", element) +
                "\nModel: " + getValue("model", element) +
                "\nKolor: " + getValue("kolor", element) +
                "\nWielkość wyświetlacza: " + getValue("wielkoscWyswietlacza", element) +
                "\nDodatkowe funkcje: " + getValue("dotatkoweFunkcje", element) +
                "\nsystemOperacyjny: " + getValue("systemOperacyjny", element) +
                "\n---";

            }

          }
          
          if (listaTelefonow == "") {
            listaTelefonow = "Brak wyszukiwanego telefonu...";
          }
          
          System.out.println(listaTelefonow);
          
          ACLMessage replyMsg = new ACLMessage(ACLMessage.INFORM);
          replyMsg.addReceiver(new AID("Klient", AID.ISLOCALNAME));
          replyMsg.setLanguage("Polish");
          replyMsg.setContent(listaTelefonow);
          send(replyMsg);

        } catch (Exception ex) {
          ACLMessage replyMsg = new ACLMessage(ACLMessage.INFORM);
          replyMsg.addReceiver(new AID("Klient", AID.ISLOCALNAME));
          replyMsg.setLanguage("Polish");
          replyMsg.setContent("Problem z połączeniem...");
          send(replyMsg);
          
          ex.printStackTrace();
        }
      }
    }
    
    private String getValue(String tag, Element element) {
      NodeList nodes = element.getElementsByTagName(tag).item(0).getChildNodes();
      Node node = (Node) nodes.item(0);
      return node.getNodeValue();
    }
  }

}
