package jade.agenci.sklep;

/**
 *
 * @author Natalia
 */
public class FormatkaTelefony extends javax.swing.JFrame {

  private Klient agentKlienta;
  private String zapytanie;

  // Inicjacja FormatkaTelefony
  public FormatkaTelefony(Klient k) {
    agentKlienta = k;
    initComponents();
  }
  
  public void showGui() {
    super.setVisible(true);
  }

  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    jScrollPane1 = new javax.swing.JScrollPane();
    poleMarka = new javax.swing.JTextPane();
    jLabel1 = new javax.swing.JLabel();
    jLabel2 = new javax.swing.JLabel();
    jLabel3 = new javax.swing.JLabel();
    jLabel4 = new javax.swing.JLabel();
    jLabel5 = new javax.swing.JLabel();
    jScrollPane2 = new javax.swing.JScrollPane();
    poleModel = new javax.swing.JTextPane();
    poleKolor = new javax.swing.JComboBox();
    poleWyswietlacz = new javax.swing.JComboBox();
    poleWiFi = new javax.swing.JCheckBox();
    poleGps = new javax.swing.JCheckBox();
    poleEkranDotykowy = new javax.swing.JCheckBox();
    poleSiec3g = new javax.swing.JCheckBox();
    poleSiecLte = new javax.swing.JCheckBox();
    poleSystem = new javax.swing.JComboBox();
    jLabel6 = new javax.swing.JLabel();
    buttonSzukaj = new javax.swing.JButton();
    jScrollPane3 = new javax.swing.JScrollPane();
    listaTelefonow = new javax.swing.JTextArea();

    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

    jScrollPane1.setViewportView(poleMarka);

    jLabel1.setText("Marka");

    jLabel2.setText("Model");
    jLabel2.setToolTipText("");

    jLabel3.setText("Kolor");

    jLabel4.setText("Wielkość wyświetlacza \"");

    jLabel5.setText("Dodatkowe funkcje");

    jScrollPane2.setViewportView(poleModel);

    poleKolor.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "biały", "czarny", "srebrny", "złoty", "inny" }));

    poleWyswietlacz.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "2.1 - 2.5", "2.6 - 3.0", "3.6 - 4.0", "4.1 - 4.5", "4.6 - 5.0", "Ponad 5.0" }));
    poleWyswietlacz.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        poleWyswietlaczActionPerformed(evt);
      }
    });

    poleWiFi.setText("Wi-fi");
    poleWiFi.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        poleWiFiActionPerformed(evt);
      }
    });

    poleGps.setText("GPS");

    poleEkranDotykowy.setText("Ekran dotykowy");
    poleEkranDotykowy.setToolTipText("");

    poleSiec3g.setText("3G");
    poleSiec3g.setToolTipText("");
    poleSiec3g.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        poleSiec3gActionPerformed(evt);
      }
    });

    poleSiecLte.setText("LTE");

    poleSystem.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Android", "iOS", "Symbian", "Windows Phone", "Inny" }));

    jLabel6.setText("System operacyjny");

    buttonSzukaj.setText("Wyszukaj telefon");
    buttonSzukaj.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        buttonSzukajActionPerformed(evt);
      }
    });

    listaTelefonow.setColumns(20);
    listaTelefonow.setRows(5);
    listaTelefonow.setText("Wyszukaj telefon...");
    jScrollPane3.setViewportView(listaTelefonow);

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addGap(20, 20, 20)
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(jLabel1)
              .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
              .addComponent(jLabel2))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
              .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel4))
              .addGroup(layout.createSequentialGroup()
                .addComponent(poleKolor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(poleWyswietlacz, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addGap(18, 18, 18)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                  .addComponent(poleWiFi)
                  .addComponent(poleSiec3g)
                  .addComponent(poleEkranDotykowy)
                  .addComponent(poleGps)
                  .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                  .addComponent(poleSystem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addComponent(jLabel6))
                .addGap(81, 81, 81))
              .addGroup(layout.createSequentialGroup()
                .addComponent(poleSiecLte)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
          .addGroup(layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 820, javax.swing.GroupLayout.PREFERRED_SIZE)
              .addComponent(buttonSzukaj))
            .addGap(0, 0, Short.MAX_VALUE))))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addGroup(layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                  .addComponent(jLabel1)
                  .addComponent(jLabel4)
                  .addComponent(jLabel5)
                  .addComponent(jLabel6)))
              .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
              .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(poleKolor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(poleWyswietlacz, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(poleWiFi)
                .addComponent(poleSystem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
          .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
            .addComponent(jLabel2)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
        .addComponent(poleGps)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
        .addComponent(poleEkranDotykowy)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
        .addComponent(poleSiec3g)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
        .addComponent(poleSiecLte)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        .addComponent(buttonSzukaj)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addGap(17, 17, 17))
    );

    pack();
  }// </editor-fold>//GEN-END:initComponents

    private void poleWiFiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_poleWiFiActionPerformed

    }//GEN-LAST:event_poleWiFiActionPerformed

    private void poleSiec3gActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_poleSiec3gActionPerformed

    }//GEN-LAST:event_poleSiec3gActionPerformed

    private void poleWyswietlaczActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_poleWyswietlaczActionPerformed

    }//GEN-LAST:event_poleWyswietlaczActionPerformed

  private void buttonSzukajActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSzukajActionPerformed
    String marka = poleMarka.getText().trim();
    String model = poleModel.getText().trim();
    String kolor = poleKolor.getSelectedItem().toString();
    String wyswietlacz = poleWyswietlacz.getSelectedItem().toString();
    Boolean wifi = poleWiFi.isSelected();
    Boolean gps = poleGps.isSelected();
    Boolean ekranDotykowy = poleEkranDotykowy.isSelected();
    Boolean siec3g = poleSiec3g.isSelected();
    Boolean siecLte = poleSiecLte.isSelected();
    String system = poleSystem.getSelectedItem().toString();
    
    if (kolor == "biały") {
      kolor = "bia%C5%82y";
    }
    
    String zapytanie = marka + "," + model + "," + kolor + "," + wyswietlacz + "," + wifi + "," + gps + "," + ekranDotykowy + "," + siec3g + "," + siecLte + "," + system;
    
    agentKlienta.wyslijZapytanie(zapytanie);
  }//GEN-LAST:event_buttonSzukajActionPerformed

  public static void main(String args[]) {
    try {
      for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
        if ("Nimbus".equals(info.getName())) {
          javax.swing.UIManager.setLookAndFeel(info.getClassName());
          break;
        }
      }
    } catch (ClassNotFoundException ex) {
      java.util.logging.Logger.getLogger(FormatkaTelefony.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (InstantiationException ex) {
      java.util.logging.Logger.getLogger(FormatkaTelefony.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (IllegalAccessException ex) {
      java.util.logging.Logger.getLogger(FormatkaTelefony.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (javax.swing.UnsupportedLookAndFeelException ex) {
      java.util.logging.Logger.getLogger(FormatkaTelefony.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    }
    //</editor-fold>

    /* Create and display the form */
    java.awt.EventQueue.invokeLater(new Runnable() {
      public void run() {
//        new FormatkaTelefony().setVisible(true);
      }
    });
  }
  
  public void zaktualizujListeTelefonow(String lista) {
    listaTelefonow.setText(lista);
  }

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JButton buttonSzukaj;
  private javax.swing.JLabel jLabel1;
  private javax.swing.JLabel jLabel2;
  private javax.swing.JLabel jLabel3;
  private javax.swing.JLabel jLabel4;
  private javax.swing.JLabel jLabel5;
  private javax.swing.JLabel jLabel6;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JScrollPane jScrollPane2;
  private javax.swing.JScrollPane jScrollPane3;
  private javax.swing.JTextArea listaTelefonow;
  private javax.swing.JCheckBox poleEkranDotykowy;
  private javax.swing.JCheckBox poleGps;
  private javax.swing.JComboBox poleKolor;
  private javax.swing.JTextPane poleMarka;
  private javax.swing.JTextPane poleModel;
  private javax.swing.JCheckBox poleSiec3g;
  private javax.swing.JCheckBox poleSiecLte;
  private javax.swing.JComboBox poleSystem;
  private javax.swing.JCheckBox poleWiFi;
  private javax.swing.JComboBox poleWyswietlacz;
  // End of variables declaration//GEN-END:variables
}
