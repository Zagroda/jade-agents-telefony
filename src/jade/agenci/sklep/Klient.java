package jade.agenci.sklep;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;

/**
 *
 * @author Natalia
 */
public class Klient extends Agent {
  private FormatkaTelefony uiKlienta;
  
  // inicjalizacja agenta
  protected void setup() {
    // wyświetl formatkę wyszukiwania dla klienta
    uiKlienta = new FormatkaTelefony(this);
    uiKlienta.showGui();
    
    addBehaviour(new aktualizujListeTelefonow());
  }
  
  public void wyslijZapytanie(final String query) {
    ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
    msg.addReceiver(new AID("Sklep", AID.ISLOCALNAME));
    msg.setLanguage("Polish");
    msg.setContent(query);
    send(msg);
  }
  
  // Zachowania agenta
  public class aktualizujListeTelefonow extends CyclicBehaviour {
    public void action() {
      ACLMessage msg = receive();
      
      if (msg == null) {
        block();
      } else {
        uiKlienta.zaktualizujListeTelefonow(msg.getContent());
      }
    }
  }
  
}
