import jade.Boot;

/**
 *
 * @author Natalia
 */
public class Main {

  Boot AgentPlatform;

  public Main() {}
  
  public static void main(String args[]) {
    // uruchamianie menagera agentów
    String[] param = new String[1];
    param[0] = "-gui";
    Boot.main(param);

    // dodawanie agenta z baza telefonow
    String agentSklep[] = {
      "-host",
      "localhost",
      "-port",
      "1099",
      "-container-name",
      "Sklep",
      "-container",
      "Sklep:jade.agenci.sklep.Sklep"
    };
    Boot.main(agentSklep);
    System.out.println("== Agent --> Sklep został stworzony");
    
    // dodawanie agenta klient
    String agentKlient[] = {
      "-host",
      "localhost",
      "-port",
      "1099",
      "-container-name",
      "Klient",
      "-container",
      "Klient:jade.agenci.sklep.Klient"
    };
    Boot.main(agentKlient);
    System.out.println("== Agent --> Klient został stworzony");
  }
}
