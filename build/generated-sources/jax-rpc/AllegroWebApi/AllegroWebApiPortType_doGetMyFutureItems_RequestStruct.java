// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class AllegroWebApiPortType_doGetMyFutureItems_RequestStruct {
    protected java.lang.String sessionId;
    protected AllegroWebApi.SortOptionsStruct sortOptions;
    protected AllegroWebApi.FutureFilterOptionsStruct filterOptions;
    protected int categoryId;
    protected long[] itemIds;
    protected int pageSize;
    protected int pageNumber;
    
    public AllegroWebApiPortType_doGetMyFutureItems_RequestStruct() {
    }
    
    public AllegroWebApiPortType_doGetMyFutureItems_RequestStruct(java.lang.String sessionId, AllegroWebApi.SortOptionsStruct sortOptions, AllegroWebApi.FutureFilterOptionsStruct filterOptions, int categoryId, long[] itemIds, int pageSize, int pageNumber) {
        this.sessionId = sessionId;
        this.sortOptions = sortOptions;
        this.filterOptions = filterOptions;
        this.categoryId = categoryId;
        this.itemIds = itemIds;
        this.pageSize = pageSize;
        this.pageNumber = pageNumber;
    }
    
    public java.lang.String getSessionId() {
        return sessionId;
    }
    
    public void setSessionId(java.lang.String sessionId) {
        this.sessionId = sessionId;
    }
    
    public AllegroWebApi.SortOptionsStruct getSortOptions() {
        return sortOptions;
    }
    
    public void setSortOptions(AllegroWebApi.SortOptionsStruct sortOptions) {
        this.sortOptions = sortOptions;
    }
    
    public AllegroWebApi.FutureFilterOptionsStruct getFilterOptions() {
        return filterOptions;
    }
    
    public void setFilterOptions(AllegroWebApi.FutureFilterOptionsStruct filterOptions) {
        this.filterOptions = filterOptions;
    }
    
    public int getCategoryId() {
        return categoryId;
    }
    
    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }
    
    public long[] getItemIds() {
        return itemIds;
    }
    
    public void setItemIds(long[] itemIds) {
        this.itemIds = itemIds;
    }
    
    public int getPageSize() {
        return pageSize;
    }
    
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
    
    public int getPageNumber() {
        return pageNumber;
    }
    
    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }
}
