// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;

import com.sun.xml.rpc.encoding.*;
import com.sun.xml.rpc.encoding.literal.DetailFragmentDeserializer;
import com.sun.xml.rpc.encoding.simpletype.*;
import com.sun.xml.rpc.encoding.soap.SOAPConstants;
import com.sun.xml.rpc.encoding.soap.SOAP12Constants;
import com.sun.xml.rpc.streaming.*;
import com.sun.xml.rpc.wsdl.document.schema.SchemaConstants;
import javax.xml.namespace.QName;

public class PaymentsInfoStruct_SOAPSerializer extends ObjectSerializerBase implements Initializable {
    private static final javax.xml.namespace.QName ns1_payments$2d$balance_QNAME = new QName("", "payments-balance");
    private static final javax.xml.namespace.QName ns2_float_TYPE_QNAME = SchemaConstants.QNAME_TYPE_FLOAT;
    private CombinedSerializer ns2_myns2__float__float_Float_Serializer;
    private static final javax.xml.namespace.QName ns1_payments$2d$bank$2d$account_QNAME = new QName("", "payments-bank-account");
    private static final javax.xml.namespace.QName ns2_string_TYPE_QNAME = SchemaConstants.QNAME_TYPE_STRING;
    private CombinedSerializer ns2_myns2_string__java_lang_String_String_Serializer;
    private static final javax.xml.namespace.QName ns1_payments$2d$user$2d$data_QNAME = new QName("", "payments-user-data");
    private static final javax.xml.namespace.QName ns3_PaymentsUserDataStruct_TYPE_QNAME = new QName("urn:AllegroWebApi", "PaymentsUserDataStruct");
    private CombinedSerializer ns3_myPaymentsUserDataStruct_SOAPSerializer;
    private static final javax.xml.namespace.QName ns1_payments$2d$payout_QNAME = new QName("", "payments-payout");
    private static final javax.xml.namespace.QName ns3_PaymentsPayoutStruct_TYPE_QNAME = new QName("urn:AllegroWebApi", "PaymentsPayoutStruct");
    private CombinedSerializer ns3_myPaymentsPayoutStruct_SOAPSerializer;
    private static final javax.xml.namespace.QName ns1_payments$2d$notifications_QNAME = new QName("", "payments-notifications");
    private static final javax.xml.namespace.QName ns2_int_TYPE_QNAME = SchemaConstants.QNAME_TYPE_INT;
    private CombinedSerializer ns2_myns2__int__int_Int_Serializer;
    private static final int myPAYMENTSBALANCE_INDEX = 0;
    private static final int myPAYMENTSBANKACCOUNT_INDEX = 1;
    private static final int myPAYMENTSUSERDATA_INDEX = 2;
    private static final int myPAYMENTSPAYOUT_INDEX = 3;
    private static final int myPAYMENTSNOTIFICATIONS_INDEX = 4;
    
    public PaymentsInfoStruct_SOAPSerializer(QName type, boolean encodeType, boolean isNullable, String encodingStyle) {
        super(type, encodeType, isNullable, encodingStyle);
    }
    
    public void initialize(InternalTypeMappingRegistry registry) throws java.lang.Exception {
        ns2_myns2__float__float_Float_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, float.class, ns2_float_TYPE_QNAME);
        ns2_myns2_string__java_lang_String_String_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, java.lang.String.class, ns2_string_TYPE_QNAME);
        ns3_myPaymentsUserDataStruct_SOAPSerializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, AllegroWebApi.PaymentsUserDataStruct.class, ns3_PaymentsUserDataStruct_TYPE_QNAME);
        ns3_myPaymentsPayoutStruct_SOAPSerializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, AllegroWebApi.PaymentsPayoutStruct.class, ns3_PaymentsPayoutStruct_TYPE_QNAME);
        ns2_myns2__int__int_Int_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, int.class, ns2_int_TYPE_QNAME);
    }
    
    public java.lang.Object doDeserialize(SOAPDeserializationState state, XMLReader reader,
        SOAPDeserializationContext context) throws java.lang.Exception {
        AllegroWebApi.PaymentsInfoStruct instance = new AllegroWebApi.PaymentsInfoStruct();
        AllegroWebApi.PaymentsInfoStruct_SOAPBuilder builder = null;
        java.lang.Object member;
        boolean isComplete = true;
        javax.xml.namespace.QName elementName;
        
        reader.nextElementContent();
        for (int i=0; i<5; i++) {
            elementName = reader.getName();
            if (reader.getState() == XMLReader.END) {
                break;
            }
            if (elementName.equals(ns1_payments$2d$balance_QNAME)) {
                member = ns2_myns2__float__float_Float_Serializer.deserialize(ns1_payments$2d$balance_QNAME, reader, context);
                instance.setPaymentsBalance(((Float)member).floatValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_payments$2d$bank$2d$account_QNAME)) {
                member = ns2_myns2_string__java_lang_String_String_Serializer.deserialize(ns1_payments$2d$bank$2d$account_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.PaymentsInfoStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myPAYMENTSBANKACCOUNT_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setPaymentsBankAccount((java.lang.String)member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_payments$2d$user$2d$data_QNAME)) {
                member = ns3_myPaymentsUserDataStruct_SOAPSerializer.deserialize(ns1_payments$2d$user$2d$data_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.PaymentsInfoStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myPAYMENTSUSERDATA_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setPaymentsUserData((AllegroWebApi.PaymentsUserDataStruct)member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_payments$2d$payout_QNAME)) {
                member = ns3_myPaymentsPayoutStruct_SOAPSerializer.deserialize(ns1_payments$2d$payout_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.PaymentsInfoStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myPAYMENTSPAYOUT_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setPaymentsPayout((AllegroWebApi.PaymentsPayoutStruct)member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_payments$2d$notifications_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_payments$2d$notifications_QNAME, reader, context);
                instance.setPaymentsNotifications(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            } else {
                throw new DeserializationException("soap.unexpectedElementName", new Object[] {ns1_payments$2d$notifications_QNAME, elementName});
            }
        }
        
        XMLReaderUtil.verifyReaderState(reader, XMLReader.END);
        return (isComplete ? (java.lang.Object)instance : (java.lang.Object)state);
    }
    
    public void doSerializeAttributes(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws java.lang.Exception {
        AllegroWebApi.PaymentsInfoStruct instance = (AllegroWebApi.PaymentsInfoStruct)obj;
        
    }
    
    public void doSerializeInstance(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws java.lang.Exception {
        AllegroWebApi.PaymentsInfoStruct instance = (AllegroWebApi.PaymentsInfoStruct)obj;
        
        ns2_myns2__float__float_Float_Serializer.serialize(new Float(instance.getPaymentsBalance()), ns1_payments$2d$balance_QNAME, null, writer, context);
        ns2_myns2_string__java_lang_String_String_Serializer.serialize(instance.getPaymentsBankAccount(), ns1_payments$2d$bank$2d$account_QNAME, null, writer, context);
        ns3_myPaymentsUserDataStruct_SOAPSerializer.serialize(instance.getPaymentsUserData(), ns1_payments$2d$user$2d$data_QNAME, null, writer, context);
        ns3_myPaymentsPayoutStruct_SOAPSerializer.serialize(instance.getPaymentsPayout(), ns1_payments$2d$payout_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getPaymentsNotifications()), ns1_payments$2d$notifications_QNAME, null, writer, context);
    }
}
