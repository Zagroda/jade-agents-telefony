// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;

import com.sun.xml.rpc.encoding.*;
import com.sun.xml.rpc.util.exception.LocalizableExceptionAdapter;

public class UserInfoStruct_SOAPBuilder implements SOAPInstanceBuilder {
    private AllegroWebApi.UserInfoStruct _instance;
    private int userId;
    private java.lang.String userLogin;
    private int userRating;
    private int userIcons;
    private int userCountry;
    private static final int myUSERID_INDEX = 0;
    private static final int myUSERLOGIN_INDEX = 1;
    private static final int myUSERRATING_INDEX = 2;
    private static final int myUSERICONS_INDEX = 3;
    private static final int myUSERCOUNTRY_INDEX = 4;
    
    public UserInfoStruct_SOAPBuilder() {
    }
    
    public void setUserId(int userId) {
        this.userId = userId;
    }
    
    public void setUserLogin(java.lang.String userLogin) {
        this.userLogin = userLogin;
    }
    
    public void setUserRating(int userRating) {
        this.userRating = userRating;
    }
    
    public void setUserIcons(int userIcons) {
        this.userIcons = userIcons;
    }
    
    public void setUserCountry(int userCountry) {
        this.userCountry = userCountry;
    }
    
    public int memberGateType(int memberIndex) {
        switch (memberIndex) {
            case myUSERLOGIN_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            default:
                throw new IllegalArgumentException();
        }
    }
    
    public void construct() {
    }
    
    public void setMember(int index, java.lang.Object memberValue) {
        try {
            switch(index) {
                case myUSERLOGIN_INDEX:
                    _instance.setUserLogin((java.lang.String)memberValue);
                    break;
                default:
                    throw new java.lang.IllegalArgumentException();
            }
        }
        catch (java.lang.RuntimeException e) {
            throw e;
        }
        catch (java.lang.Exception e) {
            throw new DeserializationException(new LocalizableExceptionAdapter(e));
        }
    }
    
    public void initialize() {
    }
    
    public void setInstance(java.lang.Object instance) {
        _instance = (AllegroWebApi.UserInfoStruct)instance;
    }
    
    public java.lang.Object getInstance() {
        return _instance;
    }
}
