// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;

import com.sun.xml.rpc.encoding.*;
import com.sun.xml.rpc.encoding.literal.DetailFragmentDeserializer;
import com.sun.xml.rpc.encoding.simpletype.*;
import com.sun.xml.rpc.encoding.soap.SOAPConstants;
import com.sun.xml.rpc.encoding.soap.SOAP12Constants;
import com.sun.xml.rpc.streaming.*;
import com.sun.xml.rpc.wsdl.document.schema.SchemaConstants;
import javax.xml.namespace.QName;

public class FilterOptionsType_SOAPSerializer extends ObjectSerializerBase implements Initializable {
    private static final javax.xml.namespace.QName ns1_filter$2d$id_QNAME = new QName("", "filter-id");
    private static final javax.xml.namespace.QName ns2_string_TYPE_QNAME = SchemaConstants.QNAME_TYPE_STRING;
    private CombinedSerializer ns2_myns2_string__java_lang_String_String_Serializer;
    private static final javax.xml.namespace.QName ns1_filter$2d$value$2d$id_QNAME = new QName("", "filter-value-id");
    private static final javax.xml.namespace.QName ns3_ArrayOfString_TYPE_QNAME = new QName("urn:AllegroWebApi", "ArrayOfString");
    private CombinedSerializer ns3_myns3_ArrayOfString__StringArray_SOAPSerializer1;
    private static final javax.xml.namespace.QName ns1_filter$2d$value$2d$range_QNAME = new QName("", "filter-value-range");
    private static final javax.xml.namespace.QName ns3_RangeValueType_TYPE_QNAME = new QName("urn:AllegroWebApi", "RangeValueType");
    private CombinedSerializer ns3_myRangeValueType_SOAPSerializer;
    private static final int myFILTERID_INDEX = 0;
    private static final int myFILTERVALUEID_INDEX = 1;
    private static final int myFILTERVALUERANGE_INDEX = 2;
    
    public FilterOptionsType_SOAPSerializer(QName type, boolean encodeType, boolean isNullable, String encodingStyle) {
        super(type, encodeType, isNullable, encodingStyle);
    }
    
    public void initialize(InternalTypeMappingRegistry registry) throws java.lang.Exception {
        ns2_myns2_string__java_lang_String_String_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, java.lang.String.class, ns2_string_TYPE_QNAME);
        ns3_myns3_ArrayOfString__StringArray_SOAPSerializer1 = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, java.lang.String[].class, ns3_ArrayOfString_TYPE_QNAME);
        ns3_myRangeValueType_SOAPSerializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, AllegroWebApi.RangeValueType.class, ns3_RangeValueType_TYPE_QNAME);
    }
    
    public java.lang.Object doDeserialize(SOAPDeserializationState state, XMLReader reader,
        SOAPDeserializationContext context) throws java.lang.Exception {
        AllegroWebApi.FilterOptionsType instance = new AllegroWebApi.FilterOptionsType();
        AllegroWebApi.FilterOptionsType_SOAPBuilder builder = null;
        java.lang.Object member;
        boolean isComplete = true;
        javax.xml.namespace.QName elementName;
        
        reader.nextElementContent();
        for (int i=0; i<3; i++) {
            elementName = reader.getName();
            if (reader.getState() == XMLReader.END) {
                break;
            }
            if (elementName.equals(ns1_filter$2d$id_QNAME)) {
                member = ns2_myns2_string__java_lang_String_String_Serializer.deserialize(ns1_filter$2d$id_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.FilterOptionsType_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myFILTERID_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setFilterId((java.lang.String)member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_filter$2d$value$2d$id_QNAME)) {
                member = ns3_myns3_ArrayOfString__StringArray_SOAPSerializer1.deserialize(ns1_filter$2d$value$2d$id_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.FilterOptionsType_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myFILTERVALUEID_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setFilterValueId((java.lang.String[])member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_filter$2d$value$2d$range_QNAME)) {
                member = ns3_myRangeValueType_SOAPSerializer.deserialize(ns1_filter$2d$value$2d$range_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.FilterOptionsType_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myFILTERVALUERANGE_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setFilterValueRange((AllegroWebApi.RangeValueType)member);
                }
                reader.nextElementContent();
                continue;
            } else {
                throw new DeserializationException("soap.unexpectedElementName", new Object[] {ns1_filter$2d$value$2d$range_QNAME, elementName});
            }
        }
        
        XMLReaderUtil.verifyReaderState(reader, XMLReader.END);
        return (isComplete ? (java.lang.Object)instance : (java.lang.Object)state);
    }
    
    public void doSerializeAttributes(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws java.lang.Exception {
        AllegroWebApi.FilterOptionsType instance = (AllegroWebApi.FilterOptionsType)obj;
        
    }
    
    public void doSerializeInstance(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws java.lang.Exception {
        AllegroWebApi.FilterOptionsType instance = (AllegroWebApi.FilterOptionsType)obj;
        
        ns2_myns2_string__java_lang_String_String_Serializer.serialize(instance.getFilterId(), ns1_filter$2d$id_QNAME, null, writer, context);
        ns3_myns3_ArrayOfString__StringArray_SOAPSerializer1.serialize(instance.getFilterValueId(), ns1_filter$2d$value$2d$id_QNAME, null, writer, context);
        ns3_myRangeValueType_SOAPSerializer.serialize(instance.getFilterValueRange(), ns1_filter$2d$value$2d$range_QNAME, null, writer, context);
    }
}
