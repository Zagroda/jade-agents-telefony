// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class AllegroWebApiPortType_doCreateItemTemplate_ResponseStruct {
    protected AllegroWebApi.CreatedItemTemplateStruct createdItemTemplate;
    
    public AllegroWebApiPortType_doCreateItemTemplate_ResponseStruct() {
    }
    
    public AllegroWebApiPortType_doCreateItemTemplate_ResponseStruct(AllegroWebApi.CreatedItemTemplateStruct createdItemTemplate) {
        this.createdItemTemplate = createdItemTemplate;
    }
    
    public AllegroWebApi.CreatedItemTemplateStruct getCreatedItemTemplate() {
        return createdItemTemplate;
    }
    
    public void setCreatedItemTemplate(AllegroWebApi.CreatedItemTemplateStruct createdItemTemplate) {
        this.createdItemTemplate = createdItemTemplate;
    }
}
