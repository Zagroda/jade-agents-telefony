// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class UserBlackListStruct {
    protected int userId;
    protected java.lang.String userBlackListNote;
    
    public UserBlackListStruct() {
    }
    
    public UserBlackListStruct(int userId, java.lang.String userBlackListNote) {
        this.userId = userId;
        this.userBlackListNote = userBlackListNote;
    }
    
    public int getUserId() {
        return userId;
    }
    
    public void setUserId(int userId) {
        this.userId = userId;
    }
    
    public java.lang.String getUserBlackListNote() {
        return userBlackListNote;
    }
    
    public void setUserBlackListNote(java.lang.String userBlackListNote) {
        this.userBlackListNote = userBlackListNote;
    }
}
