// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;

import com.sun.xml.rpc.encoding.*;
import com.sun.xml.rpc.encoding.literal.DetailFragmentDeserializer;
import com.sun.xml.rpc.encoding.simpletype.*;
import com.sun.xml.rpc.encoding.soap.SOAPConstants;
import com.sun.xml.rpc.encoding.soap.SOAP12Constants;
import com.sun.xml.rpc.streaming.*;
import com.sun.xml.rpc.wsdl.document.schema.SchemaConstants;
import javax.xml.namespace.QName;

public class ProductStruct_SOAPSerializer extends ObjectSerializerBase implements Initializable {
    private static final javax.xml.namespace.QName ns1_product$2d$id_QNAME = new QName("", "product-id");
    private static final javax.xml.namespace.QName ns2_long_TYPE_QNAME = SchemaConstants.QNAME_TYPE_LONG;
    private CombinedSerializer ns2_myns2__long__long_Long_Serializer;
    private static final javax.xml.namespace.QName ns1_product$2d$name_QNAME = new QName("", "product-name");
    private static final javax.xml.namespace.QName ns2_string_TYPE_QNAME = SchemaConstants.QNAME_TYPE_STRING;
    private CombinedSerializer ns2_myns2_string__java_lang_String_String_Serializer;
    private static final javax.xml.namespace.QName ns1_product$2d$description_QNAME = new QName("", "product-description");
    private static final javax.xml.namespace.QName ns1_product$2d$images$2d$list_QNAME = new QName("", "product-images-list");
    private static final javax.xml.namespace.QName ns3_ArrayOfProductImagesList_TYPE_QNAME = new QName("urn:AllegroWebApi", "ArrayOfProductImagesList");
    private CombinedSerializer ns3_myns3_ArrayOfProductImagesList__StringArray_SOAPSerializer1;
    private static final javax.xml.namespace.QName ns1_product$2d$parameters$2d$group$2d$list_QNAME = new QName("", "product-parameters-group-list");
    private static final javax.xml.namespace.QName ns3_ArrayOfProductParametersGroupStruct_TYPE_QNAME = new QName("urn:AllegroWebApi", "ArrayOfProductParametersGroupStruct");
    private CombinedSerializer ns3_myns3_ArrayOfProductParametersGroupStruct__ProductParametersGroupStructArray_SOAPSerializer1;
    private static final int myPRODUCTID_INDEX = 0;
    private static final int myPRODUCTNAME_INDEX = 1;
    private static final int myPRODUCTDESCRIPTION_INDEX = 2;
    private static final int myPRODUCTIMAGESLIST_INDEX = 3;
    private static final int myPRODUCTPARAMETERSGROUPLIST_INDEX = 4;
    
    public ProductStruct_SOAPSerializer(QName type, boolean encodeType, boolean isNullable, String encodingStyle) {
        super(type, encodeType, isNullable, encodingStyle);
    }
    
    public void initialize(InternalTypeMappingRegistry registry) throws java.lang.Exception {
        ns2_myns2__long__long_Long_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, long.class, ns2_long_TYPE_QNAME);
        ns2_myns2_string__java_lang_String_String_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, java.lang.String.class, ns2_string_TYPE_QNAME);
        ns3_myns3_ArrayOfProductImagesList__StringArray_SOAPSerializer1 = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, java.lang.String[].class, ns3_ArrayOfProductImagesList_TYPE_QNAME);
        ns3_myns3_ArrayOfProductParametersGroupStruct__ProductParametersGroupStructArray_SOAPSerializer1 = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, AllegroWebApi.ProductParametersGroupStruct[].class, ns3_ArrayOfProductParametersGroupStruct_TYPE_QNAME);
    }
    
    public java.lang.Object doDeserialize(SOAPDeserializationState state, XMLReader reader,
        SOAPDeserializationContext context) throws java.lang.Exception {
        AllegroWebApi.ProductStruct instance = new AllegroWebApi.ProductStruct();
        AllegroWebApi.ProductStruct_SOAPBuilder builder = null;
        java.lang.Object member;
        boolean isComplete = true;
        javax.xml.namespace.QName elementName;
        
        reader.nextElementContent();
        for (int i=0; i<5; i++) {
            elementName = reader.getName();
            if (reader.getState() == XMLReader.END) {
                break;
            }
            if (elementName.equals(ns1_product$2d$id_QNAME)) {
                member = ns2_myns2__long__long_Long_Serializer.deserialize(ns1_product$2d$id_QNAME, reader, context);
                instance.setProductId(((Long)member).longValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_product$2d$name_QNAME)) {
                member = ns2_myns2_string__java_lang_String_String_Serializer.deserialize(ns1_product$2d$name_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.ProductStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myPRODUCTNAME_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setProductName((java.lang.String)member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_product$2d$description_QNAME)) {
                member = ns2_myns2_string__java_lang_String_String_Serializer.deserialize(ns1_product$2d$description_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.ProductStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myPRODUCTDESCRIPTION_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setProductDescription((java.lang.String)member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_product$2d$images$2d$list_QNAME)) {
                member = ns3_myns3_ArrayOfProductImagesList__StringArray_SOAPSerializer1.deserialize(ns1_product$2d$images$2d$list_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.ProductStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myPRODUCTIMAGESLIST_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setProductImagesList((java.lang.String[])member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_product$2d$parameters$2d$group$2d$list_QNAME)) {
                member = ns3_myns3_ArrayOfProductParametersGroupStruct__ProductParametersGroupStructArray_SOAPSerializer1.deserialize(ns1_product$2d$parameters$2d$group$2d$list_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.ProductStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myPRODUCTPARAMETERSGROUPLIST_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setProductParametersGroupList((AllegroWebApi.ProductParametersGroupStruct[])member);
                }
                reader.nextElementContent();
                continue;
            } else {
                throw new DeserializationException("soap.unexpectedElementName", new Object[] {ns1_product$2d$parameters$2d$group$2d$list_QNAME, elementName});
            }
        }
        
        XMLReaderUtil.verifyReaderState(reader, XMLReader.END);
        return (isComplete ? (java.lang.Object)instance : (java.lang.Object)state);
    }
    
    public void doSerializeAttributes(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws java.lang.Exception {
        AllegroWebApi.ProductStruct instance = (AllegroWebApi.ProductStruct)obj;
        
    }
    
    public void doSerializeInstance(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws java.lang.Exception {
        AllegroWebApi.ProductStruct instance = (AllegroWebApi.ProductStruct)obj;
        
        ns2_myns2__long__long_Long_Serializer.serialize(new Long(instance.getProductId()), ns1_product$2d$id_QNAME, null, writer, context);
        ns2_myns2_string__java_lang_String_String_Serializer.serialize(instance.getProductName(), ns1_product$2d$name_QNAME, null, writer, context);
        ns2_myns2_string__java_lang_String_String_Serializer.serialize(instance.getProductDescription(), ns1_product$2d$description_QNAME, null, writer, context);
        ns3_myns3_ArrayOfProductImagesList__StringArray_SOAPSerializer1.serialize(instance.getProductImagesList(), ns1_product$2d$images$2d$list_QNAME, null, writer, context);
        ns3_myns3_ArrayOfProductParametersGroupStruct__ProductParametersGroupStructArray_SOAPSerializer1.serialize(instance.getProductParametersGroupList(), ns1_product$2d$parameters$2d$group$2d$list_QNAME, null, writer, context);
    }
}
