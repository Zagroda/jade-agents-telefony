// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class ChangedItemStruct {
    protected long itemId;
    protected AllegroWebApi.FieldsValue[] itemFields;
    protected AllegroWebApi.ItemSurchargeStruct[] itemSurcharge;
    
    public ChangedItemStruct() {
    }
    
    public ChangedItemStruct(long itemId, AllegroWebApi.FieldsValue[] itemFields, AllegroWebApi.ItemSurchargeStruct[] itemSurcharge) {
        this.itemId = itemId;
        this.itemFields = itemFields;
        this.itemSurcharge = itemSurcharge;
    }
    
    public long getItemId() {
        return itemId;
    }
    
    public void setItemId(long itemId) {
        this.itemId = itemId;
    }
    
    public AllegroWebApi.FieldsValue[] getItemFields() {
        return itemFields;
    }
    
    public void setItemFields(AllegroWebApi.FieldsValue[] itemFields) {
        this.itemFields = itemFields;
    }
    
    public AllegroWebApi.ItemSurchargeStruct[] getItemSurcharge() {
        return itemSurcharge;
    }
    
    public void setItemSurcharge(AllegroWebApi.ItemSurchargeStruct[] itemSurcharge) {
        this.itemSurcharge = itemSurcharge;
    }
}
