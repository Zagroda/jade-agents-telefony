// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class AllegroWebApiPortType_doBidItem_RequestStruct {
    protected java.lang.String sessionHandle;
    protected long bidItId;
    protected float bidUserPrice;
    protected long bidQuantity;
    protected long bidBuyNow;
    protected AllegroWebApi.PharmacyRecipientDataStruct pharmacyRecipientData;
    protected java.lang.String variantId;
    
    public AllegroWebApiPortType_doBidItem_RequestStruct() {
    }
    
    public AllegroWebApiPortType_doBidItem_RequestStruct(java.lang.String sessionHandle, long bidItId, float bidUserPrice, long bidQuantity, long bidBuyNow, AllegroWebApi.PharmacyRecipientDataStruct pharmacyRecipientData, java.lang.String variantId) {
        this.sessionHandle = sessionHandle;
        this.bidItId = bidItId;
        this.bidUserPrice = bidUserPrice;
        this.bidQuantity = bidQuantity;
        this.bidBuyNow = bidBuyNow;
        this.pharmacyRecipientData = pharmacyRecipientData;
        this.variantId = variantId;
    }
    
    public java.lang.String getSessionHandle() {
        return sessionHandle;
    }
    
    public void setSessionHandle(java.lang.String sessionHandle) {
        this.sessionHandle = sessionHandle;
    }
    
    public long getBidItId() {
        return bidItId;
    }
    
    public void setBidItId(long bidItId) {
        this.bidItId = bidItId;
    }
    
    public float getBidUserPrice() {
        return bidUserPrice;
    }
    
    public void setBidUserPrice(float bidUserPrice) {
        this.bidUserPrice = bidUserPrice;
    }
    
    public long getBidQuantity() {
        return bidQuantity;
    }
    
    public void setBidQuantity(long bidQuantity) {
        this.bidQuantity = bidQuantity;
    }
    
    public long getBidBuyNow() {
        return bidBuyNow;
    }
    
    public void setBidBuyNow(long bidBuyNow) {
        this.bidBuyNow = bidBuyNow;
    }
    
    public AllegroWebApi.PharmacyRecipientDataStruct getPharmacyRecipientData() {
        return pharmacyRecipientData;
    }
    
    public void setPharmacyRecipientData(AllegroWebApi.PharmacyRecipientDataStruct pharmacyRecipientData) {
        this.pharmacyRecipientData = pharmacyRecipientData;
    }
    
    public java.lang.String getVariantId() {
        return variantId;
    }
    
    public void setVariantId(java.lang.String variantId) {
        this.variantId = variantId;
    }
}
