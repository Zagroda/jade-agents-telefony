// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class FilterPriceStruct {
    protected float filterPriceFrom;
    protected float filterPriceTo;
    
    public FilterPriceStruct() {
    }
    
    public FilterPriceStruct(float filterPriceFrom, float filterPriceTo) {
        this.filterPriceFrom = filterPriceFrom;
        this.filterPriceTo = filterPriceTo;
    }
    
    public float getFilterPriceFrom() {
        return filterPriceFrom;
    }
    
    public void setFilterPriceFrom(float filterPriceFrom) {
        this.filterPriceFrom = filterPriceFrom;
    }
    
    public float getFilterPriceTo() {
        return filterPriceTo;
    }
    
    public void setFilterPriceTo(float filterPriceTo) {
        this.filterPriceTo = filterPriceTo;
    }
}
