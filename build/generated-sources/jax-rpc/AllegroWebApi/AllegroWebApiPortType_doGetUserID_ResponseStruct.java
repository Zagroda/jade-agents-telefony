// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class AllegroWebApiPortType_doGetUserID_ResponseStruct {
    protected int userId;
    
    public AllegroWebApiPortType_doGetUserID_ResponseStruct() {
    }
    
    public AllegroWebApiPortType_doGetUserID_ResponseStruct(int userId) {
        this.userId = userId;
    }
    
    public int getUserId() {
        return userId;
    }
    
    public void setUserId(int userId) {
        this.userId = userId;
    }
}
