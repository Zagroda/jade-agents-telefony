// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class AllegroWebApiPortType_doSendPostBuyForm_RequestStruct {
    protected java.lang.String sessionId;
    protected AllegroWebApi.NewPostBuyFormSellerStruct[] newPostBuyFormSeller;
    protected AllegroWebApi.NewPostBuyFormCommonStruct newPostBuyFormCommon;
    
    public AllegroWebApiPortType_doSendPostBuyForm_RequestStruct() {
    }
    
    public AllegroWebApiPortType_doSendPostBuyForm_RequestStruct(java.lang.String sessionId, AllegroWebApi.NewPostBuyFormSellerStruct[] newPostBuyFormSeller, AllegroWebApi.NewPostBuyFormCommonStruct newPostBuyFormCommon) {
        this.sessionId = sessionId;
        this.newPostBuyFormSeller = newPostBuyFormSeller;
        this.newPostBuyFormCommon = newPostBuyFormCommon;
    }
    
    public java.lang.String getSessionId() {
        return sessionId;
    }
    
    public void setSessionId(java.lang.String sessionId) {
        this.sessionId = sessionId;
    }
    
    public AllegroWebApi.NewPostBuyFormSellerStruct[] getNewPostBuyFormSeller() {
        return newPostBuyFormSeller;
    }
    
    public void setNewPostBuyFormSeller(AllegroWebApi.NewPostBuyFormSellerStruct[] newPostBuyFormSeller) {
        this.newPostBuyFormSeller = newPostBuyFormSeller;
    }
    
    public AllegroWebApi.NewPostBuyFormCommonStruct getNewPostBuyFormCommon() {
        return newPostBuyFormCommon;
    }
    
    public void setNewPostBuyFormCommon(AllegroWebApi.NewPostBuyFormCommonStruct newPostBuyFormCommon) {
        this.newPostBuyFormCommon = newPostBuyFormCommon;
    }
}
