// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class ItemImageList {
    protected int imageType;
    protected java.lang.String imageUrl;
    
    public ItemImageList() {
    }
    
    public ItemImageList(int imageType, java.lang.String imageUrl) {
        this.imageType = imageType;
        this.imageUrl = imageUrl;
    }
    
    public int getImageType() {
        return imageType;
    }
    
    public void setImageType(int imageType) {
        this.imageType = imageType;
    }
    
    public java.lang.String getImageUrl() {
        return imageUrl;
    }
    
    public void setImageUrl(java.lang.String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
