// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;

import com.sun.xml.rpc.encoding.*;
import com.sun.xml.rpc.util.exception.LocalizableExceptionAdapter;

public class CatInfoType_SOAPBuilder implements SOAPInstanceBuilder {
    private AllegroWebApi.CatInfoType _instance;
    private int catId;
    private java.lang.String catName;
    private int catParent;
    private int catPosition;
    private int catIsProductCatalogueEnabled;
    private static final int myCATID_INDEX = 0;
    private static final int myCATNAME_INDEX = 1;
    private static final int myCATPARENT_INDEX = 2;
    private static final int myCATPOSITION_INDEX = 3;
    private static final int myCATISPRODUCTCATALOGUEENABLED_INDEX = 4;
    
    public CatInfoType_SOAPBuilder() {
    }
    
    public void setCatId(int catId) {
        this.catId = catId;
    }
    
    public void setCatName(java.lang.String catName) {
        this.catName = catName;
    }
    
    public void setCatParent(int catParent) {
        this.catParent = catParent;
    }
    
    public void setCatPosition(int catPosition) {
        this.catPosition = catPosition;
    }
    
    public void setCatIsProductCatalogueEnabled(int catIsProductCatalogueEnabled) {
        this.catIsProductCatalogueEnabled = catIsProductCatalogueEnabled;
    }
    
    public int memberGateType(int memberIndex) {
        switch (memberIndex) {
            case myCATNAME_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            default:
                throw new IllegalArgumentException();
        }
    }
    
    public void construct() {
    }
    
    public void setMember(int index, java.lang.Object memberValue) {
        try {
            switch(index) {
                case myCATNAME_INDEX:
                    _instance.setCatName((java.lang.String)memberValue);
                    break;
                default:
                    throw new java.lang.IllegalArgumentException();
            }
        }
        catch (java.lang.RuntimeException e) {
            throw e;
        }
        catch (java.lang.Exception e) {
            throw new DeserializationException(new LocalizableExceptionAdapter(e));
        }
    }
    
    public void initialize() {
    }
    
    public void setInstance(java.lang.Object instance) {
        _instance = (AllegroWebApi.CatInfoType)instance;
    }
    
    public java.lang.Object getInstance() {
        return _instance;
    }
}
