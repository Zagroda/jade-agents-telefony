// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class AllegroWebApiPortType_doGetAdminUserLicenceDate_ResponseStruct {
    protected float getDateValue;
    
    public AllegroWebApiPortType_doGetAdminUserLicenceDate_ResponseStruct() {
    }
    
    public AllegroWebApiPortType_doGetAdminUserLicenceDate_ResponseStruct(float getDateValue) {
        this.getDateValue = getDateValue;
    }
    
    public float getGetDateValue() {
        return getDateValue;
    }
    
    public void setGetDateValue(float getDateValue) {
        this.getDateValue = getDateValue;
    }
}
