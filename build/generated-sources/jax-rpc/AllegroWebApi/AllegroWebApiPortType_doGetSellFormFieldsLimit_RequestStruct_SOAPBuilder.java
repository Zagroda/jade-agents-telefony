// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;

import com.sun.xml.rpc.encoding.*;
import com.sun.xml.rpc.util.exception.LocalizableExceptionAdapter;

public class AllegroWebApiPortType_doGetSellFormFieldsLimit_RequestStruct_SOAPBuilder implements SOAPInstanceBuilder {
    private AllegroWebApi.AllegroWebApiPortType_doGetSellFormFieldsLimit_RequestStruct _instance;
    private int countryCode;
    private long localVersion;
    private java.lang.String webapiKey;
    private int offset;
    private int packageElement;
    private static final int myCOUNTRYCODE_INDEX = 0;
    private static final int myLOCALVERSION_INDEX = 1;
    private static final int myWEBAPIKEY_INDEX = 2;
    private static final int myOFFSET_INDEX = 3;
    private static final int myPACKAGEELEMENT_INDEX = 4;
    
    public AllegroWebApiPortType_doGetSellFormFieldsLimit_RequestStruct_SOAPBuilder() {
    }
    
    public void setCountryCode(int countryCode) {
        this.countryCode = countryCode;
    }
    
    public void setLocalVersion(long localVersion) {
        this.localVersion = localVersion;
    }
    
    public void setWebapiKey(java.lang.String webapiKey) {
        this.webapiKey = webapiKey;
    }
    
    public void setOffset(int offset) {
        this.offset = offset;
    }
    
    public void setPackageElement(int packageElement) {
        this.packageElement = packageElement;
    }
    
    public int memberGateType(int memberIndex) {
        switch (memberIndex) {
            case myWEBAPIKEY_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            default:
                throw new IllegalArgumentException();
        }
    }
    
    public void construct() {
    }
    
    public void setMember(int index, java.lang.Object memberValue) {
        try {
            switch(index) {
                case myWEBAPIKEY_INDEX:
                    _instance.setWebapiKey((java.lang.String)memberValue);
                    break;
                default:
                    throw new java.lang.IllegalArgumentException();
            }
        }
        catch (java.lang.RuntimeException e) {
            throw e;
        }
        catch (java.lang.Exception e) {
            throw new DeserializationException(new LocalizableExceptionAdapter(e));
        }
    }
    
    public void initialize() {
    }
    
    public void setInstance(java.lang.Object instance) {
        _instance = (AllegroWebApi.AllegroWebApiPortType_doGetSellFormFieldsLimit_RequestStruct)instance;
    }
    
    public java.lang.Object getInstance() {
        return _instance;
    }
}
