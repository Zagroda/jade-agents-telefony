// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi.holders;


public class ArrayOfStructSellAgainHolder implements javax.xml.rpc.holders.Holder {
    public AllegroWebApi.StructSellAgain[] value;
    
    public ArrayOfStructSellAgainHolder() {
    }
    
    public ArrayOfStructSellAgainHolder(AllegroWebApi.StructSellAgain[] myStructSellAgain) {
        this.value = myStructSellAgain;
    }
}
