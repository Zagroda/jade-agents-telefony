// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi.holders;


public class ArrayOfFinishSuccessStructHolder implements javax.xml.rpc.holders.Holder {
    public long[] value;
    
    public ArrayOfFinishSuccessStructHolder() {
    }
    
    public ArrayOfFinishSuccessStructHolder(long[] mylong) {
        this.value = mylong;
    }
}
