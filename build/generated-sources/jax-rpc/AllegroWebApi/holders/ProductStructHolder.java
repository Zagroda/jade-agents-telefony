// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi.holders;


public class ProductStructHolder implements javax.xml.rpc.holders.Holder {
    public AllegroWebApi.ProductStruct value;
    
    public ProductStructHolder() {
    }
    
    public ProductStructHolder(AllegroWebApi.ProductStruct myProductStruct) {
        this.value = myProductStruct;
    }
}
