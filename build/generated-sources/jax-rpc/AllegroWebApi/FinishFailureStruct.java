// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class FinishFailureStruct {
    protected long finishItemId;
    protected java.lang.String finishErrorCode;
    protected java.lang.String finishErrorMessage;
    
    public FinishFailureStruct() {
    }
    
    public FinishFailureStruct(long finishItemId, java.lang.String finishErrorCode, java.lang.String finishErrorMessage) {
        this.finishItemId = finishItemId;
        this.finishErrorCode = finishErrorCode;
        this.finishErrorMessage = finishErrorMessage;
    }
    
    public long getFinishItemId() {
        return finishItemId;
    }
    
    public void setFinishItemId(long finishItemId) {
        this.finishItemId = finishItemId;
    }
    
    public java.lang.String getFinishErrorCode() {
        return finishErrorCode;
    }
    
    public void setFinishErrorCode(java.lang.String finishErrorCode) {
        this.finishErrorCode = finishErrorCode;
    }
    
    public java.lang.String getFinishErrorMessage() {
        return finishErrorMessage;
    }
    
    public void setFinishErrorMessage(java.lang.String finishErrorMessage) {
        this.finishErrorMessage = finishErrorMessage;
    }
}
