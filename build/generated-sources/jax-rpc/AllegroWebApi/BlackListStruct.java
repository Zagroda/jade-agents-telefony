// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class BlackListStruct {
    protected long userId;
    protected java.lang.String userLogin;
    protected int userRating;
    protected int userCountry;
    
    public BlackListStruct() {
    }
    
    public BlackListStruct(long userId, java.lang.String userLogin, int userRating, int userCountry) {
        this.userId = userId;
        this.userLogin = userLogin;
        this.userRating = userRating;
        this.userCountry = userCountry;
    }
    
    public long getUserId() {
        return userId;
    }
    
    public void setUserId(long userId) {
        this.userId = userId;
    }
    
    public java.lang.String getUserLogin() {
        return userLogin;
    }
    
    public void setUserLogin(java.lang.String userLogin) {
        this.userLogin = userLogin;
    }
    
    public int getUserRating() {
        return userRating;
    }
    
    public void setUserRating(int userRating) {
        this.userRating = userRating;
    }
    
    public int getUserCountry() {
        return userCountry;
    }
    
    public void setUserCountry(int userCountry) {
        this.userCountry = userCountry;
    }
}
