// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;

import com.sun.xml.rpc.encoding.*;
import com.sun.xml.rpc.util.exception.LocalizableExceptionAdapter;

public class PostBuyFormForBuyersDataStruct_SOAPBuilder implements SOAPInstanceBuilder {
    private AllegroWebApi.PostBuyFormForBuyersDataStruct _instance;
    private long postBuyFormId;
    private int postBuyFormBuyerId;
    private AllegroWebApi.PostBuyFormSellersStruct[] postBuyFormSellers;
    private float postBuyFormTotalAmount;
    private int postBuyFormInvoiceOption;
    private AllegroWebApi.PostBuyFormAddressStruct postBuyFormInvoiceData;
    private AllegroWebApi.PostBuyFormAddressStruct postBuyFormShipmentAddress;
    private java.lang.String postBuyFormPayType;
    private long postBuyFormPayId;
    private java.lang.String postBuyFormPayStatus;
    private java.lang.String postBuyFormDateInit;
    private java.lang.String postBuyFormDateRecv;
    private java.lang.String postBuyFormDateCancel;
    private float postBuyFormPaymentAmount;
    private static final int myPOSTBUYFORMID_INDEX = 0;
    private static final int myPOSTBUYFORMBUYERID_INDEX = 1;
    private static final int myPOSTBUYFORMSELLERS_INDEX = 2;
    private static final int myPOSTBUYFORMTOTALAMOUNT_INDEX = 3;
    private static final int myPOSTBUYFORMINVOICEOPTION_INDEX = 4;
    private static final int myPOSTBUYFORMINVOICEDATA_INDEX = 5;
    private static final int myPOSTBUYFORMSHIPMENTADDRESS_INDEX = 6;
    private static final int myPOSTBUYFORMPAYTYPE_INDEX = 7;
    private static final int myPOSTBUYFORMPAYID_INDEX = 8;
    private static final int myPOSTBUYFORMPAYSTATUS_INDEX = 9;
    private static final int myPOSTBUYFORMDATEINIT_INDEX = 10;
    private static final int myPOSTBUYFORMDATERECV_INDEX = 11;
    private static final int myPOSTBUYFORMDATECANCEL_INDEX = 12;
    private static final int myPOSTBUYFORMPAYMENTAMOUNT_INDEX = 13;
    
    public PostBuyFormForBuyersDataStruct_SOAPBuilder() {
    }
    
    public void setPostBuyFormId(long postBuyFormId) {
        this.postBuyFormId = postBuyFormId;
    }
    
    public void setPostBuyFormBuyerId(int postBuyFormBuyerId) {
        this.postBuyFormBuyerId = postBuyFormBuyerId;
    }
    
    public void setPostBuyFormSellers(AllegroWebApi.PostBuyFormSellersStruct[] postBuyFormSellers) {
        this.postBuyFormSellers = postBuyFormSellers;
    }
    
    public void setPostBuyFormTotalAmount(float postBuyFormTotalAmount) {
        this.postBuyFormTotalAmount = postBuyFormTotalAmount;
    }
    
    public void setPostBuyFormInvoiceOption(int postBuyFormInvoiceOption) {
        this.postBuyFormInvoiceOption = postBuyFormInvoiceOption;
    }
    
    public void setPostBuyFormInvoiceData(AllegroWebApi.PostBuyFormAddressStruct postBuyFormInvoiceData) {
        this.postBuyFormInvoiceData = postBuyFormInvoiceData;
    }
    
    public void setPostBuyFormShipmentAddress(AllegroWebApi.PostBuyFormAddressStruct postBuyFormShipmentAddress) {
        this.postBuyFormShipmentAddress = postBuyFormShipmentAddress;
    }
    
    public void setPostBuyFormPayType(java.lang.String postBuyFormPayType) {
        this.postBuyFormPayType = postBuyFormPayType;
    }
    
    public void setPostBuyFormPayId(long postBuyFormPayId) {
        this.postBuyFormPayId = postBuyFormPayId;
    }
    
    public void setPostBuyFormPayStatus(java.lang.String postBuyFormPayStatus) {
        this.postBuyFormPayStatus = postBuyFormPayStatus;
    }
    
    public void setPostBuyFormDateInit(java.lang.String postBuyFormDateInit) {
        this.postBuyFormDateInit = postBuyFormDateInit;
    }
    
    public void setPostBuyFormDateRecv(java.lang.String postBuyFormDateRecv) {
        this.postBuyFormDateRecv = postBuyFormDateRecv;
    }
    
    public void setPostBuyFormDateCancel(java.lang.String postBuyFormDateCancel) {
        this.postBuyFormDateCancel = postBuyFormDateCancel;
    }
    
    public void setPostBuyFormPaymentAmount(float postBuyFormPaymentAmount) {
        this.postBuyFormPaymentAmount = postBuyFormPaymentAmount;
    }
    
    public int memberGateType(int memberIndex) {
        switch (memberIndex) {
            case myPOSTBUYFORMSELLERS_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            case myPOSTBUYFORMINVOICEDATA_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            case myPOSTBUYFORMSHIPMENTADDRESS_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            case myPOSTBUYFORMPAYTYPE_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            case myPOSTBUYFORMPAYSTATUS_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            case myPOSTBUYFORMDATEINIT_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            case myPOSTBUYFORMDATERECV_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            case myPOSTBUYFORMDATECANCEL_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            default:
                throw new IllegalArgumentException();
        }
    }
    
    public void construct() {
    }
    
    public void setMember(int index, java.lang.Object memberValue) {
        try {
            switch(index) {
                case myPOSTBUYFORMSELLERS_INDEX:
                    _instance.setPostBuyFormSellers((AllegroWebApi.PostBuyFormSellersStruct[])memberValue);
                    break;
                case myPOSTBUYFORMINVOICEDATA_INDEX:
                    _instance.setPostBuyFormInvoiceData((AllegroWebApi.PostBuyFormAddressStruct)memberValue);
                    break;
                case myPOSTBUYFORMSHIPMENTADDRESS_INDEX:
                    _instance.setPostBuyFormShipmentAddress((AllegroWebApi.PostBuyFormAddressStruct)memberValue);
                    break;
                case myPOSTBUYFORMPAYTYPE_INDEX:
                    _instance.setPostBuyFormPayType((java.lang.String)memberValue);
                    break;
                case myPOSTBUYFORMPAYSTATUS_INDEX:
                    _instance.setPostBuyFormPayStatus((java.lang.String)memberValue);
                    break;
                case myPOSTBUYFORMDATEINIT_INDEX:
                    _instance.setPostBuyFormDateInit((java.lang.String)memberValue);
                    break;
                case myPOSTBUYFORMDATERECV_INDEX:
                    _instance.setPostBuyFormDateRecv((java.lang.String)memberValue);
                    break;
                case myPOSTBUYFORMDATECANCEL_INDEX:
                    _instance.setPostBuyFormDateCancel((java.lang.String)memberValue);
                    break;
                default:
                    throw new java.lang.IllegalArgumentException();
            }
        }
        catch (java.lang.RuntimeException e) {
            throw e;
        }
        catch (java.lang.Exception e) {
            throw new DeserializationException(new LocalizableExceptionAdapter(e));
        }
    }
    
    public void initialize() {
    }
    
    public void setInstance(java.lang.Object instance) {
        _instance = (AllegroWebApi.PostBuyFormForBuyersDataStruct)instance;
    }
    
    public java.lang.Object getInstance() {
        return _instance;
    }
}
