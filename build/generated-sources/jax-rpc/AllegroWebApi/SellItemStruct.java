// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class SellItemStruct {
    protected long itemId;
    protected java.lang.String itemTitle;
    protected java.lang.String itemThumbnailUrl;
    protected AllegroWebApi.ItemPriceStruct[] itemPrice;
    protected int itemStartQuantity;
    protected int itemSoldQuantity;
    protected int itemQuantityType;
    protected long itemStartTime;
    protected long itemEndTime;
    protected java.lang.String itemEndTimeLeft;
    protected int itemBiddersCounter;
    protected AllegroWebApi.UserInfoStruct itemHighestBidder;
    protected int itemCategoryId;
    protected int itemWatchersCounter;
    protected int itemViewsCounter;
    protected java.lang.String itemNote;
    protected int itemSpecialInfo;
    protected int itemShopInfo;
    protected long itemProductInfo;
    protected int itemPayuInfo;
    protected AllegroWebApi.DurationInfoStruct itemDurationInfo;
    
    public SellItemStruct() {
    }
    
    public SellItemStruct(long itemId, java.lang.String itemTitle, java.lang.String itemThumbnailUrl, AllegroWebApi.ItemPriceStruct[] itemPrice, int itemStartQuantity, int itemSoldQuantity, int itemQuantityType, long itemStartTime, long itemEndTime, java.lang.String itemEndTimeLeft, int itemBiddersCounter, AllegroWebApi.UserInfoStruct itemHighestBidder, int itemCategoryId, int itemWatchersCounter, int itemViewsCounter, java.lang.String itemNote, int itemSpecialInfo, int itemShopInfo, long itemProductInfo, int itemPayuInfo, AllegroWebApi.DurationInfoStruct itemDurationInfo) {
        this.itemId = itemId;
        this.itemTitle = itemTitle;
        this.itemThumbnailUrl = itemThumbnailUrl;
        this.itemPrice = itemPrice;
        this.itemStartQuantity = itemStartQuantity;
        this.itemSoldQuantity = itemSoldQuantity;
        this.itemQuantityType = itemQuantityType;
        this.itemStartTime = itemStartTime;
        this.itemEndTime = itemEndTime;
        this.itemEndTimeLeft = itemEndTimeLeft;
        this.itemBiddersCounter = itemBiddersCounter;
        this.itemHighestBidder = itemHighestBidder;
        this.itemCategoryId = itemCategoryId;
        this.itemWatchersCounter = itemWatchersCounter;
        this.itemViewsCounter = itemViewsCounter;
        this.itemNote = itemNote;
        this.itemSpecialInfo = itemSpecialInfo;
        this.itemShopInfo = itemShopInfo;
        this.itemProductInfo = itemProductInfo;
        this.itemPayuInfo = itemPayuInfo;
        this.itemDurationInfo = itemDurationInfo;
    }
    
    public long getItemId() {
        return itemId;
    }
    
    public void setItemId(long itemId) {
        this.itemId = itemId;
    }
    
    public java.lang.String getItemTitle() {
        return itemTitle;
    }
    
    public void setItemTitle(java.lang.String itemTitle) {
        this.itemTitle = itemTitle;
    }
    
    public java.lang.String getItemThumbnailUrl() {
        return itemThumbnailUrl;
    }
    
    public void setItemThumbnailUrl(java.lang.String itemThumbnailUrl) {
        this.itemThumbnailUrl = itemThumbnailUrl;
    }
    
    public AllegroWebApi.ItemPriceStruct[] getItemPrice() {
        return itemPrice;
    }
    
    public void setItemPrice(AllegroWebApi.ItemPriceStruct[] itemPrice) {
        this.itemPrice = itemPrice;
    }
    
    public int getItemStartQuantity() {
        return itemStartQuantity;
    }
    
    public void setItemStartQuantity(int itemStartQuantity) {
        this.itemStartQuantity = itemStartQuantity;
    }
    
    public int getItemSoldQuantity() {
        return itemSoldQuantity;
    }
    
    public void setItemSoldQuantity(int itemSoldQuantity) {
        this.itemSoldQuantity = itemSoldQuantity;
    }
    
    public int getItemQuantityType() {
        return itemQuantityType;
    }
    
    public void setItemQuantityType(int itemQuantityType) {
        this.itemQuantityType = itemQuantityType;
    }
    
    public long getItemStartTime() {
        return itemStartTime;
    }
    
    public void setItemStartTime(long itemStartTime) {
        this.itemStartTime = itemStartTime;
    }
    
    public long getItemEndTime() {
        return itemEndTime;
    }
    
    public void setItemEndTime(long itemEndTime) {
        this.itemEndTime = itemEndTime;
    }
    
    public java.lang.String getItemEndTimeLeft() {
        return itemEndTimeLeft;
    }
    
    public void setItemEndTimeLeft(java.lang.String itemEndTimeLeft) {
        this.itemEndTimeLeft = itemEndTimeLeft;
    }
    
    public int getItemBiddersCounter() {
        return itemBiddersCounter;
    }
    
    public void setItemBiddersCounter(int itemBiddersCounter) {
        this.itemBiddersCounter = itemBiddersCounter;
    }
    
    public AllegroWebApi.UserInfoStruct getItemHighestBidder() {
        return itemHighestBidder;
    }
    
    public void setItemHighestBidder(AllegroWebApi.UserInfoStruct itemHighestBidder) {
        this.itemHighestBidder = itemHighestBidder;
    }
    
    public int getItemCategoryId() {
        return itemCategoryId;
    }
    
    public void setItemCategoryId(int itemCategoryId) {
        this.itemCategoryId = itemCategoryId;
    }
    
    public int getItemWatchersCounter() {
        return itemWatchersCounter;
    }
    
    public void setItemWatchersCounter(int itemWatchersCounter) {
        this.itemWatchersCounter = itemWatchersCounter;
    }
    
    public int getItemViewsCounter() {
        return itemViewsCounter;
    }
    
    public void setItemViewsCounter(int itemViewsCounter) {
        this.itemViewsCounter = itemViewsCounter;
    }
    
    public java.lang.String getItemNote() {
        return itemNote;
    }
    
    public void setItemNote(java.lang.String itemNote) {
        this.itemNote = itemNote;
    }
    
    public int getItemSpecialInfo() {
        return itemSpecialInfo;
    }
    
    public void setItemSpecialInfo(int itemSpecialInfo) {
        this.itemSpecialInfo = itemSpecialInfo;
    }
    
    public int getItemShopInfo() {
        return itemShopInfo;
    }
    
    public void setItemShopInfo(int itemShopInfo) {
        this.itemShopInfo = itemShopInfo;
    }
    
    public long getItemProductInfo() {
        return itemProductInfo;
    }
    
    public void setItemProductInfo(long itemProductInfo) {
        this.itemProductInfo = itemProductInfo;
    }
    
    public int getItemPayuInfo() {
        return itemPayuInfo;
    }
    
    public void setItemPayuInfo(int itemPayuInfo) {
        this.itemPayuInfo = itemPayuInfo;
    }
    
    public AllegroWebApi.DurationInfoStruct getItemDurationInfo() {
        return itemDurationInfo;
    }
    
    public void setItemDurationInfo(AllegroWebApi.DurationInfoStruct itemDurationInfo) {
        this.itemDurationInfo = itemDurationInfo;
    }
}
