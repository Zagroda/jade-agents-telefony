// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class AllegroWebApiPortType_doGetFavouriteSellers_ResponseStruct {
    protected AllegroWebApi.FavouritesSellersStruct[] sFavouriteSellersList;
    
    public AllegroWebApiPortType_doGetFavouriteSellers_ResponseStruct() {
    }
    
    public AllegroWebApiPortType_doGetFavouriteSellers_ResponseStruct(AllegroWebApi.FavouritesSellersStruct[] sFavouriteSellersList) {
        this.sFavouriteSellersList = sFavouriteSellersList;
    }
    
    public AllegroWebApi.FavouritesSellersStruct[] getSFavouriteSellersList() {
        return sFavouriteSellersList;
    }
    
    public void setSFavouriteSellersList(AllegroWebApi.FavouritesSellersStruct[] sFavouriteSellersList) {
        this.sFavouriteSellersList = sFavouriteSellersList;
    }
}
