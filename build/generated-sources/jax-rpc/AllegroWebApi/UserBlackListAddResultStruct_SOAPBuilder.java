// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;

import com.sun.xml.rpc.encoding.*;
import com.sun.xml.rpc.util.exception.LocalizableExceptionAdapter;

public class UserBlackListAddResultStruct_SOAPBuilder implements SOAPInstanceBuilder {
    private AllegroWebApi.UserBlackListAddResultStruct _instance;
    private int userId;
    private int addToBlackListResult;
    private java.lang.String addToBlackListErrCode;
    private java.lang.String addToBlackListErrMsg;
    private static final int myUSERID_INDEX = 0;
    private static final int myADDTOBLACKLISTRESULT_INDEX = 1;
    private static final int myADDTOBLACKLISTERRCODE_INDEX = 2;
    private static final int myADDTOBLACKLISTERRMSG_INDEX = 3;
    
    public UserBlackListAddResultStruct_SOAPBuilder() {
    }
    
    public void setUserId(int userId) {
        this.userId = userId;
    }
    
    public void setAddToBlackListResult(int addToBlackListResult) {
        this.addToBlackListResult = addToBlackListResult;
    }
    
    public void setAddToBlackListErrCode(java.lang.String addToBlackListErrCode) {
        this.addToBlackListErrCode = addToBlackListErrCode;
    }
    
    public void setAddToBlackListErrMsg(java.lang.String addToBlackListErrMsg) {
        this.addToBlackListErrMsg = addToBlackListErrMsg;
    }
    
    public int memberGateType(int memberIndex) {
        switch (memberIndex) {
            case myADDTOBLACKLISTERRCODE_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            case myADDTOBLACKLISTERRMSG_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            default:
                throw new IllegalArgumentException();
        }
    }
    
    public void construct() {
    }
    
    public void setMember(int index, java.lang.Object memberValue) {
        try {
            switch(index) {
                case myADDTOBLACKLISTERRCODE_INDEX:
                    _instance.setAddToBlackListErrCode((java.lang.String)memberValue);
                    break;
                case myADDTOBLACKLISTERRMSG_INDEX:
                    _instance.setAddToBlackListErrMsg((java.lang.String)memberValue);
                    break;
                default:
                    throw new java.lang.IllegalArgumentException();
            }
        }
        catch (java.lang.RuntimeException e) {
            throw e;
        }
        catch (java.lang.Exception e) {
            throw new DeserializationException(new LocalizableExceptionAdapter(e));
        }
    }
    
    public void initialize() {
    }
    
    public void setInstance(java.lang.Object instance) {
        _instance = (AllegroWebApi.UserBlackListAddResultStruct)instance;
    }
    
    public java.lang.Object getInstance() {
        return _instance;
    }
}
