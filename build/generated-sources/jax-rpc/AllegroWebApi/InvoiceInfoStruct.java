// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class InvoiceInfoStruct {
    protected int invoiceAddressType;
    protected AllegroWebApi.AddressUserDataStruct invoiceAddressData;
    protected java.lang.String invoiceNip;
    
    public InvoiceInfoStruct() {
    }
    
    public InvoiceInfoStruct(int invoiceAddressType, AllegroWebApi.AddressUserDataStruct invoiceAddressData, java.lang.String invoiceNip) {
        this.invoiceAddressType = invoiceAddressType;
        this.invoiceAddressData = invoiceAddressData;
        this.invoiceNip = invoiceNip;
    }
    
    public int getInvoiceAddressType() {
        return invoiceAddressType;
    }
    
    public void setInvoiceAddressType(int invoiceAddressType) {
        this.invoiceAddressType = invoiceAddressType;
    }
    
    public AllegroWebApi.AddressUserDataStruct getInvoiceAddressData() {
        return invoiceAddressData;
    }
    
    public void setInvoiceAddressData(AllegroWebApi.AddressUserDataStruct invoiceAddressData) {
        this.invoiceAddressData = invoiceAddressData;
    }
    
    public java.lang.String getInvoiceNip() {
        return invoiceNip;
    }
    
    public void setInvoiceNip(java.lang.String invoiceNip) {
        this.invoiceNip = invoiceNip;
    }
}
