// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class ItemTemplatesStruct {
    protected AllegroWebApi.ItemTemplateListStruct[] itemTemplateList;
    protected int[] itemTemplateIncorrectIds;
    
    public ItemTemplatesStruct() {
    }
    
    public ItemTemplatesStruct(AllegroWebApi.ItemTemplateListStruct[] itemTemplateList, int[] itemTemplateIncorrectIds) {
        this.itemTemplateList = itemTemplateList;
        this.itemTemplateIncorrectIds = itemTemplateIncorrectIds;
    }
    
    public AllegroWebApi.ItemTemplateListStruct[] getItemTemplateList() {
        return itemTemplateList;
    }
    
    public void setItemTemplateList(AllegroWebApi.ItemTemplateListStruct[] itemTemplateList) {
        this.itemTemplateList = itemTemplateList;
    }
    
    public int[] getItemTemplateIncorrectIds() {
        return itemTemplateIncorrectIds;
    }
    
    public void setItemTemplateIncorrectIds(int[] itemTemplateIncorrectIds) {
        this.itemTemplateIncorrectIds = itemTemplateIncorrectIds;
    }
}
