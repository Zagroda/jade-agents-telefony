// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;

import com.sun.xml.rpc.encoding.*;
import com.sun.xml.rpc.encoding.literal.DetailFragmentDeserializer;
import com.sun.xml.rpc.encoding.simpletype.*;
import com.sun.xml.rpc.encoding.soap.SOAPConstants;
import com.sun.xml.rpc.encoding.soap.SOAP12Constants;
import com.sun.xml.rpc.streaming.*;
import com.sun.xml.rpc.wsdl.document.schema.SchemaConstants;
import javax.xml.namespace.QName;

public class PostBuyFormSellersStruct_SOAPSerializer extends ObjectSerializerBase implements Initializable {
    private static final javax.xml.namespace.QName ns1_post$2d$buy$2d$form$2d$seller$2d$id_QNAME = new QName("", "post-buy-form-seller-id");
    private static final javax.xml.namespace.QName ns2_int_TYPE_QNAME = SchemaConstants.QNAME_TYPE_INT;
    private CombinedSerializer ns2_myns2__int__int_Int_Serializer;
    private static final javax.xml.namespace.QName ns1_post$2d$buy$2d$form$2d$seller$2d$name_QNAME = new QName("", "post-buy-form-seller-name");
    private static final javax.xml.namespace.QName ns2_string_TYPE_QNAME = SchemaConstants.QNAME_TYPE_STRING;
    private CombinedSerializer ns2_myns2_string__java_lang_String_String_Serializer;
    private static final javax.xml.namespace.QName ns1_post$2d$buy$2d$form$2d$items_QNAME = new QName("", "post-buy-form-items");
    private static final javax.xml.namespace.QName ns3_ArrayOfPostBuyFormItemStruct_TYPE_QNAME = new QName("urn:AllegroWebApi", "ArrayOfPostBuyFormItemStruct");
    private CombinedSerializer ns3_myns3_ArrayOfPostBuyFormItemStruct__PostBuyFormItemStructArray_SOAPSerializer1;
    private static final javax.xml.namespace.QName ns1_post$2d$buy$2d$form$2d$shipment$2d$id_QNAME = new QName("", "post-buy-form-shipment-id");
    private static final javax.xml.namespace.QName ns1_post$2d$buy$2d$form$2d$postage$2d$amount_QNAME = new QName("", "post-buy-form-postage-amount");
    private static final javax.xml.namespace.QName ns2_float_TYPE_QNAME = SchemaConstants.QNAME_TYPE_FLOAT;
    private CombinedSerializer ns2_myns2__float__float_Float_Serializer;
    private static final javax.xml.namespace.QName ns1_post$2d$buy$2d$form$2d$msg$2d$to$2d$seller_QNAME = new QName("", "post-buy-form-msg-to-seller");
    private static final javax.xml.namespace.QName ns1_post$2d$buy$2d$form$2d$amount_QNAME = new QName("", "post-buy-form-amount");
    private static final javax.xml.namespace.QName ns1_post$2d$buy$2d$form$2d$surcharges$2d$list_QNAME = new QName("", "post-buy-form-surcharges-list");
    private static final javax.xml.namespace.QName ns3_ArrayOfPostBuyFormSurcharges_TYPE_QNAME = new QName("urn:AllegroWebApi", "ArrayOfPostBuyFormSurcharges");
    private CombinedSerializer ns3_myns3_ArrayOfPostBuyFormSurcharges__longArray_SOAPSerializer1;
    private static final javax.xml.namespace.QName ns1_post$2d$buy$2d$form$2d$shipment$2d$tracking_QNAME = new QName("", "post-buy-form-shipment-tracking");
    private static final javax.xml.namespace.QName ns3_ArrayOfPostBuyFormShipmentTrackingStruct_TYPE_QNAME = new QName("urn:AllegroWebApi", "ArrayOfPostBuyFormShipmentTrackingStruct");
    private CombinedSerializer ns3_myns3_ArrayOfPostBuyFormShipmentTrackingStruct__PostBuyFormShipmentTrackingStructArray_SOAPSerializer1;
    private static final javax.xml.namespace.QName ns1_post$2d$buy$2d$form$2d$gd$2d$address_QNAME = new QName("", "post-buy-form-gd-address");
    private static final javax.xml.namespace.QName ns3_PostBuyFormAddressStruct_TYPE_QNAME = new QName("urn:AllegroWebApi", "PostBuyFormAddressStruct");
    private CombinedSerializer ns3_myPostBuyFormAddressStruct_SOAPSerializer;
    private static final javax.xml.namespace.QName ns1_post$2d$buy$2d$form$2d$gd$2d$additional$2d$info_QNAME = new QName("", "post-buy-form-gd-additional-info");
    private static final javax.xml.namespace.QName ns1_post$2d$buy$2d$form$2d$sent$2d$by$2d$seller_QNAME = new QName("", "post-buy-form-sent-by-seller");
    private static final int myPOSTBUYFORMSELLERID_INDEX = 0;
    private static final int myPOSTBUYFORMSELLERNAME_INDEX = 1;
    private static final int myPOSTBUYFORMITEMS_INDEX = 2;
    private static final int myPOSTBUYFORMSHIPMENTID_INDEX = 3;
    private static final int myPOSTBUYFORMPOSTAGEAMOUNT_INDEX = 4;
    private static final int myPOSTBUYFORMMSGTOSELLER_INDEX = 5;
    private static final int myPOSTBUYFORMAMOUNT_INDEX = 6;
    private static final int myPOSTBUYFORMSURCHARGESLIST_INDEX = 7;
    private static final int myPOSTBUYFORMSHIPMENTTRACKING_INDEX = 8;
    private static final int myPOSTBUYFORMGDADDRESS_INDEX = 9;
    private static final int myPOSTBUYFORMGDADDITIONALINFO_INDEX = 10;
    private static final int myPOSTBUYFORMSENTBYSELLER_INDEX = 11;
    
    public PostBuyFormSellersStruct_SOAPSerializer(QName type, boolean encodeType, boolean isNullable, String encodingStyle) {
        super(type, encodeType, isNullable, encodingStyle);
    }
    
    public void initialize(InternalTypeMappingRegistry registry) throws java.lang.Exception {
        ns2_myns2__int__int_Int_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, int.class, ns2_int_TYPE_QNAME);
        ns2_myns2_string__java_lang_String_String_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, java.lang.String.class, ns2_string_TYPE_QNAME);
        ns3_myns3_ArrayOfPostBuyFormItemStruct__PostBuyFormItemStructArray_SOAPSerializer1 = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, AllegroWebApi.PostBuyFormItemStruct[].class, ns3_ArrayOfPostBuyFormItemStruct_TYPE_QNAME);
        ns2_myns2__float__float_Float_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, float.class, ns2_float_TYPE_QNAME);
        ns3_myns3_ArrayOfPostBuyFormSurcharges__longArray_SOAPSerializer1 = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, long[].class, ns3_ArrayOfPostBuyFormSurcharges_TYPE_QNAME);
        ns3_myns3_ArrayOfPostBuyFormShipmentTrackingStruct__PostBuyFormShipmentTrackingStructArray_SOAPSerializer1 = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, AllegroWebApi.PostBuyFormShipmentTrackingStruct[].class, ns3_ArrayOfPostBuyFormShipmentTrackingStruct_TYPE_QNAME);
        ns3_myPostBuyFormAddressStruct_SOAPSerializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, AllegroWebApi.PostBuyFormAddressStruct.class, ns3_PostBuyFormAddressStruct_TYPE_QNAME);
    }
    
    public java.lang.Object doDeserialize(SOAPDeserializationState state, XMLReader reader,
        SOAPDeserializationContext context) throws java.lang.Exception {
        AllegroWebApi.PostBuyFormSellersStruct instance = new AllegroWebApi.PostBuyFormSellersStruct();
        AllegroWebApi.PostBuyFormSellersStruct_SOAPBuilder builder = null;
        java.lang.Object member;
        boolean isComplete = true;
        javax.xml.namespace.QName elementName;
        
        reader.nextElementContent();
        for (int i=0; i<12; i++) {
            elementName = reader.getName();
            if (reader.getState() == XMLReader.END) {
                break;
            }
            if (elementName.equals(ns1_post$2d$buy$2d$form$2d$seller$2d$id_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_post$2d$buy$2d$form$2d$seller$2d$id_QNAME, reader, context);
                instance.setPostBuyFormSellerId(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_post$2d$buy$2d$form$2d$seller$2d$name_QNAME)) {
                member = ns2_myns2_string__java_lang_String_String_Serializer.deserialize(ns1_post$2d$buy$2d$form$2d$seller$2d$name_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.PostBuyFormSellersStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myPOSTBUYFORMSELLERNAME_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setPostBuyFormSellerName((java.lang.String)member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_post$2d$buy$2d$form$2d$items_QNAME)) {
                member = ns3_myns3_ArrayOfPostBuyFormItemStruct__PostBuyFormItemStructArray_SOAPSerializer1.deserialize(ns1_post$2d$buy$2d$form$2d$items_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.PostBuyFormSellersStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myPOSTBUYFORMITEMS_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setPostBuyFormItems((AllegroWebApi.PostBuyFormItemStruct[])member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_post$2d$buy$2d$form$2d$shipment$2d$id_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_post$2d$buy$2d$form$2d$shipment$2d$id_QNAME, reader, context);
                instance.setPostBuyFormShipmentId(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_post$2d$buy$2d$form$2d$postage$2d$amount_QNAME)) {
                member = ns2_myns2__float__float_Float_Serializer.deserialize(ns1_post$2d$buy$2d$form$2d$postage$2d$amount_QNAME, reader, context);
                instance.setPostBuyFormPostageAmount(((Float)member).floatValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_post$2d$buy$2d$form$2d$msg$2d$to$2d$seller_QNAME)) {
                member = ns2_myns2_string__java_lang_String_String_Serializer.deserialize(ns1_post$2d$buy$2d$form$2d$msg$2d$to$2d$seller_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.PostBuyFormSellersStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myPOSTBUYFORMMSGTOSELLER_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setPostBuyFormMsgToSeller((java.lang.String)member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_post$2d$buy$2d$form$2d$amount_QNAME)) {
                member = ns2_myns2__float__float_Float_Serializer.deserialize(ns1_post$2d$buy$2d$form$2d$amount_QNAME, reader, context);
                instance.setPostBuyFormAmount(((Float)member).floatValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_post$2d$buy$2d$form$2d$surcharges$2d$list_QNAME)) {
                member = ns3_myns3_ArrayOfPostBuyFormSurcharges__longArray_SOAPSerializer1.deserialize(ns1_post$2d$buy$2d$form$2d$surcharges$2d$list_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.PostBuyFormSellersStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myPOSTBUYFORMSURCHARGESLIST_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setPostBuyFormSurchargesList((long[])member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_post$2d$buy$2d$form$2d$shipment$2d$tracking_QNAME)) {
                member = ns3_myns3_ArrayOfPostBuyFormShipmentTrackingStruct__PostBuyFormShipmentTrackingStructArray_SOAPSerializer1.deserialize(ns1_post$2d$buy$2d$form$2d$shipment$2d$tracking_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.PostBuyFormSellersStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myPOSTBUYFORMSHIPMENTTRACKING_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setPostBuyFormShipmentTracking((AllegroWebApi.PostBuyFormShipmentTrackingStruct[])member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_post$2d$buy$2d$form$2d$gd$2d$address_QNAME)) {
                member = ns3_myPostBuyFormAddressStruct_SOAPSerializer.deserialize(ns1_post$2d$buy$2d$form$2d$gd$2d$address_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.PostBuyFormSellersStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myPOSTBUYFORMGDADDRESS_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setPostBuyFormGdAddress((AllegroWebApi.PostBuyFormAddressStruct)member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_post$2d$buy$2d$form$2d$gd$2d$additional$2d$info_QNAME)) {
                member = ns2_myns2_string__java_lang_String_String_Serializer.deserialize(ns1_post$2d$buy$2d$form$2d$gd$2d$additional$2d$info_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.PostBuyFormSellersStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myPOSTBUYFORMGDADDITIONALINFO_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setPostBuyFormGdAdditionalInfo((java.lang.String)member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_post$2d$buy$2d$form$2d$sent$2d$by$2d$seller_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_post$2d$buy$2d$form$2d$sent$2d$by$2d$seller_QNAME, reader, context);
                instance.setPostBuyFormSentBySeller(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            } else {
                throw new DeserializationException("soap.unexpectedElementName", new Object[] {ns1_post$2d$buy$2d$form$2d$sent$2d$by$2d$seller_QNAME, elementName});
            }
        }
        
        XMLReaderUtil.verifyReaderState(reader, XMLReader.END);
        return (isComplete ? (java.lang.Object)instance : (java.lang.Object)state);
    }
    
    public void doSerializeAttributes(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws java.lang.Exception {
        AllegroWebApi.PostBuyFormSellersStruct instance = (AllegroWebApi.PostBuyFormSellersStruct)obj;
        
    }
    
    public void doSerializeInstance(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws java.lang.Exception {
        AllegroWebApi.PostBuyFormSellersStruct instance = (AllegroWebApi.PostBuyFormSellersStruct)obj;
        
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getPostBuyFormSellerId()), ns1_post$2d$buy$2d$form$2d$seller$2d$id_QNAME, null, writer, context);
        ns2_myns2_string__java_lang_String_String_Serializer.serialize(instance.getPostBuyFormSellerName(), ns1_post$2d$buy$2d$form$2d$seller$2d$name_QNAME, null, writer, context);
        ns3_myns3_ArrayOfPostBuyFormItemStruct__PostBuyFormItemStructArray_SOAPSerializer1.serialize(instance.getPostBuyFormItems(), ns1_post$2d$buy$2d$form$2d$items_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getPostBuyFormShipmentId()), ns1_post$2d$buy$2d$form$2d$shipment$2d$id_QNAME, null, writer, context);
        ns2_myns2__float__float_Float_Serializer.serialize(new Float(instance.getPostBuyFormPostageAmount()), ns1_post$2d$buy$2d$form$2d$postage$2d$amount_QNAME, null, writer, context);
        ns2_myns2_string__java_lang_String_String_Serializer.serialize(instance.getPostBuyFormMsgToSeller(), ns1_post$2d$buy$2d$form$2d$msg$2d$to$2d$seller_QNAME, null, writer, context);
        ns2_myns2__float__float_Float_Serializer.serialize(new Float(instance.getPostBuyFormAmount()), ns1_post$2d$buy$2d$form$2d$amount_QNAME, null, writer, context);
        ns3_myns3_ArrayOfPostBuyFormSurcharges__longArray_SOAPSerializer1.serialize(instance.getPostBuyFormSurchargesList(), ns1_post$2d$buy$2d$form$2d$surcharges$2d$list_QNAME, null, writer, context);
        ns3_myns3_ArrayOfPostBuyFormShipmentTrackingStruct__PostBuyFormShipmentTrackingStructArray_SOAPSerializer1.serialize(instance.getPostBuyFormShipmentTracking(), ns1_post$2d$buy$2d$form$2d$shipment$2d$tracking_QNAME, null, writer, context);
        ns3_myPostBuyFormAddressStruct_SOAPSerializer.serialize(instance.getPostBuyFormGdAddress(), ns1_post$2d$buy$2d$form$2d$gd$2d$address_QNAME, null, writer, context);
        ns2_myns2_string__java_lang_String_String_Serializer.serialize(instance.getPostBuyFormGdAdditionalInfo(), ns1_post$2d$buy$2d$form$2d$gd$2d$additional$2d$info_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getPostBuyFormSentBySeller()), ns1_post$2d$buy$2d$form$2d$sent$2d$by$2d$seller_QNAME, null, writer, context);
    }
}
