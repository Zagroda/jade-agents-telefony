// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;

import com.sun.xml.rpc.encoding.*;
import com.sun.xml.rpc.encoding.literal.DetailFragmentDeserializer;
import com.sun.xml.rpc.encoding.simpletype.*;
import com.sun.xml.rpc.encoding.soap.SOAPConstants;
import com.sun.xml.rpc.encoding.soap.SOAP12Constants;
import com.sun.xml.rpc.streaming.*;
import com.sun.xml.rpc.wsdl.document.schema.SchemaConstants;
import javax.xml.namespace.QName;

public class UserPayoutStruct_SOAPSerializer extends ObjectSerializerBase implements Initializable {
    private static final javax.xml.namespace.QName ns1_pay$2d$trans$2d$id_QNAME = new QName("", "pay-trans-id");
    private static final javax.xml.namespace.QName ns2_long_TYPE_QNAME = SchemaConstants.QNAME_TYPE_LONG;
    private CombinedSerializer ns2_myns2__long__long_Long_Serializer;
    private static final javax.xml.namespace.QName ns1_pay$2d$trans$2d$status_QNAME = new QName("", "pay-trans-status");
    private static final javax.xml.namespace.QName ns2_string_TYPE_QNAME = SchemaConstants.QNAME_TYPE_STRING;
    private CombinedSerializer ns2_myns2_string__java_lang_String_String_Serializer;
    private static final javax.xml.namespace.QName ns1_pay$2d$trans$2d$amount_QNAME = new QName("", "pay-trans-amount");
    private static final javax.xml.namespace.QName ns2_float_TYPE_QNAME = SchemaConstants.QNAME_TYPE_FLOAT;
    private CombinedSerializer ns2_myns2__float__float_Float_Serializer;
    private static final javax.xml.namespace.QName ns1_pay$2d$trans$2d$create$2d$date_QNAME = new QName("", "pay-trans-create-date");
    private static final javax.xml.namespace.QName ns1_pay$2d$trans$2d$recv$2d$date_QNAME = new QName("", "pay-trans-recv-date");
    private static final javax.xml.namespace.QName ns1_pay$2d$trans$2d$cancel$2d$date_QNAME = new QName("", "pay-trans-cancel-date");
    private static final javax.xml.namespace.QName ns1_pay$2d$trans$2d$report_QNAME = new QName("", "pay-trans-report");
    private static final int myPAYTRANSID_INDEX = 0;
    private static final int myPAYTRANSSTATUS_INDEX = 1;
    private static final int myPAYTRANSAMOUNT_INDEX = 2;
    private static final int myPAYTRANSCREATEDATE_INDEX = 3;
    private static final int myPAYTRANSRECVDATE_INDEX = 4;
    private static final int myPAYTRANSCANCELDATE_INDEX = 5;
    private static final int myPAYTRANSREPORT_INDEX = 6;
    
    public UserPayoutStruct_SOAPSerializer(QName type, boolean encodeType, boolean isNullable, String encodingStyle) {
        super(type, encodeType, isNullable, encodingStyle);
    }
    
    public void initialize(InternalTypeMappingRegistry registry) throws java.lang.Exception {
        ns2_myns2__long__long_Long_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, long.class, ns2_long_TYPE_QNAME);
        ns2_myns2_string__java_lang_String_String_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, java.lang.String.class, ns2_string_TYPE_QNAME);
        ns2_myns2__float__float_Float_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, float.class, ns2_float_TYPE_QNAME);
    }
    
    public java.lang.Object doDeserialize(SOAPDeserializationState state, XMLReader reader,
        SOAPDeserializationContext context) throws java.lang.Exception {
        AllegroWebApi.UserPayoutStruct instance = new AllegroWebApi.UserPayoutStruct();
        AllegroWebApi.UserPayoutStruct_SOAPBuilder builder = null;
        java.lang.Object member;
        boolean isComplete = true;
        javax.xml.namespace.QName elementName;
        
        reader.nextElementContent();
        for (int i=0; i<7; i++) {
            elementName = reader.getName();
            if (reader.getState() == XMLReader.END) {
                break;
            }
            if (elementName.equals(ns1_pay$2d$trans$2d$id_QNAME)) {
                member = ns2_myns2__long__long_Long_Serializer.deserialize(ns1_pay$2d$trans$2d$id_QNAME, reader, context);
                instance.setPayTransId(((Long)member).longValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_pay$2d$trans$2d$status_QNAME)) {
                member = ns2_myns2_string__java_lang_String_String_Serializer.deserialize(ns1_pay$2d$trans$2d$status_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.UserPayoutStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myPAYTRANSSTATUS_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setPayTransStatus((java.lang.String)member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_pay$2d$trans$2d$amount_QNAME)) {
                member = ns2_myns2__float__float_Float_Serializer.deserialize(ns1_pay$2d$trans$2d$amount_QNAME, reader, context);
                instance.setPayTransAmount(((Float)member).floatValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_pay$2d$trans$2d$create$2d$date_QNAME)) {
                member = ns2_myns2__long__long_Long_Serializer.deserialize(ns1_pay$2d$trans$2d$create$2d$date_QNAME, reader, context);
                instance.setPayTransCreateDate(((Long)member).longValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_pay$2d$trans$2d$recv$2d$date_QNAME)) {
                member = ns2_myns2__long__long_Long_Serializer.deserialize(ns1_pay$2d$trans$2d$recv$2d$date_QNAME, reader, context);
                instance.setPayTransRecvDate(((Long)member).longValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_pay$2d$trans$2d$cancel$2d$date_QNAME)) {
                member = ns2_myns2__long__long_Long_Serializer.deserialize(ns1_pay$2d$trans$2d$cancel$2d$date_QNAME, reader, context);
                instance.setPayTransCancelDate(((Long)member).longValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_pay$2d$trans$2d$report_QNAME)) {
                member = ns2_myns2_string__java_lang_String_String_Serializer.deserialize(ns1_pay$2d$trans$2d$report_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.UserPayoutStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myPAYTRANSREPORT_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setPayTransReport((java.lang.String)member);
                }
                reader.nextElementContent();
                continue;
            } else {
                throw new DeserializationException("soap.unexpectedElementName", new Object[] {ns1_pay$2d$trans$2d$report_QNAME, elementName});
            }
        }
        
        XMLReaderUtil.verifyReaderState(reader, XMLReader.END);
        return (isComplete ? (java.lang.Object)instance : (java.lang.Object)state);
    }
    
    public void doSerializeAttributes(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws java.lang.Exception {
        AllegroWebApi.UserPayoutStruct instance = (AllegroWebApi.UserPayoutStruct)obj;
        
    }
    
    public void doSerializeInstance(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws java.lang.Exception {
        AllegroWebApi.UserPayoutStruct instance = (AllegroWebApi.UserPayoutStruct)obj;
        
        ns2_myns2__long__long_Long_Serializer.serialize(new Long(instance.getPayTransId()), ns1_pay$2d$trans$2d$id_QNAME, null, writer, context);
        ns2_myns2_string__java_lang_String_String_Serializer.serialize(instance.getPayTransStatus(), ns1_pay$2d$trans$2d$status_QNAME, null, writer, context);
        ns2_myns2__float__float_Float_Serializer.serialize(new Float(instance.getPayTransAmount()), ns1_pay$2d$trans$2d$amount_QNAME, null, writer, context);
        ns2_myns2__long__long_Long_Serializer.serialize(new Long(instance.getPayTransCreateDate()), ns1_pay$2d$trans$2d$create$2d$date_QNAME, null, writer, context);
        ns2_myns2__long__long_Long_Serializer.serialize(new Long(instance.getPayTransRecvDate()), ns1_pay$2d$trans$2d$recv$2d$date_QNAME, null, writer, context);
        ns2_myns2__long__long_Long_Serializer.serialize(new Long(instance.getPayTransCancelDate()), ns1_pay$2d$trans$2d$cancel$2d$date_QNAME, null, writer, context);
        ns2_myns2_string__java_lang_String_String_Serializer.serialize(instance.getPayTransReport(), ns1_pay$2d$trans$2d$report_QNAME, null, writer, context);
    }
}
