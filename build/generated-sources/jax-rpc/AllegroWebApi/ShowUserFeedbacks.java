// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class ShowUserFeedbacks {
    protected int userFLastWeek;
    protected int userFLastMonth;
    protected int userFAll;
    protected int userFSoldItems;
    protected int userFBuyItems;
    
    public ShowUserFeedbacks() {
    }
    
    public ShowUserFeedbacks(int userFLastWeek, int userFLastMonth, int userFAll, int userFSoldItems, int userFBuyItems) {
        this.userFLastWeek = userFLastWeek;
        this.userFLastMonth = userFLastMonth;
        this.userFAll = userFAll;
        this.userFSoldItems = userFSoldItems;
        this.userFBuyItems = userFBuyItems;
    }
    
    public int getUserFLastWeek() {
        return userFLastWeek;
    }
    
    public void setUserFLastWeek(int userFLastWeek) {
        this.userFLastWeek = userFLastWeek;
    }
    
    public int getUserFLastMonth() {
        return userFLastMonth;
    }
    
    public void setUserFLastMonth(int userFLastMonth) {
        this.userFLastMonth = userFLastMonth;
    }
    
    public int getUserFAll() {
        return userFAll;
    }
    
    public void setUserFAll(int userFAll) {
        this.userFAll = userFAll;
    }
    
    public int getUserFSoldItems() {
        return userFSoldItems;
    }
    
    public void setUserFSoldItems(int userFSoldItems) {
        this.userFSoldItems = userFSoldItems;
    }
    
    public int getUserFBuyItems() {
        return userFBuyItems;
    }
    
    public void setUserFBuyItems(int userFBuyItems) {
        this.userFBuyItems = userFBuyItems;
    }
}
