// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class AddressInfoStruct {
    protected int addressType;
    protected AllegroWebApi.AddressUserDataStruct addressUserData;
    
    public AddressInfoStruct() {
    }
    
    public AddressInfoStruct(int addressType, AllegroWebApi.AddressUserDataStruct addressUserData) {
        this.addressType = addressType;
        this.addressUserData = addressUserData;
    }
    
    public int getAddressType() {
        return addressType;
    }
    
    public void setAddressType(int addressType) {
        this.addressType = addressType;
    }
    
    public AllegroWebApi.AddressUserDataStruct getAddressUserData() {
        return addressUserData;
    }
    
    public void setAddressUserData(AllegroWebApi.AddressUserDataStruct addressUserData) {
        this.addressUserData = addressUserData;
    }
}
