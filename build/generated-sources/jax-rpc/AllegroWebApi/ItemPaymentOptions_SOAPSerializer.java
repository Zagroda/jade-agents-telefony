// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;

import com.sun.xml.rpc.encoding.*;
import com.sun.xml.rpc.encoding.literal.DetailFragmentDeserializer;
import com.sun.xml.rpc.encoding.simpletype.*;
import com.sun.xml.rpc.encoding.soap.SOAPConstants;
import com.sun.xml.rpc.encoding.soap.SOAP12Constants;
import com.sun.xml.rpc.streaming.*;
import com.sun.xml.rpc.wsdl.document.schema.SchemaConstants;
import javax.xml.namespace.QName;

public class ItemPaymentOptions_SOAPSerializer extends ObjectSerializerBase implements Initializable {
    private static final javax.xml.namespace.QName ns1_pay$2d$option$2d$transfer_QNAME = new QName("", "pay-option-transfer");
    private static final javax.xml.namespace.QName ns2_int_TYPE_QNAME = SchemaConstants.QNAME_TYPE_INT;
    private CombinedSerializer ns2_myns2__int__int_Int_Serializer;
    private static final javax.xml.namespace.QName ns1_pay$2d$option$2d$on$2d$delivery_QNAME = new QName("", "pay-option-on-delivery");
    private static final javax.xml.namespace.QName ns1_pay$2d$option$2d$allegro$2d$pay_QNAME = new QName("", "pay-option-allegro-pay");
    private static final javax.xml.namespace.QName ns1_pay$2d$option$2d$see$2d$desc_QNAME = new QName("", "pay-option-see-desc");
    private static final javax.xml.namespace.QName ns1_pay$2d$option$2d$payu_QNAME = new QName("", "pay-option-payu");
    private static final javax.xml.namespace.QName ns1_pay$2d$option$2d$escrow_QNAME = new QName("", "pay-option-escrow");
    private static final javax.xml.namespace.QName ns1_pay$2d$option$2d$qiwi_QNAME = new QName("", "pay-option-qiwi");
    private static final int myPAYOPTIONTRANSFER_INDEX = 0;
    private static final int myPAYOPTIONONDELIVERY_INDEX = 1;
    private static final int myPAYOPTIONALLEGROPAY_INDEX = 2;
    private static final int myPAYOPTIONSEEDESC_INDEX = 3;
    private static final int myPAYOPTIONPAYU_INDEX = 4;
    private static final int myPAYOPTIONESCROW_INDEX = 5;
    private static final int myPAYOPTIONQIWI_INDEX = 6;
    
    public ItemPaymentOptions_SOAPSerializer(QName type, boolean encodeType, boolean isNullable, String encodingStyle) {
        super(type, encodeType, isNullable, encodingStyle);
    }
    
    public void initialize(InternalTypeMappingRegistry registry) throws java.lang.Exception {
        ns2_myns2__int__int_Int_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, int.class, ns2_int_TYPE_QNAME);
    }
    
    public java.lang.Object doDeserialize(SOAPDeserializationState state, XMLReader reader,
        SOAPDeserializationContext context) throws java.lang.Exception {
        AllegroWebApi.ItemPaymentOptions instance = new AllegroWebApi.ItemPaymentOptions();
        java.lang.Object member;
        boolean isComplete = true;
        javax.xml.namespace.QName elementName;
        
        reader.nextElementContent();
        for (int i=0; i<7; i++) {
            elementName = reader.getName();
            if (reader.getState() == XMLReader.END) {
                break;
            }
            if (elementName.equals(ns1_pay$2d$option$2d$transfer_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_pay$2d$option$2d$transfer_QNAME, reader, context);
                instance.setPayOptionTransfer(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_pay$2d$option$2d$on$2d$delivery_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_pay$2d$option$2d$on$2d$delivery_QNAME, reader, context);
                instance.setPayOptionOnDelivery(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_pay$2d$option$2d$allegro$2d$pay_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_pay$2d$option$2d$allegro$2d$pay_QNAME, reader, context);
                instance.setPayOptionAllegroPay(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_pay$2d$option$2d$see$2d$desc_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_pay$2d$option$2d$see$2d$desc_QNAME, reader, context);
                instance.setPayOptionSeeDesc(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_pay$2d$option$2d$payu_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_pay$2d$option$2d$payu_QNAME, reader, context);
                instance.setPayOptionPayu(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_pay$2d$option$2d$escrow_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_pay$2d$option$2d$escrow_QNAME, reader, context);
                instance.setPayOptionEscrow(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_pay$2d$option$2d$qiwi_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_pay$2d$option$2d$qiwi_QNAME, reader, context);
                instance.setPayOptionQiwi(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            } else {
                throw new DeserializationException("soap.unexpectedElementName", new Object[] {ns1_pay$2d$option$2d$qiwi_QNAME, elementName});
            }
        }
        
        XMLReaderUtil.verifyReaderState(reader, XMLReader.END);
        return (isComplete ? (java.lang.Object)instance : (java.lang.Object)state);
    }
    
    public void doSerializeAttributes(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws java.lang.Exception {
        AllegroWebApi.ItemPaymentOptions instance = (AllegroWebApi.ItemPaymentOptions)obj;
        
    }
    
    public void doSerializeInstance(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws java.lang.Exception {
        AllegroWebApi.ItemPaymentOptions instance = (AllegroWebApi.ItemPaymentOptions)obj;
        
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getPayOptionTransfer()), ns1_pay$2d$option$2d$transfer_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getPayOptionOnDelivery()), ns1_pay$2d$option$2d$on$2d$delivery_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getPayOptionAllegroPay()), ns1_pay$2d$option$2d$allegro$2d$pay_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getPayOptionSeeDesc()), ns1_pay$2d$option$2d$see$2d$desc_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getPayOptionPayu()), ns1_pay$2d$option$2d$payu_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getPayOptionEscrow()), ns1_pay$2d$option$2d$escrow_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getPayOptionQiwi()), ns1_pay$2d$option$2d$qiwi_QNAME, null, writer, context);
    }
}
