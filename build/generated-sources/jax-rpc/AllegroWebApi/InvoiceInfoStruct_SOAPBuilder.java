// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;

import com.sun.xml.rpc.encoding.*;
import com.sun.xml.rpc.util.exception.LocalizableExceptionAdapter;

public class InvoiceInfoStruct_SOAPBuilder implements SOAPInstanceBuilder {
    private AllegroWebApi.InvoiceInfoStruct _instance;
    private int invoiceAddressType;
    private AllegroWebApi.AddressUserDataStruct invoiceAddressData;
    private java.lang.String invoiceNip;
    private static final int myINVOICEADDRESSTYPE_INDEX = 0;
    private static final int myINVOICEADDRESSDATA_INDEX = 1;
    private static final int myINVOICENIP_INDEX = 2;
    
    public InvoiceInfoStruct_SOAPBuilder() {
    }
    
    public void setInvoiceAddressType(int invoiceAddressType) {
        this.invoiceAddressType = invoiceAddressType;
    }
    
    public void setInvoiceAddressData(AllegroWebApi.AddressUserDataStruct invoiceAddressData) {
        this.invoiceAddressData = invoiceAddressData;
    }
    
    public void setInvoiceNip(java.lang.String invoiceNip) {
        this.invoiceNip = invoiceNip;
    }
    
    public int memberGateType(int memberIndex) {
        switch (memberIndex) {
            case myINVOICEADDRESSDATA_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            case myINVOICENIP_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            default:
                throw new IllegalArgumentException();
        }
    }
    
    public void construct() {
    }
    
    public void setMember(int index, java.lang.Object memberValue) {
        try {
            switch(index) {
                case myINVOICEADDRESSDATA_INDEX:
                    _instance.setInvoiceAddressData((AllegroWebApi.AddressUserDataStruct)memberValue);
                    break;
                case myINVOICENIP_INDEX:
                    _instance.setInvoiceNip((java.lang.String)memberValue);
                    break;
                default:
                    throw new java.lang.IllegalArgumentException();
            }
        }
        catch (java.lang.RuntimeException e) {
            throw e;
        }
        catch (java.lang.Exception e) {
            throw new DeserializationException(new LocalizableExceptionAdapter(e));
        }
    }
    
    public void initialize() {
    }
    
    public void setInstance(java.lang.Object instance) {
        _instance = (AllegroWebApi.InvoiceInfoStruct)instance;
    }
    
    public java.lang.Object getInstance() {
        return _instance;
    }
}
