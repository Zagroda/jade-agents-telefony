// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;

import com.sun.xml.rpc.encoding.*;
import com.sun.xml.rpc.encoding.literal.DetailFragmentDeserializer;
import com.sun.xml.rpc.encoding.simpletype.*;
import com.sun.xml.rpc.encoding.soap.SOAPConstants;
import com.sun.xml.rpc.encoding.soap.SOAP12Constants;
import com.sun.xml.rpc.streaming.*;
import com.sun.xml.rpc.wsdl.document.schema.SchemaConstants;
import javax.xml.namespace.QName;

public class SellRatingInfoStruct_SOAPSerializer extends ObjectSerializerBase implements Initializable {
    private static final javax.xml.namespace.QName ns1_sell$2d$rating$2d$group$2d$id_QNAME = new QName("", "sell-rating-group-id");
    private static final javax.xml.namespace.QName ns2_int_TYPE_QNAME = SchemaConstants.QNAME_TYPE_INT;
    private CombinedSerializer ns2_myns2__int__int_Int_Serializer;
    private static final javax.xml.namespace.QName ns1_sell$2d$rating$2d$group$2d$title_QNAME = new QName("", "sell-rating-group-title");
    private static final javax.xml.namespace.QName ns2_string_TYPE_QNAME = SchemaConstants.QNAME_TYPE_STRING;
    private CombinedSerializer ns2_myns2_string__java_lang_String_String_Serializer;
    private static final javax.xml.namespace.QName ns1_sell$2d$rating$2d$reasons_QNAME = new QName("", "sell-rating-reasons");
    private static final javax.xml.namespace.QName ns3_ArrayOfSellRatingReasonStruct_TYPE_QNAME = new QName("urn:AllegroWebApi", "ArrayOfSellRatingReasonStruct");
    private CombinedSerializer ns3_myns3_ArrayOfSellRatingReasonStruct__SellRatingReasonStructArray_SOAPSerializer1;
    private static final int mySELLRATINGGROUPID_INDEX = 0;
    private static final int mySELLRATINGGROUPTITLE_INDEX = 1;
    private static final int mySELLRATINGREASONS_INDEX = 2;
    
    public SellRatingInfoStruct_SOAPSerializer(QName type, boolean encodeType, boolean isNullable, String encodingStyle) {
        super(type, encodeType, isNullable, encodingStyle);
    }
    
    public void initialize(InternalTypeMappingRegistry registry) throws java.lang.Exception {
        ns2_myns2__int__int_Int_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, int.class, ns2_int_TYPE_QNAME);
        ns2_myns2_string__java_lang_String_String_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, java.lang.String.class, ns2_string_TYPE_QNAME);
        ns3_myns3_ArrayOfSellRatingReasonStruct__SellRatingReasonStructArray_SOAPSerializer1 = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, AllegroWebApi.SellRatingReasonStruct[].class, ns3_ArrayOfSellRatingReasonStruct_TYPE_QNAME);
    }
    
    public java.lang.Object doDeserialize(SOAPDeserializationState state, XMLReader reader,
        SOAPDeserializationContext context) throws java.lang.Exception {
        AllegroWebApi.SellRatingInfoStruct instance = new AllegroWebApi.SellRatingInfoStruct();
        AllegroWebApi.SellRatingInfoStruct_SOAPBuilder builder = null;
        java.lang.Object member;
        boolean isComplete = true;
        javax.xml.namespace.QName elementName;
        
        reader.nextElementContent();
        for (int i=0; i<3; i++) {
            elementName = reader.getName();
            if (reader.getState() == XMLReader.END) {
                break;
            }
            if (elementName.equals(ns1_sell$2d$rating$2d$group$2d$id_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_sell$2d$rating$2d$group$2d$id_QNAME, reader, context);
                instance.setSellRatingGroupId(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_sell$2d$rating$2d$group$2d$title_QNAME)) {
                member = ns2_myns2_string__java_lang_String_String_Serializer.deserialize(ns1_sell$2d$rating$2d$group$2d$title_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.SellRatingInfoStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, mySELLRATINGGROUPTITLE_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setSellRatingGroupTitle((java.lang.String)member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_sell$2d$rating$2d$reasons_QNAME)) {
                member = ns3_myns3_ArrayOfSellRatingReasonStruct__SellRatingReasonStructArray_SOAPSerializer1.deserialize(ns1_sell$2d$rating$2d$reasons_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.SellRatingInfoStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, mySELLRATINGREASONS_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setSellRatingReasons((AllegroWebApi.SellRatingReasonStruct[])member);
                }
                reader.nextElementContent();
                continue;
            } else {
                throw new DeserializationException("soap.unexpectedElementName", new Object[] {ns1_sell$2d$rating$2d$reasons_QNAME, elementName});
            }
        }
        
        XMLReaderUtil.verifyReaderState(reader, XMLReader.END);
        return (isComplete ? (java.lang.Object)instance : (java.lang.Object)state);
    }
    
    public void doSerializeAttributes(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws java.lang.Exception {
        AllegroWebApi.SellRatingInfoStruct instance = (AllegroWebApi.SellRatingInfoStruct)obj;
        
    }
    
    public void doSerializeInstance(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws java.lang.Exception {
        AllegroWebApi.SellRatingInfoStruct instance = (AllegroWebApi.SellRatingInfoStruct)obj;
        
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getSellRatingGroupId()), ns1_sell$2d$rating$2d$group$2d$id_QNAME, null, writer, context);
        ns2_myns2_string__java_lang_String_String_Serializer.serialize(instance.getSellRatingGroupTitle(), ns1_sell$2d$rating$2d$group$2d$title_QNAME, null, writer, context);
        ns3_myns3_ArrayOfSellRatingReasonStruct__SellRatingReasonStructArray_SOAPSerializer1.serialize(instance.getSellRatingReasons(), ns1_sell$2d$rating$2d$reasons_QNAME, null, writer, context);
    }
}
