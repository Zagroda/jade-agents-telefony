// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;

import com.sun.xml.rpc.encoding.*;
import com.sun.xml.rpc.util.exception.LocalizableExceptionAdapter;

public class RelatedPersonsStruct_SOAPBuilder implements SOAPInstanceBuilder {
    private AllegroWebApi.RelatedPersonsStruct _instance;
    private java.lang.String spouseFirstName;
    private java.lang.String spouseLastName;
    private static final int mySPOUSEFIRSTNAME_INDEX = 0;
    private static final int mySPOUSELASTNAME_INDEX = 1;
    
    public RelatedPersonsStruct_SOAPBuilder() {
    }
    
    public void setSpouseFirstName(java.lang.String spouseFirstName) {
        this.spouseFirstName = spouseFirstName;
    }
    
    public void setSpouseLastName(java.lang.String spouseLastName) {
        this.spouseLastName = spouseLastName;
    }
    
    public int memberGateType(int memberIndex) {
        switch (memberIndex) {
            case mySPOUSEFIRSTNAME_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            case mySPOUSELASTNAME_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            default:
                throw new IllegalArgumentException();
        }
    }
    
    public void construct() {
    }
    
    public void setMember(int index, java.lang.Object memberValue) {
        try {
            switch(index) {
                case mySPOUSEFIRSTNAME_INDEX:
                    _instance.setSpouseFirstName((java.lang.String)memberValue);
                    break;
                case mySPOUSELASTNAME_INDEX:
                    _instance.setSpouseLastName((java.lang.String)memberValue);
                    break;
                default:
                    throw new java.lang.IllegalArgumentException();
            }
        }
        catch (java.lang.RuntimeException e) {
            throw e;
        }
        catch (java.lang.Exception e) {
            throw new DeserializationException(new LocalizableExceptionAdapter(e));
        }
    }
    
    public void initialize() {
    }
    
    public void setInstance(java.lang.Object instance) {
        _instance = (AllegroWebApi.RelatedPersonsStruct)instance;
    }
    
    public java.lang.Object getInstance() {
        return _instance;
    }
}
