// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class SellRatingDetailStruct {
    protected java.lang.String sellRatingGroupTitle;
    protected AllegroWebApi.SellRatingReasonSumStruct[] sellRatingReasonsSummary;
    
    public SellRatingDetailStruct() {
    }
    
    public SellRatingDetailStruct(java.lang.String sellRatingGroupTitle, AllegroWebApi.SellRatingReasonSumStruct[] sellRatingReasonsSummary) {
        this.sellRatingGroupTitle = sellRatingGroupTitle;
        this.sellRatingReasonsSummary = sellRatingReasonsSummary;
    }
    
    public java.lang.String getSellRatingGroupTitle() {
        return sellRatingGroupTitle;
    }
    
    public void setSellRatingGroupTitle(java.lang.String sellRatingGroupTitle) {
        this.sellRatingGroupTitle = sellRatingGroupTitle;
    }
    
    public AllegroWebApi.SellRatingReasonSumStruct[] getSellRatingReasonsSummary() {
        return sellRatingReasonsSummary;
    }
    
    public void setSellRatingReasonsSummary(AllegroWebApi.SellRatingReasonSumStruct[] sellRatingReasonsSummary) {
        this.sellRatingReasonsSummary = sellRatingReasonsSummary;
    }
}
