// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class AllegroWebApiPortType_doSetShipmentPriceType_RequestStruct {
    protected java.lang.String sessionId;
    protected int shipmentPriceTypeId;
    
    public AllegroWebApiPortType_doSetShipmentPriceType_RequestStruct() {
    }
    
    public AllegroWebApiPortType_doSetShipmentPriceType_RequestStruct(java.lang.String sessionId, int shipmentPriceTypeId) {
        this.sessionId = sessionId;
        this.shipmentPriceTypeId = shipmentPriceTypeId;
    }
    
    public java.lang.String getSessionId() {
        return sessionId;
    }
    
    public void setSessionId(java.lang.String sessionId) {
        this.sessionId = sessionId;
    }
    
    public int getShipmentPriceTypeId() {
        return shipmentPriceTypeId;
    }
    
    public void setShipmentPriceTypeId(int shipmentPriceTypeId) {
        this.shipmentPriceTypeId = shipmentPriceTypeId;
    }
}
