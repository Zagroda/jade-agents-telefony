// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;

import com.sun.xml.rpc.encoding.*;
import com.sun.xml.rpc.util.exception.LocalizableExceptionAdapter;

public class AllegroWebApiPortType_doGetPostBuyData_RequestStruct_SOAPBuilder implements SOAPInstanceBuilder {
    private AllegroWebApi.AllegroWebApiPortType_doGetPostBuyData_RequestStruct _instance;
    private java.lang.String sessionHandle;
    private long[] itemsArray;
    private long[] buyerFilterArray;
    private static final int mySESSIONHANDLE_INDEX = 0;
    private static final int myITEMSARRAY_INDEX = 1;
    private static final int myBUYERFILTERARRAY_INDEX = 2;
    
    public AllegroWebApiPortType_doGetPostBuyData_RequestStruct_SOAPBuilder() {
    }
    
    public void setSessionHandle(java.lang.String sessionHandle) {
        this.sessionHandle = sessionHandle;
    }
    
    public void setItemsArray(long[] itemsArray) {
        this.itemsArray = itemsArray;
    }
    
    public void setBuyerFilterArray(long[] buyerFilterArray) {
        this.buyerFilterArray = buyerFilterArray;
    }
    
    public int memberGateType(int memberIndex) {
        switch (memberIndex) {
            case mySESSIONHANDLE_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            case myITEMSARRAY_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            case myBUYERFILTERARRAY_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            default:
                throw new IllegalArgumentException();
        }
    }
    
    public void construct() {
    }
    
    public void setMember(int index, java.lang.Object memberValue) {
        try {
            switch(index) {
                case mySESSIONHANDLE_INDEX:
                    _instance.setSessionHandle((java.lang.String)memberValue);
                    break;
                case myITEMSARRAY_INDEX:
                    _instance.setItemsArray((long[])memberValue);
                    break;
                case myBUYERFILTERARRAY_INDEX:
                    _instance.setBuyerFilterArray((long[])memberValue);
                    break;
                default:
                    throw new java.lang.IllegalArgumentException();
            }
        }
        catch (java.lang.RuntimeException e) {
            throw e;
        }
        catch (java.lang.Exception e) {
            throw new DeserializationException(new LocalizableExceptionAdapter(e));
        }
    }
    
    public void initialize() {
    }
    
    public void setInstance(java.lang.Object instance) {
        _instance = (AllegroWebApi.AllegroWebApiPortType_doGetPostBuyData_RequestStruct)instance;
    }
    
    public java.lang.Object getInstance() {
        return _instance;
    }
}
