// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;

import com.sun.xml.rpc.encoding.*;
import com.sun.xml.rpc.util.exception.LocalizableExceptionAdapter;

public class UserPostBuyDataStruct_SOAPBuilder implements SOAPInstanceBuilder {
    private AllegroWebApi.UserPostBuyDataStruct _instance;
    private AllegroWebApi.UserDataStruct userData;
    private AllegroWebApi.UserSentToDataStruct userSentToData;
    private java.lang.String[] userBankAccounts;
    private AllegroWebApi.CompanySecondAddressStruct companySecondAddress;
    private static final int myUSERDATA_INDEX = 0;
    private static final int myUSERSENTTODATA_INDEX = 1;
    private static final int myUSERBANKACCOUNTS_INDEX = 2;
    private static final int myCOMPANYSECONDADDRESS_INDEX = 3;
    
    public UserPostBuyDataStruct_SOAPBuilder() {
    }
    
    public void setUserData(AllegroWebApi.UserDataStruct userData) {
        this.userData = userData;
    }
    
    public void setUserSentToData(AllegroWebApi.UserSentToDataStruct userSentToData) {
        this.userSentToData = userSentToData;
    }
    
    public void setUserBankAccounts(java.lang.String[] userBankAccounts) {
        this.userBankAccounts = userBankAccounts;
    }
    
    public void setCompanySecondAddress(AllegroWebApi.CompanySecondAddressStruct companySecondAddress) {
        this.companySecondAddress = companySecondAddress;
    }
    
    public int memberGateType(int memberIndex) {
        switch (memberIndex) {
            case myUSERDATA_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            case myUSERSENTTODATA_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            case myUSERBANKACCOUNTS_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            case myCOMPANYSECONDADDRESS_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            default:
                throw new IllegalArgumentException();
        }
    }
    
    public void construct() {
    }
    
    public void setMember(int index, java.lang.Object memberValue) {
        try {
            switch(index) {
                case myUSERDATA_INDEX:
                    _instance.setUserData((AllegroWebApi.UserDataStruct)memberValue);
                    break;
                case myUSERSENTTODATA_INDEX:
                    _instance.setUserSentToData((AllegroWebApi.UserSentToDataStruct)memberValue);
                    break;
                case myUSERBANKACCOUNTS_INDEX:
                    _instance.setUserBankAccounts((java.lang.String[])memberValue);
                    break;
                case myCOMPANYSECONDADDRESS_INDEX:
                    _instance.setCompanySecondAddress((AllegroWebApi.CompanySecondAddressStruct)memberValue);
                    break;
                default:
                    throw new java.lang.IllegalArgumentException();
            }
        }
        catch (java.lang.RuntimeException e) {
            throw e;
        }
        catch (java.lang.Exception e) {
            throw new DeserializationException(new LocalizableExceptionAdapter(e));
        }
    }
    
    public void initialize() {
    }
    
    public void setInstance(java.lang.Object instance) {
        _instance = (AllegroWebApi.UserPostBuyDataStruct)instance;
    }
    
    public java.lang.Object getInstance() {
        return _instance;
    }
}
