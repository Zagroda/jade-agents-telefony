// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class AllegroWebApiPortType_doMyAccount2_ResponseStruct {
    protected AllegroWebApi.MyAccountStruct2[] myaccountList;
    
    public AllegroWebApiPortType_doMyAccount2_ResponseStruct() {
    }
    
    public AllegroWebApiPortType_doMyAccount2_ResponseStruct(AllegroWebApi.MyAccountStruct2[] myaccountList) {
        this.myaccountList = myaccountList;
    }
    
    public AllegroWebApi.MyAccountStruct2[] getMyaccountList() {
        return myaccountList;
    }
    
    public void setMyaccountList(AllegroWebApi.MyAccountStruct2[] myaccountList) {
        this.myaccountList = myaccountList;
    }
}
