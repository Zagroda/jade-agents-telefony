// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;

import com.sun.xml.rpc.encoding.*;
import com.sun.xml.rpc.encoding.literal.DetailFragmentDeserializer;
import com.sun.xml.rpc.encoding.simpletype.*;
import com.sun.xml.rpc.encoding.soap.SOAPConstants;
import com.sun.xml.rpc.encoding.soap.SOAP12Constants;
import com.sun.xml.rpc.streaming.*;
import com.sun.xml.rpc.wsdl.document.schema.SchemaConstants;
import javax.xml.namespace.QName;

public class PostBuyFormForBuyersDataStruct_SOAPSerializer extends ObjectSerializerBase implements Initializable {
    private static final javax.xml.namespace.QName ns1_post$2d$buy$2d$form$2d$id_QNAME = new QName("", "post-buy-form-id");
    private static final javax.xml.namespace.QName ns2_long_TYPE_QNAME = SchemaConstants.QNAME_TYPE_LONG;
    private CombinedSerializer ns2_myns2__long__long_Long_Serializer;
    private static final javax.xml.namespace.QName ns1_post$2d$buy$2d$form$2d$buyer$2d$id_QNAME = new QName("", "post-buy-form-buyer-id");
    private static final javax.xml.namespace.QName ns2_int_TYPE_QNAME = SchemaConstants.QNAME_TYPE_INT;
    private CombinedSerializer ns2_myns2__int__int_Int_Serializer;
    private static final javax.xml.namespace.QName ns1_post$2d$buy$2d$form$2d$sellers_QNAME = new QName("", "post-buy-form-sellers");
    private static final javax.xml.namespace.QName ns3_ArrayOfPostBuyFormSellersStruct_TYPE_QNAME = new QName("urn:AllegroWebApi", "ArrayOfPostBuyFormSellersStruct");
    private CombinedSerializer ns3_myns3_ArrayOfPostBuyFormSellersStruct__PostBuyFormSellersStructArray_SOAPSerializer1;
    private static final javax.xml.namespace.QName ns1_post$2d$buy$2d$form$2d$total$2d$amount_QNAME = new QName("", "post-buy-form-total-amount");
    private static final javax.xml.namespace.QName ns2_float_TYPE_QNAME = SchemaConstants.QNAME_TYPE_FLOAT;
    private CombinedSerializer ns2_myns2__float__float_Float_Serializer;
    private static final javax.xml.namespace.QName ns1_post$2d$buy$2d$form$2d$invoice$2d$option_QNAME = new QName("", "post-buy-form-invoice-option");
    private static final javax.xml.namespace.QName ns1_post$2d$buy$2d$form$2d$invoice$2d$data_QNAME = new QName("", "post-buy-form-invoice-data");
    private static final javax.xml.namespace.QName ns3_PostBuyFormAddressStruct_TYPE_QNAME = new QName("urn:AllegroWebApi", "PostBuyFormAddressStruct");
    private CombinedSerializer ns3_myPostBuyFormAddressStruct_SOAPSerializer;
    private static final javax.xml.namespace.QName ns1_post$2d$buy$2d$form$2d$shipment$2d$address_QNAME = new QName("", "post-buy-form-shipment-address");
    private static final javax.xml.namespace.QName ns1_post$2d$buy$2d$form$2d$pay$2d$type_QNAME = new QName("", "post-buy-form-pay-type");
    private static final javax.xml.namespace.QName ns2_string_TYPE_QNAME = SchemaConstants.QNAME_TYPE_STRING;
    private CombinedSerializer ns2_myns2_string__java_lang_String_String_Serializer;
    private static final javax.xml.namespace.QName ns1_post$2d$buy$2d$form$2d$pay$2d$id_QNAME = new QName("", "post-buy-form-pay-id");
    private static final javax.xml.namespace.QName ns1_post$2d$buy$2d$form$2d$pay$2d$status_QNAME = new QName("", "post-buy-form-pay-status");
    private static final javax.xml.namespace.QName ns1_post$2d$buy$2d$form$2d$date$2d$init_QNAME = new QName("", "post-buy-form-date-init");
    private static final javax.xml.namespace.QName ns1_post$2d$buy$2d$form$2d$date$2d$recv_QNAME = new QName("", "post-buy-form-date-recv");
    private static final javax.xml.namespace.QName ns1_post$2d$buy$2d$form$2d$date$2d$cancel_QNAME = new QName("", "post-buy-form-date-cancel");
    private static final javax.xml.namespace.QName ns1_post$2d$buy$2d$form$2d$payment$2d$amount_QNAME = new QName("", "post-buy-form-payment-amount");
    private static final int myPOSTBUYFORMID_INDEX = 0;
    private static final int myPOSTBUYFORMBUYERID_INDEX = 1;
    private static final int myPOSTBUYFORMSELLERS_INDEX = 2;
    private static final int myPOSTBUYFORMTOTALAMOUNT_INDEX = 3;
    private static final int myPOSTBUYFORMINVOICEOPTION_INDEX = 4;
    private static final int myPOSTBUYFORMINVOICEDATA_INDEX = 5;
    private static final int myPOSTBUYFORMSHIPMENTADDRESS_INDEX = 6;
    private static final int myPOSTBUYFORMPAYTYPE_INDEX = 7;
    private static final int myPOSTBUYFORMPAYID_INDEX = 8;
    private static final int myPOSTBUYFORMPAYSTATUS_INDEX = 9;
    private static final int myPOSTBUYFORMDATEINIT_INDEX = 10;
    private static final int myPOSTBUYFORMDATERECV_INDEX = 11;
    private static final int myPOSTBUYFORMDATECANCEL_INDEX = 12;
    private static final int myPOSTBUYFORMPAYMENTAMOUNT_INDEX = 13;
    
    public PostBuyFormForBuyersDataStruct_SOAPSerializer(QName type, boolean encodeType, boolean isNullable, String encodingStyle) {
        super(type, encodeType, isNullable, encodingStyle);
    }
    
    public void initialize(InternalTypeMappingRegistry registry) throws java.lang.Exception {
        ns2_myns2__long__long_Long_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, long.class, ns2_long_TYPE_QNAME);
        ns2_myns2__int__int_Int_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, int.class, ns2_int_TYPE_QNAME);
        ns3_myns3_ArrayOfPostBuyFormSellersStruct__PostBuyFormSellersStructArray_SOAPSerializer1 = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, AllegroWebApi.PostBuyFormSellersStruct[].class, ns3_ArrayOfPostBuyFormSellersStruct_TYPE_QNAME);
        ns2_myns2__float__float_Float_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, float.class, ns2_float_TYPE_QNAME);
        ns3_myPostBuyFormAddressStruct_SOAPSerializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, AllegroWebApi.PostBuyFormAddressStruct.class, ns3_PostBuyFormAddressStruct_TYPE_QNAME);
        ns2_myns2_string__java_lang_String_String_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, java.lang.String.class, ns2_string_TYPE_QNAME);
    }
    
    public java.lang.Object doDeserialize(SOAPDeserializationState state, XMLReader reader,
        SOAPDeserializationContext context) throws java.lang.Exception {
        AllegroWebApi.PostBuyFormForBuyersDataStruct instance = new AllegroWebApi.PostBuyFormForBuyersDataStruct();
        AllegroWebApi.PostBuyFormForBuyersDataStruct_SOAPBuilder builder = null;
        java.lang.Object member;
        boolean isComplete = true;
        javax.xml.namespace.QName elementName;
        
        reader.nextElementContent();
        for (int i=0; i<14; i++) {
            elementName = reader.getName();
            if (reader.getState() == XMLReader.END) {
                break;
            }
            if (elementName.equals(ns1_post$2d$buy$2d$form$2d$id_QNAME)) {
                member = ns2_myns2__long__long_Long_Serializer.deserialize(ns1_post$2d$buy$2d$form$2d$id_QNAME, reader, context);
                instance.setPostBuyFormId(((Long)member).longValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_post$2d$buy$2d$form$2d$buyer$2d$id_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_post$2d$buy$2d$form$2d$buyer$2d$id_QNAME, reader, context);
                instance.setPostBuyFormBuyerId(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_post$2d$buy$2d$form$2d$sellers_QNAME)) {
                member = ns3_myns3_ArrayOfPostBuyFormSellersStruct__PostBuyFormSellersStructArray_SOAPSerializer1.deserialize(ns1_post$2d$buy$2d$form$2d$sellers_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.PostBuyFormForBuyersDataStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myPOSTBUYFORMSELLERS_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setPostBuyFormSellers((AllegroWebApi.PostBuyFormSellersStruct[])member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_post$2d$buy$2d$form$2d$total$2d$amount_QNAME)) {
                member = ns2_myns2__float__float_Float_Serializer.deserialize(ns1_post$2d$buy$2d$form$2d$total$2d$amount_QNAME, reader, context);
                instance.setPostBuyFormTotalAmount(((Float)member).floatValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_post$2d$buy$2d$form$2d$invoice$2d$option_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_post$2d$buy$2d$form$2d$invoice$2d$option_QNAME, reader, context);
                instance.setPostBuyFormInvoiceOption(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_post$2d$buy$2d$form$2d$invoice$2d$data_QNAME)) {
                member = ns3_myPostBuyFormAddressStruct_SOAPSerializer.deserialize(ns1_post$2d$buy$2d$form$2d$invoice$2d$data_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.PostBuyFormForBuyersDataStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myPOSTBUYFORMINVOICEDATA_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setPostBuyFormInvoiceData((AllegroWebApi.PostBuyFormAddressStruct)member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_post$2d$buy$2d$form$2d$shipment$2d$address_QNAME)) {
                member = ns3_myPostBuyFormAddressStruct_SOAPSerializer.deserialize(ns1_post$2d$buy$2d$form$2d$shipment$2d$address_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.PostBuyFormForBuyersDataStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myPOSTBUYFORMSHIPMENTADDRESS_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setPostBuyFormShipmentAddress((AllegroWebApi.PostBuyFormAddressStruct)member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_post$2d$buy$2d$form$2d$pay$2d$type_QNAME)) {
                member = ns2_myns2_string__java_lang_String_String_Serializer.deserialize(ns1_post$2d$buy$2d$form$2d$pay$2d$type_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.PostBuyFormForBuyersDataStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myPOSTBUYFORMPAYTYPE_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setPostBuyFormPayType((java.lang.String)member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_post$2d$buy$2d$form$2d$pay$2d$id_QNAME)) {
                member = ns2_myns2__long__long_Long_Serializer.deserialize(ns1_post$2d$buy$2d$form$2d$pay$2d$id_QNAME, reader, context);
                instance.setPostBuyFormPayId(((Long)member).longValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_post$2d$buy$2d$form$2d$pay$2d$status_QNAME)) {
                member = ns2_myns2_string__java_lang_String_String_Serializer.deserialize(ns1_post$2d$buy$2d$form$2d$pay$2d$status_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.PostBuyFormForBuyersDataStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myPOSTBUYFORMPAYSTATUS_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setPostBuyFormPayStatus((java.lang.String)member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_post$2d$buy$2d$form$2d$date$2d$init_QNAME)) {
                member = ns2_myns2_string__java_lang_String_String_Serializer.deserialize(ns1_post$2d$buy$2d$form$2d$date$2d$init_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.PostBuyFormForBuyersDataStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myPOSTBUYFORMDATEINIT_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setPostBuyFormDateInit((java.lang.String)member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_post$2d$buy$2d$form$2d$date$2d$recv_QNAME)) {
                member = ns2_myns2_string__java_lang_String_String_Serializer.deserialize(ns1_post$2d$buy$2d$form$2d$date$2d$recv_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.PostBuyFormForBuyersDataStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myPOSTBUYFORMDATERECV_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setPostBuyFormDateRecv((java.lang.String)member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_post$2d$buy$2d$form$2d$date$2d$cancel_QNAME)) {
                member = ns2_myns2_string__java_lang_String_String_Serializer.deserialize(ns1_post$2d$buy$2d$form$2d$date$2d$cancel_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.PostBuyFormForBuyersDataStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myPOSTBUYFORMDATECANCEL_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setPostBuyFormDateCancel((java.lang.String)member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_post$2d$buy$2d$form$2d$payment$2d$amount_QNAME)) {
                member = ns2_myns2__float__float_Float_Serializer.deserialize(ns1_post$2d$buy$2d$form$2d$payment$2d$amount_QNAME, reader, context);
                instance.setPostBuyFormPaymentAmount(((Float)member).floatValue());
                reader.nextElementContent();
                continue;
            } else {
                throw new DeserializationException("soap.unexpectedElementName", new Object[] {ns1_post$2d$buy$2d$form$2d$payment$2d$amount_QNAME, elementName});
            }
        }
        
        XMLReaderUtil.verifyReaderState(reader, XMLReader.END);
        return (isComplete ? (java.lang.Object)instance : (java.lang.Object)state);
    }
    
    public void doSerializeAttributes(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws java.lang.Exception {
        AllegroWebApi.PostBuyFormForBuyersDataStruct instance = (AllegroWebApi.PostBuyFormForBuyersDataStruct)obj;
        
    }
    
    public void doSerializeInstance(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws java.lang.Exception {
        AllegroWebApi.PostBuyFormForBuyersDataStruct instance = (AllegroWebApi.PostBuyFormForBuyersDataStruct)obj;
        
        ns2_myns2__long__long_Long_Serializer.serialize(new Long(instance.getPostBuyFormId()), ns1_post$2d$buy$2d$form$2d$id_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getPostBuyFormBuyerId()), ns1_post$2d$buy$2d$form$2d$buyer$2d$id_QNAME, null, writer, context);
        ns3_myns3_ArrayOfPostBuyFormSellersStruct__PostBuyFormSellersStructArray_SOAPSerializer1.serialize(instance.getPostBuyFormSellers(), ns1_post$2d$buy$2d$form$2d$sellers_QNAME, null, writer, context);
        ns2_myns2__float__float_Float_Serializer.serialize(new Float(instance.getPostBuyFormTotalAmount()), ns1_post$2d$buy$2d$form$2d$total$2d$amount_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getPostBuyFormInvoiceOption()), ns1_post$2d$buy$2d$form$2d$invoice$2d$option_QNAME, null, writer, context);
        ns3_myPostBuyFormAddressStruct_SOAPSerializer.serialize(instance.getPostBuyFormInvoiceData(), ns1_post$2d$buy$2d$form$2d$invoice$2d$data_QNAME, null, writer, context);
        ns3_myPostBuyFormAddressStruct_SOAPSerializer.serialize(instance.getPostBuyFormShipmentAddress(), ns1_post$2d$buy$2d$form$2d$shipment$2d$address_QNAME, null, writer, context);
        ns2_myns2_string__java_lang_String_String_Serializer.serialize(instance.getPostBuyFormPayType(), ns1_post$2d$buy$2d$form$2d$pay$2d$type_QNAME, null, writer, context);
        ns2_myns2__long__long_Long_Serializer.serialize(new Long(instance.getPostBuyFormPayId()), ns1_post$2d$buy$2d$form$2d$pay$2d$id_QNAME, null, writer, context);
        ns2_myns2_string__java_lang_String_String_Serializer.serialize(instance.getPostBuyFormPayStatus(), ns1_post$2d$buy$2d$form$2d$pay$2d$status_QNAME, null, writer, context);
        ns2_myns2_string__java_lang_String_String_Serializer.serialize(instance.getPostBuyFormDateInit(), ns1_post$2d$buy$2d$form$2d$date$2d$init_QNAME, null, writer, context);
        ns2_myns2_string__java_lang_String_String_Serializer.serialize(instance.getPostBuyFormDateRecv(), ns1_post$2d$buy$2d$form$2d$date$2d$recv_QNAME, null, writer, context);
        ns2_myns2_string__java_lang_String_String_Serializer.serialize(instance.getPostBuyFormDateCancel(), ns1_post$2d$buy$2d$form$2d$date$2d$cancel_QNAME, null, writer, context);
        ns2_myns2__float__float_Float_Serializer.serialize(new Float(instance.getPostBuyFormPaymentAmount()), ns1_post$2d$buy$2d$form$2d$payment$2d$amount_QNAME, null, writer, context);
    }
}
