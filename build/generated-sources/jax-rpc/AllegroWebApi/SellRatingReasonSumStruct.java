// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class SellRatingReasonSumStruct {
    protected java.lang.String sellRatingReasonTitle;
    protected int sellRatingReasonCount;
    
    public SellRatingReasonSumStruct() {
    }
    
    public SellRatingReasonSumStruct(java.lang.String sellRatingReasonTitle, int sellRatingReasonCount) {
        this.sellRatingReasonTitle = sellRatingReasonTitle;
        this.sellRatingReasonCount = sellRatingReasonCount;
    }
    
    public java.lang.String getSellRatingReasonTitle() {
        return sellRatingReasonTitle;
    }
    
    public void setSellRatingReasonTitle(java.lang.String sellRatingReasonTitle) {
        this.sellRatingReasonTitle = sellRatingReasonTitle;
    }
    
    public int getSellRatingReasonCount() {
        return sellRatingReasonCount;
    }
    
    public void setSellRatingReasonCount(int sellRatingReasonCount) {
        this.sellRatingReasonCount = sellRatingReasonCount;
    }
}
