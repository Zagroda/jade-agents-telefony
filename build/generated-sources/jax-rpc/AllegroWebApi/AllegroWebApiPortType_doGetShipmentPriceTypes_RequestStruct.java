// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class AllegroWebApiPortType_doGetShipmentPriceTypes_RequestStruct {
    protected int countryId;
    protected java.lang.String webapiKey;
    
    public AllegroWebApiPortType_doGetShipmentPriceTypes_RequestStruct() {
    }
    
    public AllegroWebApiPortType_doGetShipmentPriceTypes_RequestStruct(int countryId, java.lang.String webapiKey) {
        this.countryId = countryId;
        this.webapiKey = webapiKey;
    }
    
    public int getCountryId() {
        return countryId;
    }
    
    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }
    
    public java.lang.String getWebapiKey() {
        return webapiKey;
    }
    
    public void setWebapiKey(java.lang.String webapiKey) {
        this.webapiKey = webapiKey;
    }
}
