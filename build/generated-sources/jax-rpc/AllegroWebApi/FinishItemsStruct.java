// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class FinishItemsStruct {
    protected long finishItemId;
    protected int finishCancelAllBids;
    protected java.lang.String finishCancelReason;
    
    public FinishItemsStruct() {
    }
    
    public FinishItemsStruct(long finishItemId, int finishCancelAllBids, java.lang.String finishCancelReason) {
        this.finishItemId = finishItemId;
        this.finishCancelAllBids = finishCancelAllBids;
        this.finishCancelReason = finishCancelReason;
    }
    
    public long getFinishItemId() {
        return finishItemId;
    }
    
    public void setFinishItemId(long finishItemId) {
        this.finishItemId = finishItemId;
    }
    
    public int getFinishCancelAllBids() {
        return finishCancelAllBids;
    }
    
    public void setFinishCancelAllBids(int finishCancelAllBids) {
        this.finishCancelAllBids = finishCancelAllBids;
    }
    
    public java.lang.String getFinishCancelReason() {
        return finishCancelReason;
    }
    
    public void setFinishCancelReason(java.lang.String finishCancelReason) {
        this.finishCancelReason = finishCancelReason;
    }
}
