// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;

import com.sun.xml.rpc.encoding.*;
import com.sun.xml.rpc.encoding.literal.DetailFragmentDeserializer;
import com.sun.xml.rpc.encoding.simpletype.*;
import com.sun.xml.rpc.encoding.soap.SOAPConstants;
import com.sun.xml.rpc.encoding.soap.SOAP12Constants;
import com.sun.xml.rpc.streaming.*;
import com.sun.xml.rpc.wsdl.document.schema.SchemaConstants;
import javax.xml.namespace.QName;

public class AllegroWebApiPortType_doSellSomeAgainInShop_RequestStruct_SOAPSerializer extends ObjectSerializerBase implements Initializable {
    private static final javax.xml.namespace.QName ns1_session$2d$handle_QNAME = new QName("", "session-handle");
    private static final javax.xml.namespace.QName ns2_string_TYPE_QNAME = SchemaConstants.QNAME_TYPE_STRING;
    private CombinedSerializer ns2_myns2_string__java_lang_String_String_Serializer;
    private static final javax.xml.namespace.QName ns1_sell$2d$items$2d$array_QNAME = new QName("", "sell-items-array");
    private static final javax.xml.namespace.QName ns3_ArrayOfItemsID_TYPE_QNAME = new QName("urn:AllegroWebApi", "ArrayOfItemsID");
    private CombinedSerializer ns3_myns3_ArrayOfItemsID__longArray_SOAPSerializer1;
    private static final javax.xml.namespace.QName ns1_sell$2d$starting$2d$time_QNAME = new QName("", "sell-starting-time");
    private static final javax.xml.namespace.QName ns2_long_TYPE_QNAME = SchemaConstants.QNAME_TYPE_LONG;
    private CombinedSerializer ns2_myns2__long__long_Long_Serializer;
    private static final javax.xml.namespace.QName ns1_sell$2d$shop$2d$duration_QNAME = new QName("", "sell-shop-duration");
    private static final javax.xml.namespace.QName ns2_int_TYPE_QNAME = SchemaConstants.QNAME_TYPE_INT;
    private CombinedSerializer ns2_myns2__int__int_Int_Serializer;
    private static final javax.xml.namespace.QName ns1_sell$2d$shop$2d$options_QNAME = new QName("", "sell-shop-options");
    private static final javax.xml.namespace.QName ns1_sell$2d$prolong$2d$options_QNAME = new QName("", "sell-prolong-options");
    private static final javax.xml.namespace.QName ns1_sell$2d$shop$2d$category_QNAME = new QName("", "sell-shop-category");
    private static final javax.xml.namespace.QName ns1_local$2d$ids_QNAME = new QName("", "local-ids");
    private static final javax.xml.namespace.QName ns3_ArrayOfLocalIds_TYPE_QNAME = new QName("urn:AllegroWebApi", "ArrayOfLocalIds");
    private CombinedSerializer ns3_myns3_ArrayOfLocalIds__intArray_SOAPSerializer1;
    private static final int mySESSIONHANDLE_INDEX = 0;
    private static final int mySELLITEMSARRAY_INDEX = 1;
    private static final int mySELLSTARTINGTIME_INDEX = 2;
    private static final int mySELLSHOPDURATION_INDEX = 3;
    private static final int mySELLSHOPOPTIONS_INDEX = 4;
    private static final int mySELLPROLONGOPTIONS_INDEX = 5;
    private static final int mySELLSHOPCATEGORY_INDEX = 6;
    private static final int myLOCALIDS_INDEX = 7;
    
    public AllegroWebApiPortType_doSellSomeAgainInShop_RequestStruct_SOAPSerializer(QName type, boolean encodeType, boolean isNullable, String encodingStyle) {
        super(type, encodeType, isNullable, encodingStyle);
    }
    
    public void initialize(InternalTypeMappingRegistry registry) throws java.lang.Exception {
        ns2_myns2_string__java_lang_String_String_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, java.lang.String.class, ns2_string_TYPE_QNAME);
        ns3_myns3_ArrayOfItemsID__longArray_SOAPSerializer1 = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, long[].class, ns3_ArrayOfItemsID_TYPE_QNAME);
        ns2_myns2__long__long_Long_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, long.class, ns2_long_TYPE_QNAME);
        ns2_myns2__int__int_Int_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, int.class, ns2_int_TYPE_QNAME);
        ns3_myns3_ArrayOfLocalIds__intArray_SOAPSerializer1 = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, int[].class, ns3_ArrayOfLocalIds_TYPE_QNAME);
    }
    
    public java.lang.Object doDeserialize(SOAPDeserializationState state, XMLReader reader,
        SOAPDeserializationContext context) throws java.lang.Exception {
        AllegroWebApi.AllegroWebApiPortType_doSellSomeAgainInShop_RequestStruct instance = new AllegroWebApi.AllegroWebApiPortType_doSellSomeAgainInShop_RequestStruct();
        AllegroWebApi.AllegroWebApiPortType_doSellSomeAgainInShop_RequestStruct_SOAPBuilder builder = null;
        java.lang.Object member;
        boolean isComplete = true;
        javax.xml.namespace.QName elementName;
        
        reader.nextElementContent();
        for (int i=0; i<8; i++) {
            elementName = reader.getName();
            if (reader.getState() == XMLReader.END) {
                break;
            }
            if (elementName.equals(ns1_session$2d$handle_QNAME)) {
                member = ns2_myns2_string__java_lang_String_String_Serializer.deserialize(ns1_session$2d$handle_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.AllegroWebApiPortType_doSellSomeAgainInShop_RequestStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, mySESSIONHANDLE_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setSessionHandle((java.lang.String)member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_sell$2d$items$2d$array_QNAME)) {
                member = ns3_myns3_ArrayOfItemsID__longArray_SOAPSerializer1.deserialize(ns1_sell$2d$items$2d$array_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.AllegroWebApiPortType_doSellSomeAgainInShop_RequestStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, mySELLITEMSARRAY_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setSellItemsArray((long[])member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_sell$2d$starting$2d$time_QNAME)) {
                member = ns2_myns2__long__long_Long_Serializer.deserialize(ns1_sell$2d$starting$2d$time_QNAME, reader, context);
                instance.setSellStartingTime(((Long)member).longValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_sell$2d$shop$2d$duration_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_sell$2d$shop$2d$duration_QNAME, reader, context);
                instance.setSellShopDuration(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_sell$2d$shop$2d$options_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_sell$2d$shop$2d$options_QNAME, reader, context);
                instance.setSellShopOptions(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_sell$2d$prolong$2d$options_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_sell$2d$prolong$2d$options_QNAME, reader, context);
                instance.setSellProlongOptions(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_sell$2d$shop$2d$category_QNAME)) {
                member = ns2_myns2__long__long_Long_Serializer.deserialize(ns1_sell$2d$shop$2d$category_QNAME, reader, context);
                instance.setSellShopCategory(((Long)member).longValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_local$2d$ids_QNAME)) {
                member = ns3_myns3_ArrayOfLocalIds__intArray_SOAPSerializer1.deserialize(ns1_local$2d$ids_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.AllegroWebApiPortType_doSellSomeAgainInShop_RequestStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myLOCALIDS_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setLocalIds((int[])member);
                }
                reader.nextElementContent();
                continue;
            } else {
                throw new DeserializationException("soap.unexpectedElementName", new Object[] {ns1_local$2d$ids_QNAME, elementName});
            }
        }
        
        XMLReaderUtil.verifyReaderState(reader, XMLReader.END);
        return (isComplete ? (java.lang.Object)instance : (java.lang.Object)state);
    }
    
    public void doSerializeAttributes(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws java.lang.Exception {
        AllegroWebApi.AllegroWebApiPortType_doSellSomeAgainInShop_RequestStruct instance = (AllegroWebApi.AllegroWebApiPortType_doSellSomeAgainInShop_RequestStruct)obj;
        
    }
    
    public void doSerializeInstance(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws java.lang.Exception {
        AllegroWebApi.AllegroWebApiPortType_doSellSomeAgainInShop_RequestStruct instance = (AllegroWebApi.AllegroWebApiPortType_doSellSomeAgainInShop_RequestStruct)obj;
        
        ns2_myns2_string__java_lang_String_String_Serializer.serialize(instance.getSessionHandle(), ns1_session$2d$handle_QNAME, null, writer, context);
        ns3_myns3_ArrayOfItemsID__longArray_SOAPSerializer1.serialize(instance.getSellItemsArray(), ns1_sell$2d$items$2d$array_QNAME, null, writer, context);
        ns2_myns2__long__long_Long_Serializer.serialize(new Long(instance.getSellStartingTime()), ns1_sell$2d$starting$2d$time_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getSellShopDuration()), ns1_sell$2d$shop$2d$duration_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getSellShopOptions()), ns1_sell$2d$shop$2d$options_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getSellProlongOptions()), ns1_sell$2d$prolong$2d$options_QNAME, null, writer, context);
        ns2_myns2__long__long_Long_Serializer.serialize(new Long(instance.getSellShopCategory()), ns1_sell$2d$shop$2d$category_QNAME, null, writer, context);
        ns3_myns3_ArrayOfLocalIds__intArray_SOAPSerializer1.serialize(instance.getLocalIds(), ns1_local$2d$ids_QNAME, null, writer, context);
    }
}
