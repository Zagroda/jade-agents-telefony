// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;

import com.sun.xml.rpc.encoding.*;
import com.sun.xml.rpc.encoding.literal.DetailFragmentDeserializer;
import com.sun.xml.rpc.encoding.simpletype.*;
import com.sun.xml.rpc.encoding.soap.SOAPConstants;
import com.sun.xml.rpc.encoding.soap.SOAP12Constants;
import com.sun.xml.rpc.streaming.*;
import com.sun.xml.rpc.wsdl.document.schema.SchemaConstants;
import javax.xml.namespace.QName;

public class AllegroWebApiPortType_doFeedback_RequestStruct_SOAPSerializer extends ObjectSerializerBase implements Initializable {
    private static final javax.xml.namespace.QName ns1_session$2d$handle_QNAME = new QName("", "session-handle");
    private static final javax.xml.namespace.QName ns2_string_TYPE_QNAME = SchemaConstants.QNAME_TYPE_STRING;
    private CombinedSerializer ns2_myns2_string__java_lang_String_String_Serializer;
    private static final javax.xml.namespace.QName ns1_fe$2d$item$2d$id_QNAME = new QName("", "fe-item-id");
    private static final javax.xml.namespace.QName ns2_long_TYPE_QNAME = SchemaConstants.QNAME_TYPE_LONG;
    private CombinedSerializer ns2_myns2__long__long_Long_Serializer;
    private static final javax.xml.namespace.QName ns1_fe$2d$use$2d$comment$2d$template_QNAME = new QName("", "fe-use-comment-template");
    private static final javax.xml.namespace.QName ns2_int_TYPE_QNAME = SchemaConstants.QNAME_TYPE_INT;
    private CombinedSerializer ns2_myns2__int__int_Int_Serializer;
    private static final javax.xml.namespace.QName ns1_fe$2d$to$2d$user$2d$id_QNAME = new QName("", "fe-to-user-id");
    private static final javax.xml.namespace.QName ns1_fe$2d$comment_QNAME = new QName("", "fe-comment");
    private static final javax.xml.namespace.QName ns1_fe$2d$comment$2d$type_QNAME = new QName("", "fe-comment-type");
    private static final javax.xml.namespace.QName ns1_fe$2d$op_QNAME = new QName("", "fe-op");
    private static final javax.xml.namespace.QName ns1_fe$2d$rating_QNAME = new QName("", "fe-rating");
    private static final javax.xml.namespace.QName ns3_ArrayOfSellRatingEstimationStruct_TYPE_QNAME = new QName("urn:AllegroWebApi", "ArrayOfSellRatingEstimationStruct");
    private CombinedSerializer ns3_myns3_ArrayOfSellRatingEstimationStruct__SellRatingEstimationStructArray_SOAPSerializer1;
    private static final int mySESSIONHANDLE_INDEX = 0;
    private static final int myFEITEMID_INDEX = 1;
    private static final int myFEUSECOMMENTTEMPLATE_INDEX = 2;
    private static final int myFETOUSERID_INDEX = 3;
    private static final int myFECOMMENT_INDEX = 4;
    private static final int myFECOMMENTTYPE_INDEX = 5;
    private static final int myFEOP_INDEX = 6;
    private static final int myFERATING_INDEX = 7;
    
    public AllegroWebApiPortType_doFeedback_RequestStruct_SOAPSerializer(QName type, boolean encodeType, boolean isNullable, String encodingStyle) {
        super(type, encodeType, isNullable, encodingStyle);
    }
    
    public void initialize(InternalTypeMappingRegistry registry) throws java.lang.Exception {
        ns2_myns2_string__java_lang_String_String_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, java.lang.String.class, ns2_string_TYPE_QNAME);
        ns2_myns2__long__long_Long_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, long.class, ns2_long_TYPE_QNAME);
        ns2_myns2__int__int_Int_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, int.class, ns2_int_TYPE_QNAME);
        ns3_myns3_ArrayOfSellRatingEstimationStruct__SellRatingEstimationStructArray_SOAPSerializer1 = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, AllegroWebApi.SellRatingEstimationStruct[].class, ns3_ArrayOfSellRatingEstimationStruct_TYPE_QNAME);
    }
    
    public java.lang.Object doDeserialize(SOAPDeserializationState state, XMLReader reader,
        SOAPDeserializationContext context) throws java.lang.Exception {
        AllegroWebApi.AllegroWebApiPortType_doFeedback_RequestStruct instance = new AllegroWebApi.AllegroWebApiPortType_doFeedback_RequestStruct();
        AllegroWebApi.AllegroWebApiPortType_doFeedback_RequestStruct_SOAPBuilder builder = null;
        java.lang.Object member;
        boolean isComplete = true;
        javax.xml.namespace.QName elementName;
        
        reader.nextElementContent();
        for (int i=0; i<8; i++) {
            elementName = reader.getName();
            if (reader.getState() == XMLReader.END) {
                break;
            }
            if (elementName.equals(ns1_session$2d$handle_QNAME)) {
                member = ns2_myns2_string__java_lang_String_String_Serializer.deserialize(ns1_session$2d$handle_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.AllegroWebApiPortType_doFeedback_RequestStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, mySESSIONHANDLE_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setSessionHandle((java.lang.String)member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_fe$2d$item$2d$id_QNAME)) {
                member = ns2_myns2__long__long_Long_Serializer.deserialize(ns1_fe$2d$item$2d$id_QNAME, reader, context);
                instance.setFeItemId(((Long)member).longValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_fe$2d$use$2d$comment$2d$template_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_fe$2d$use$2d$comment$2d$template_QNAME, reader, context);
                instance.setFeUseCommentTemplate(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_fe$2d$to$2d$user$2d$id_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_fe$2d$to$2d$user$2d$id_QNAME, reader, context);
                instance.setFeToUserId(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_fe$2d$comment_QNAME)) {
                member = ns2_myns2_string__java_lang_String_String_Serializer.deserialize(ns1_fe$2d$comment_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.AllegroWebApiPortType_doFeedback_RequestStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myFECOMMENT_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setFeComment((java.lang.String)member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_fe$2d$comment$2d$type_QNAME)) {
                member = ns2_myns2_string__java_lang_String_String_Serializer.deserialize(ns1_fe$2d$comment$2d$type_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.AllegroWebApiPortType_doFeedback_RequestStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myFECOMMENTTYPE_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setFeCommentType((java.lang.String)member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_fe$2d$op_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_fe$2d$op_QNAME, reader, context);
                instance.setFeOp(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_fe$2d$rating_QNAME)) {
                member = ns3_myns3_ArrayOfSellRatingEstimationStruct__SellRatingEstimationStructArray_SOAPSerializer1.deserialize(ns1_fe$2d$rating_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.AllegroWebApiPortType_doFeedback_RequestStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myFERATING_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setFeRating((AllegroWebApi.SellRatingEstimationStruct[])member);
                }
                reader.nextElementContent();
                continue;
            } else {
                throw new DeserializationException("soap.unexpectedElementName", new Object[] {ns1_fe$2d$rating_QNAME, elementName});
            }
        }
        
        XMLReaderUtil.verifyReaderState(reader, XMLReader.END);
        return (isComplete ? (java.lang.Object)instance : (java.lang.Object)state);
    }
    
    public void doSerializeAttributes(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws java.lang.Exception {
        AllegroWebApi.AllegroWebApiPortType_doFeedback_RequestStruct instance = (AllegroWebApi.AllegroWebApiPortType_doFeedback_RequestStruct)obj;
        
    }
    
    public void doSerializeInstance(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws java.lang.Exception {
        AllegroWebApi.AllegroWebApiPortType_doFeedback_RequestStruct instance = (AllegroWebApi.AllegroWebApiPortType_doFeedback_RequestStruct)obj;
        
        ns2_myns2_string__java_lang_String_String_Serializer.serialize(instance.getSessionHandle(), ns1_session$2d$handle_QNAME, null, writer, context);
        ns2_myns2__long__long_Long_Serializer.serialize(new Long(instance.getFeItemId()), ns1_fe$2d$item$2d$id_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getFeUseCommentTemplate()), ns1_fe$2d$use$2d$comment$2d$template_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getFeToUserId()), ns1_fe$2d$to$2d$user$2d$id_QNAME, null, writer, context);
        ns2_myns2_string__java_lang_String_String_Serializer.serialize(instance.getFeComment(), ns1_fe$2d$comment_QNAME, null, writer, context);
        ns2_myns2_string__java_lang_String_String_Serializer.serialize(instance.getFeCommentType(), ns1_fe$2d$comment$2d$type_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getFeOp()), ns1_fe$2d$op_QNAME, null, writer, context);
        ns3_myns3_ArrayOfSellRatingEstimationStruct__SellRatingEstimationStructArray_SOAPSerializer1.serialize(instance.getFeRating(), ns1_fe$2d$rating_QNAME, null, writer, context);
    }
}
