// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class SellFormType {
    protected int sellFormId;
    protected java.lang.String sellFormTitle;
    protected int sellFormCat;
    protected int sellFormType;
    protected int sellFormResType;
    protected int sellFormDefValue;
    protected int sellFormOpt;
    protected int sellFormPos;
    protected int sellFormLength;
    protected java.lang.String sellMinValue;
    protected java.lang.String sellMaxValue;
    protected java.lang.String sellFormDesc;
    protected java.lang.String sellFormOptsValues;
    protected java.lang.String sellFormFieldDesc;
    protected int sellFormParamId;
    protected java.lang.String sellFormParamValues;
    protected int sellFormParentId;
    protected java.lang.String sellFormParentValue;
    protected java.lang.String sellFormUnit;
    protected int sellFormOptions;
    
    public SellFormType() {
    }
    
    public SellFormType(int sellFormId, java.lang.String sellFormTitle, int sellFormCat, int sellFormType, int sellFormResType, int sellFormDefValue, int sellFormOpt, int sellFormPos, int sellFormLength, java.lang.String sellMinValue, java.lang.String sellMaxValue, java.lang.String sellFormDesc, java.lang.String sellFormOptsValues, java.lang.String sellFormFieldDesc, int sellFormParamId, java.lang.String sellFormParamValues, int sellFormParentId, java.lang.String sellFormParentValue, java.lang.String sellFormUnit, int sellFormOptions) {
        this.sellFormId = sellFormId;
        this.sellFormTitle = sellFormTitle;
        this.sellFormCat = sellFormCat;
        this.sellFormType = sellFormType;
        this.sellFormResType = sellFormResType;
        this.sellFormDefValue = sellFormDefValue;
        this.sellFormOpt = sellFormOpt;
        this.sellFormPos = sellFormPos;
        this.sellFormLength = sellFormLength;
        this.sellMinValue = sellMinValue;
        this.sellMaxValue = sellMaxValue;
        this.sellFormDesc = sellFormDesc;
        this.sellFormOptsValues = sellFormOptsValues;
        this.sellFormFieldDesc = sellFormFieldDesc;
        this.sellFormParamId = sellFormParamId;
        this.sellFormParamValues = sellFormParamValues;
        this.sellFormParentId = sellFormParentId;
        this.sellFormParentValue = sellFormParentValue;
        this.sellFormUnit = sellFormUnit;
        this.sellFormOptions = sellFormOptions;
    }
    
    public int getSellFormId() {
        return sellFormId;
    }
    
    public void setSellFormId(int sellFormId) {
        this.sellFormId = sellFormId;
    }
    
    public java.lang.String getSellFormTitle() {
        return sellFormTitle;
    }
    
    public void setSellFormTitle(java.lang.String sellFormTitle) {
        this.sellFormTitle = sellFormTitle;
    }
    
    public int getSellFormCat() {
        return sellFormCat;
    }
    
    public void setSellFormCat(int sellFormCat) {
        this.sellFormCat = sellFormCat;
    }
    
    public int getSellFormType() {
        return sellFormType;
    }
    
    public void setSellFormType(int sellFormType) {
        this.sellFormType = sellFormType;
    }
    
    public int getSellFormResType() {
        return sellFormResType;
    }
    
    public void setSellFormResType(int sellFormResType) {
        this.sellFormResType = sellFormResType;
    }
    
    public int getSellFormDefValue() {
        return sellFormDefValue;
    }
    
    public void setSellFormDefValue(int sellFormDefValue) {
        this.sellFormDefValue = sellFormDefValue;
    }
    
    public int getSellFormOpt() {
        return sellFormOpt;
    }
    
    public void setSellFormOpt(int sellFormOpt) {
        this.sellFormOpt = sellFormOpt;
    }
    
    public int getSellFormPos() {
        return sellFormPos;
    }
    
    public void setSellFormPos(int sellFormPos) {
        this.sellFormPos = sellFormPos;
    }
    
    public int getSellFormLength() {
        return sellFormLength;
    }
    
    public void setSellFormLength(int sellFormLength) {
        this.sellFormLength = sellFormLength;
    }
    
    public java.lang.String getSellMinValue() {
        return sellMinValue;
    }
    
    public void setSellMinValue(java.lang.String sellMinValue) {
        this.sellMinValue = sellMinValue;
    }
    
    public java.lang.String getSellMaxValue() {
        return sellMaxValue;
    }
    
    public void setSellMaxValue(java.lang.String sellMaxValue) {
        this.sellMaxValue = sellMaxValue;
    }
    
    public java.lang.String getSellFormDesc() {
        return sellFormDesc;
    }
    
    public void setSellFormDesc(java.lang.String sellFormDesc) {
        this.sellFormDesc = sellFormDesc;
    }
    
    public java.lang.String getSellFormOptsValues() {
        return sellFormOptsValues;
    }
    
    public void setSellFormOptsValues(java.lang.String sellFormOptsValues) {
        this.sellFormOptsValues = sellFormOptsValues;
    }
    
    public java.lang.String getSellFormFieldDesc() {
        return sellFormFieldDesc;
    }
    
    public void setSellFormFieldDesc(java.lang.String sellFormFieldDesc) {
        this.sellFormFieldDesc = sellFormFieldDesc;
    }
    
    public int getSellFormParamId() {
        return sellFormParamId;
    }
    
    public void setSellFormParamId(int sellFormParamId) {
        this.sellFormParamId = sellFormParamId;
    }
    
    public java.lang.String getSellFormParamValues() {
        return sellFormParamValues;
    }
    
    public void setSellFormParamValues(java.lang.String sellFormParamValues) {
        this.sellFormParamValues = sellFormParamValues;
    }
    
    public int getSellFormParentId() {
        return sellFormParentId;
    }
    
    public void setSellFormParentId(int sellFormParentId) {
        this.sellFormParentId = sellFormParentId;
    }
    
    public java.lang.String getSellFormParentValue() {
        return sellFormParentValue;
    }
    
    public void setSellFormParentValue(java.lang.String sellFormParentValue) {
        this.sellFormParentValue = sellFormParentValue;
    }
    
    public java.lang.String getSellFormUnit() {
        return sellFormUnit;
    }
    
    public void setSellFormUnit(java.lang.String sellFormUnit) {
        this.sellFormUnit = sellFormUnit;
    }
    
    public int getSellFormOptions() {
        return sellFormOptions;
    }
    
    public void setSellFormOptions(int sellFormOptions) {
        this.sellFormOptions = sellFormOptions;
    }
}
