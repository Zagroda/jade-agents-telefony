// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class AllegroWebApiPortType_doGetSiteJournal_ResponseStruct {
    protected AllegroWebApi.SiteJournal[] siteJournalArray;
    
    public AllegroWebApiPortType_doGetSiteJournal_ResponseStruct() {
    }
    
    public AllegroWebApiPortType_doGetSiteJournal_ResponseStruct(AllegroWebApi.SiteJournal[] siteJournalArray) {
        this.siteJournalArray = siteJournalArray;
    }
    
    public AllegroWebApi.SiteJournal[] getSiteJournalArray() {
        return siteJournalArray;
    }
    
    public void setSiteJournalArray(AllegroWebApi.SiteJournal[] siteJournalArray) {
        this.siteJournalArray = siteJournalArray;
    }
}
