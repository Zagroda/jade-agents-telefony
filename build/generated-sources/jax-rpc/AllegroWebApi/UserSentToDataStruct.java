// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class UserSentToDataStruct {
    protected long userId;
    protected java.lang.String userFirstName;
    protected java.lang.String userLastName;
    protected java.lang.String userCompany;
    protected int userCountryId;
    protected java.lang.String userPostcode;
    protected java.lang.String userCity;
    protected java.lang.String userAddress;
    
    public UserSentToDataStruct() {
    }
    
    public UserSentToDataStruct(long userId, java.lang.String userFirstName, java.lang.String userLastName, java.lang.String userCompany, int userCountryId, java.lang.String userPostcode, java.lang.String userCity, java.lang.String userAddress) {
        this.userId = userId;
        this.userFirstName = userFirstName;
        this.userLastName = userLastName;
        this.userCompany = userCompany;
        this.userCountryId = userCountryId;
        this.userPostcode = userPostcode;
        this.userCity = userCity;
        this.userAddress = userAddress;
    }
    
    public long getUserId() {
        return userId;
    }
    
    public void setUserId(long userId) {
        this.userId = userId;
    }
    
    public java.lang.String getUserFirstName() {
        return userFirstName;
    }
    
    public void setUserFirstName(java.lang.String userFirstName) {
        this.userFirstName = userFirstName;
    }
    
    public java.lang.String getUserLastName() {
        return userLastName;
    }
    
    public void setUserLastName(java.lang.String userLastName) {
        this.userLastName = userLastName;
    }
    
    public java.lang.String getUserCompany() {
        return userCompany;
    }
    
    public void setUserCompany(java.lang.String userCompany) {
        this.userCompany = userCompany;
    }
    
    public int getUserCountryId() {
        return userCountryId;
    }
    
    public void setUserCountryId(int userCountryId) {
        this.userCountryId = userCountryId;
    }
    
    public java.lang.String getUserPostcode() {
        return userPostcode;
    }
    
    public void setUserPostcode(java.lang.String userPostcode) {
        this.userPostcode = userPostcode;
    }
    
    public java.lang.String getUserCity() {
        return userCity;
    }
    
    public void setUserCity(java.lang.String userCity) {
        this.userCity = userCity;
    }
    
    public java.lang.String getUserAddress() {
        return userAddress;
    }
    
    public void setUserAddress(java.lang.String userAddress) {
        this.userAddress = userAddress;
    }
}
