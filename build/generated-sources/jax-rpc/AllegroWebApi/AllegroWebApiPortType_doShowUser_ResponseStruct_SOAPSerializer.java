// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;

import com.sun.xml.rpc.encoding.*;
import com.sun.xml.rpc.encoding.literal.DetailFragmentDeserializer;
import com.sun.xml.rpc.encoding.simpletype.*;
import com.sun.xml.rpc.encoding.soap.SOAPConstants;
import com.sun.xml.rpc.encoding.soap.SOAP12Constants;
import com.sun.xml.rpc.streaming.*;
import com.sun.xml.rpc.wsdl.document.schema.SchemaConstants;
import javax.xml.namespace.QName;

public class AllegroWebApiPortType_doShowUser_ResponseStruct_SOAPSerializer extends ObjectSerializerBase implements Initializable {
    private static final javax.xml.namespace.QName ns1_user$2d$id_QNAME = new QName("", "user-id");
    private static final javax.xml.namespace.QName ns2_long_TYPE_QNAME = SchemaConstants.QNAME_TYPE_LONG;
    private CombinedSerializer ns2_myns2__long__long_Long_Serializer;
    private static final javax.xml.namespace.QName ns1_user$2d$login_QNAME = new QName("", "user-login");
    private static final javax.xml.namespace.QName ns2_string_TYPE_QNAME = SchemaConstants.QNAME_TYPE_STRING;
    private CombinedSerializer ns2_myns2_string__java_lang_String_String_Serializer;
    private static final javax.xml.namespace.QName ns1_user$2d$country_QNAME = new QName("", "user-country");
    private static final javax.xml.namespace.QName ns2_int_TYPE_QNAME = SchemaConstants.QNAME_TYPE_INT;
    private CombinedSerializer ns2_myns2__int__int_Int_Serializer;
    private static final javax.xml.namespace.QName ns1_user$2d$create$2d$date_QNAME = new QName("", "user-create-date");
    private static final javax.xml.namespace.QName ns1_user$2d$login$2d$date_QNAME = new QName("", "user-login-date");
    private static final javax.xml.namespace.QName ns1_user$2d$rating_QNAME = new QName("", "user-rating");
    private static final javax.xml.namespace.QName ns1_user$2d$is$2d$new$2d$user_QNAME = new QName("", "user-is-new-user");
    private static final javax.xml.namespace.QName ns1_user$2d$not$2d$activated_QNAME = new QName("", "user-not-activated");
    private static final javax.xml.namespace.QName ns1_user$2d$closed_QNAME = new QName("", "user-closed");
    private static final javax.xml.namespace.QName ns1_user$2d$blocked_QNAME = new QName("", "user-blocked");
    private static final javax.xml.namespace.QName ns1_user$2d$terminated_QNAME = new QName("", "user-terminated");
    private static final javax.xml.namespace.QName ns1_user$2d$has$2d$page_QNAME = new QName("", "user-has-page");
    private static final javax.xml.namespace.QName ns1_user$2d$is$2d$sseller_QNAME = new QName("", "user-is-sseller");
    private static final javax.xml.namespace.QName ns1_user$2d$is$2d$eco_QNAME = new QName("", "user-is-eco");
    private static final javax.xml.namespace.QName ns1_user$2d$positive$2d$feedback_QNAME = new QName("", "user-positive-feedback");
    private static final javax.xml.namespace.QName ns3_ShowUserFeedbacks_TYPE_QNAME = new QName("urn:AllegroWebApi", "ShowUserFeedbacks");
    private CombinedSerializer ns3_myShowUserFeedbacks_SOAPSerializer;
    private static final javax.xml.namespace.QName ns1_user$2d$negative$2d$feedback_QNAME = new QName("", "user-negative-feedback");
    private static final javax.xml.namespace.QName ns1_user$2d$neutral$2d$feedback_QNAME = new QName("", "user-neutral-feedback");
    private static final javax.xml.namespace.QName ns1_user$2d$junior$2d$status_QNAME = new QName("", "user-junior-status");
    private static final javax.xml.namespace.QName ns1_user$2d$has$2d$shop_QNAME = new QName("", "user-has-shop");
    private static final javax.xml.namespace.QName ns1_user$2d$company$2d$icon_QNAME = new QName("", "user-company-icon");
    private static final javax.xml.namespace.QName ns1_user$2d$sell$2d$rating$2d$count_QNAME = new QName("", "user-sell-rating-count");
    private static final javax.xml.namespace.QName ns1_user$2d$sell$2d$rating$2d$average_QNAME = new QName("", "user-sell-rating-average");
    private static final javax.xml.namespace.QName ns3_ArrayOfSellRatingAverageStruct_TYPE_QNAME = new QName("urn:AllegroWebApi", "ArrayOfSellRatingAverageStruct");
    private CombinedSerializer ns3_myns3_ArrayOfSellRatingAverageStruct__SellRatingAverageStructArray_SOAPSerializer1;
    private static final javax.xml.namespace.QName ns1_user$2d$is$2d$allegro$2d$standard_QNAME = new QName("", "user-is-allegro-standard");
    private static final javax.xml.namespace.QName ns1_user$2d$is$2d$b2c$2d$seller_QNAME = new QName("", "user-is-b2c-seller");
    private static final int myUSERID_INDEX = 0;
    private static final int myUSERLOGIN_INDEX = 1;
    private static final int myUSERCOUNTRY_INDEX = 2;
    private static final int myUSERCREATEDATE_INDEX = 3;
    private static final int myUSERLOGINDATE_INDEX = 4;
    private static final int myUSERRATING_INDEX = 5;
    private static final int myUSERISNEWUSER_INDEX = 6;
    private static final int myUSERNOTACTIVATED_INDEX = 7;
    private static final int myUSERCLOSED_INDEX = 8;
    private static final int myUSERBLOCKED_INDEX = 9;
    private static final int myUSERTERMINATED_INDEX = 10;
    private static final int myUSERHASPAGE_INDEX = 11;
    private static final int myUSERISSSELLER_INDEX = 12;
    private static final int myUSERISECO_INDEX = 13;
    private static final int myUSERPOSITIVEFEEDBACK_INDEX = 14;
    private static final int myUSERNEGATIVEFEEDBACK_INDEX = 15;
    private static final int myUSERNEUTRALFEEDBACK_INDEX = 16;
    private static final int myUSERJUNIORSTATUS_INDEX = 17;
    private static final int myUSERHASSHOP_INDEX = 18;
    private static final int myUSERCOMPANYICON_INDEX = 19;
    private static final int myUSERSELLRATINGCOUNT_INDEX = 20;
    private static final int myUSERSELLRATINGAVERAGE_INDEX = 21;
    private static final int myUSERISALLEGROSTANDARD_INDEX = 22;
    private static final int myUSERISB2CSELLER_INDEX = 23;
    
    public AllegroWebApiPortType_doShowUser_ResponseStruct_SOAPSerializer(QName type, boolean encodeType, boolean isNullable, String encodingStyle) {
        super(type, encodeType, isNullable, encodingStyle);
    }
    
    public void initialize(InternalTypeMappingRegistry registry) throws java.lang.Exception {
        ns2_myns2__long__long_Long_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, long.class, ns2_long_TYPE_QNAME);
        ns2_myns2_string__java_lang_String_String_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, java.lang.String.class, ns2_string_TYPE_QNAME);
        ns2_myns2__int__int_Int_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, int.class, ns2_int_TYPE_QNAME);
        ns3_myShowUserFeedbacks_SOAPSerializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, AllegroWebApi.ShowUserFeedbacks.class, ns3_ShowUserFeedbacks_TYPE_QNAME);
        ns3_myns3_ArrayOfSellRatingAverageStruct__SellRatingAverageStructArray_SOAPSerializer1 = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, AllegroWebApi.SellRatingAverageStruct[].class, ns3_ArrayOfSellRatingAverageStruct_TYPE_QNAME);
    }
    
    public java.lang.Object doDeserialize(SOAPDeserializationState state, XMLReader reader,
        SOAPDeserializationContext context) throws java.lang.Exception {
        AllegroWebApi.AllegroWebApiPortType_doShowUser_ResponseStruct instance = new AllegroWebApi.AllegroWebApiPortType_doShowUser_ResponseStruct();
        AllegroWebApi.AllegroWebApiPortType_doShowUser_ResponseStruct_SOAPBuilder builder = null;
        java.lang.Object member;
        boolean isComplete = true;
        javax.xml.namespace.QName elementName;
        
        reader.nextElementContent();
        for (int i=0; i<24; i++) {
            elementName = reader.getName();
            if (reader.getState() == XMLReader.END) {
                break;
            }
            if (elementName.equals(ns1_user$2d$id_QNAME)) {
                member = ns2_myns2__long__long_Long_Serializer.deserialize(ns1_user$2d$id_QNAME, reader, context);
                instance.setUserId(((Long)member).longValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_user$2d$login_QNAME)) {
                member = ns2_myns2_string__java_lang_String_String_Serializer.deserialize(ns1_user$2d$login_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.AllegroWebApiPortType_doShowUser_ResponseStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myUSERLOGIN_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setUserLogin((java.lang.String)member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_user$2d$country_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_user$2d$country_QNAME, reader, context);
                instance.setUserCountry(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_user$2d$create$2d$date_QNAME)) {
                member = ns2_myns2__long__long_Long_Serializer.deserialize(ns1_user$2d$create$2d$date_QNAME, reader, context);
                instance.setUserCreateDate(((Long)member).longValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_user$2d$login$2d$date_QNAME)) {
                member = ns2_myns2__long__long_Long_Serializer.deserialize(ns1_user$2d$login$2d$date_QNAME, reader, context);
                instance.setUserLoginDate(((Long)member).longValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_user$2d$rating_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_user$2d$rating_QNAME, reader, context);
                instance.setUserRating(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_user$2d$is$2d$new$2d$user_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_user$2d$is$2d$new$2d$user_QNAME, reader, context);
                instance.setUserIsNewUser(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_user$2d$not$2d$activated_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_user$2d$not$2d$activated_QNAME, reader, context);
                instance.setUserNotActivated(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_user$2d$closed_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_user$2d$closed_QNAME, reader, context);
                instance.setUserClosed(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_user$2d$blocked_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_user$2d$blocked_QNAME, reader, context);
                instance.setUserBlocked(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_user$2d$terminated_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_user$2d$terminated_QNAME, reader, context);
                instance.setUserTerminated(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_user$2d$has$2d$page_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_user$2d$has$2d$page_QNAME, reader, context);
                instance.setUserHasPage(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_user$2d$is$2d$sseller_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_user$2d$is$2d$sseller_QNAME, reader, context);
                instance.setUserIsSseller(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_user$2d$is$2d$eco_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_user$2d$is$2d$eco_QNAME, reader, context);
                instance.setUserIsEco(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_user$2d$positive$2d$feedback_QNAME)) {
                member = ns3_myShowUserFeedbacks_SOAPSerializer.deserialize(ns1_user$2d$positive$2d$feedback_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.AllegroWebApiPortType_doShowUser_ResponseStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myUSERPOSITIVEFEEDBACK_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setUserPositiveFeedback((AllegroWebApi.ShowUserFeedbacks)member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_user$2d$negative$2d$feedback_QNAME)) {
                member = ns3_myShowUserFeedbacks_SOAPSerializer.deserialize(ns1_user$2d$negative$2d$feedback_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.AllegroWebApiPortType_doShowUser_ResponseStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myUSERNEGATIVEFEEDBACK_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setUserNegativeFeedback((AllegroWebApi.ShowUserFeedbacks)member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_user$2d$neutral$2d$feedback_QNAME)) {
                member = ns3_myShowUserFeedbacks_SOAPSerializer.deserialize(ns1_user$2d$neutral$2d$feedback_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.AllegroWebApiPortType_doShowUser_ResponseStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myUSERNEUTRALFEEDBACK_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setUserNeutralFeedback((AllegroWebApi.ShowUserFeedbacks)member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_user$2d$junior$2d$status_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_user$2d$junior$2d$status_QNAME, reader, context);
                instance.setUserJuniorStatus(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_user$2d$has$2d$shop_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_user$2d$has$2d$shop_QNAME, reader, context);
                instance.setUserHasShop(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_user$2d$company$2d$icon_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_user$2d$company$2d$icon_QNAME, reader, context);
                instance.setUserCompanyIcon(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_user$2d$sell$2d$rating$2d$count_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_user$2d$sell$2d$rating$2d$count_QNAME, reader, context);
                instance.setUserSellRatingCount(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_user$2d$sell$2d$rating$2d$average_QNAME)) {
                member = ns3_myns3_ArrayOfSellRatingAverageStruct__SellRatingAverageStructArray_SOAPSerializer1.deserialize(ns1_user$2d$sell$2d$rating$2d$average_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.AllegroWebApiPortType_doShowUser_ResponseStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myUSERSELLRATINGAVERAGE_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setUserSellRatingAverage((AllegroWebApi.SellRatingAverageStruct[])member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_user$2d$is$2d$allegro$2d$standard_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_user$2d$is$2d$allegro$2d$standard_QNAME, reader, context);
                instance.setUserIsAllegroStandard(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_user$2d$is$2d$b2c$2d$seller_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_user$2d$is$2d$b2c$2d$seller_QNAME, reader, context);
                instance.setUserIsB2CSeller(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            } else {
                throw new DeserializationException("soap.unexpectedElementName", new Object[] {ns1_user$2d$is$2d$b2c$2d$seller_QNAME, elementName});
            }
        }
        
        XMLReaderUtil.verifyReaderState(reader, XMLReader.END);
        return (isComplete ? (java.lang.Object)instance : (java.lang.Object)state);
    }
    
    public void doSerializeAttributes(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws java.lang.Exception {
        AllegroWebApi.AllegroWebApiPortType_doShowUser_ResponseStruct instance = (AllegroWebApi.AllegroWebApiPortType_doShowUser_ResponseStruct)obj;
        
    }
    
    public void doSerializeInstance(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws java.lang.Exception {
        AllegroWebApi.AllegroWebApiPortType_doShowUser_ResponseStruct instance = (AllegroWebApi.AllegroWebApiPortType_doShowUser_ResponseStruct)obj;
        
        ns2_myns2__long__long_Long_Serializer.serialize(new Long(instance.getUserId()), ns1_user$2d$id_QNAME, null, writer, context);
        ns2_myns2_string__java_lang_String_String_Serializer.serialize(instance.getUserLogin(), ns1_user$2d$login_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getUserCountry()), ns1_user$2d$country_QNAME, null, writer, context);
        ns2_myns2__long__long_Long_Serializer.serialize(new Long(instance.getUserCreateDate()), ns1_user$2d$create$2d$date_QNAME, null, writer, context);
        ns2_myns2__long__long_Long_Serializer.serialize(new Long(instance.getUserLoginDate()), ns1_user$2d$login$2d$date_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getUserRating()), ns1_user$2d$rating_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getUserIsNewUser()), ns1_user$2d$is$2d$new$2d$user_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getUserNotActivated()), ns1_user$2d$not$2d$activated_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getUserClosed()), ns1_user$2d$closed_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getUserBlocked()), ns1_user$2d$blocked_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getUserTerminated()), ns1_user$2d$terminated_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getUserHasPage()), ns1_user$2d$has$2d$page_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getUserIsSseller()), ns1_user$2d$is$2d$sseller_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getUserIsEco()), ns1_user$2d$is$2d$eco_QNAME, null, writer, context);
        ns3_myShowUserFeedbacks_SOAPSerializer.serialize(instance.getUserPositiveFeedback(), ns1_user$2d$positive$2d$feedback_QNAME, null, writer, context);
        ns3_myShowUserFeedbacks_SOAPSerializer.serialize(instance.getUserNegativeFeedback(), ns1_user$2d$negative$2d$feedback_QNAME, null, writer, context);
        ns3_myShowUserFeedbacks_SOAPSerializer.serialize(instance.getUserNeutralFeedback(), ns1_user$2d$neutral$2d$feedback_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getUserJuniorStatus()), ns1_user$2d$junior$2d$status_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getUserHasShop()), ns1_user$2d$has$2d$shop_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getUserCompanyIcon()), ns1_user$2d$company$2d$icon_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getUserSellRatingCount()), ns1_user$2d$sell$2d$rating$2d$count_QNAME, null, writer, context);
        ns3_myns3_ArrayOfSellRatingAverageStruct__SellRatingAverageStructArray_SOAPSerializer1.serialize(instance.getUserSellRatingAverage(), ns1_user$2d$sell$2d$rating$2d$average_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getUserIsAllegroStandard()), ns1_user$2d$is$2d$allegro$2d$standard_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getUserIsB2CSeller()), ns1_user$2d$is$2d$b2c$2d$seller_QNAME, null, writer, context);
    }
    protected void verifyName(XMLReader reader, javax.xml.namespace.QName expectedName) throws java.lang.Exception {
    }
}
