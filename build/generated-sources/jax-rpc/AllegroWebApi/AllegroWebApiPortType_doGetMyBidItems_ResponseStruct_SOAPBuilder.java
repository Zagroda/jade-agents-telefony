// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;

import com.sun.xml.rpc.encoding.*;
import com.sun.xml.rpc.util.exception.LocalizableExceptionAdapter;

public class AllegroWebApiPortType_doGetMyBidItems_ResponseStruct_SOAPBuilder implements SOAPInstanceBuilder {
    private AllegroWebApi.AllegroWebApiPortType_doGetMyBidItems_ResponseStruct _instance;
    private int bidItemsCounter;
    private AllegroWebApi.BidItemStruct[] bidItemsList;
    private static final int myBIDITEMSCOUNTER_INDEX = 0;
    private static final int myBIDITEMSLIST_INDEX = 1;
    
    public AllegroWebApiPortType_doGetMyBidItems_ResponseStruct_SOAPBuilder() {
    }
    
    public void setBidItemsCounter(int bidItemsCounter) {
        this.bidItemsCounter = bidItemsCounter;
    }
    
    public void setBidItemsList(AllegroWebApi.BidItemStruct[] bidItemsList) {
        this.bidItemsList = bidItemsList;
    }
    
    public int memberGateType(int memberIndex) {
        switch (memberIndex) {
            case myBIDITEMSLIST_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            default:
                throw new IllegalArgumentException();
        }
    }
    
    public void construct() {
    }
    
    public void setMember(int index, java.lang.Object memberValue) {
        try {
            switch(index) {
                case myBIDITEMSLIST_INDEX:
                    _instance.setBidItemsList((AllegroWebApi.BidItemStruct[])memberValue);
                    break;
                default:
                    throw new java.lang.IllegalArgumentException();
            }
        }
        catch (java.lang.RuntimeException e) {
            throw e;
        }
        catch (java.lang.Exception e) {
            throw new DeserializationException(new LocalizableExceptionAdapter(e));
        }
    }
    
    public void initialize() {
    }
    
    public void setInstance(java.lang.Object instance) {
        _instance = (AllegroWebApi.AllegroWebApiPortType_doGetMyBidItems_ResponseStruct)instance;
    }
    
    public java.lang.Object getInstance() {
        return _instance;
    }
}
