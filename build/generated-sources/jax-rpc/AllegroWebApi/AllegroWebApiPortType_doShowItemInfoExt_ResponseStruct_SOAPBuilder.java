// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;

import com.sun.xml.rpc.encoding.*;
import com.sun.xml.rpc.util.exception.LocalizableExceptionAdapter;

public class AllegroWebApiPortType_doShowItemInfoExt_ResponseStruct_SOAPBuilder implements SOAPInstanceBuilder {
    private AllegroWebApi.AllegroWebApiPortType_doShowItemInfoExt_ResponseStruct _instance;
    private AllegroWebApi.ItemInfoExt itemListInfoExt;
    private AllegroWebApi.ItemCatList[] itemCatPath;
    private AllegroWebApi.ItemImageList[] itemImgList;
    private AllegroWebApi.AttribStruct[] itemAttribList;
    private AllegroWebApi.PostageStruct[] itemPostageOptions;
    private AllegroWebApi.ItemPaymentOptions itemPaymentOptions;
    private AllegroWebApi.CompanyInfoStruct itemCompanyInfo;
    private AllegroWebApi.ProductStruct itemProductInfo;
    private AllegroWebApi.ItemVariantStruct[] itemVariants;
    private static final int myITEMLISTINFOEXT_INDEX = 0;
    private static final int myITEMCATPATH_INDEX = 1;
    private static final int myITEMIMGLIST_INDEX = 2;
    private static final int myITEMATTRIBLIST_INDEX = 3;
    private static final int myITEMPOSTAGEOPTIONS_INDEX = 4;
    private static final int myITEMPAYMENTOPTIONS_INDEX = 5;
    private static final int myITEMCOMPANYINFO_INDEX = 6;
    private static final int myITEMPRODUCTINFO_INDEX = 7;
    private static final int myITEMVARIANTS_INDEX = 8;
    
    public AllegroWebApiPortType_doShowItemInfoExt_ResponseStruct_SOAPBuilder() {
    }
    
    public void setItemListInfoExt(AllegroWebApi.ItemInfoExt itemListInfoExt) {
        this.itemListInfoExt = itemListInfoExt;
    }
    
    public void setItemCatPath(AllegroWebApi.ItemCatList[] itemCatPath) {
        this.itemCatPath = itemCatPath;
    }
    
    public void setItemImgList(AllegroWebApi.ItemImageList[] itemImgList) {
        this.itemImgList = itemImgList;
    }
    
    public void setItemAttribList(AllegroWebApi.AttribStruct[] itemAttribList) {
        this.itemAttribList = itemAttribList;
    }
    
    public void setItemPostageOptions(AllegroWebApi.PostageStruct[] itemPostageOptions) {
        this.itemPostageOptions = itemPostageOptions;
    }
    
    public void setItemPaymentOptions(AllegroWebApi.ItemPaymentOptions itemPaymentOptions) {
        this.itemPaymentOptions = itemPaymentOptions;
    }
    
    public void setItemCompanyInfo(AllegroWebApi.CompanyInfoStruct itemCompanyInfo) {
        this.itemCompanyInfo = itemCompanyInfo;
    }
    
    public void setItemProductInfo(AllegroWebApi.ProductStruct itemProductInfo) {
        this.itemProductInfo = itemProductInfo;
    }
    
    public void setItemVariants(AllegroWebApi.ItemVariantStruct[] itemVariants) {
        this.itemVariants = itemVariants;
    }
    
    public int memberGateType(int memberIndex) {
        switch (memberIndex) {
            case myITEMLISTINFOEXT_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            case myITEMCATPATH_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            case myITEMIMGLIST_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            case myITEMATTRIBLIST_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            case myITEMPOSTAGEOPTIONS_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            case myITEMPAYMENTOPTIONS_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            case myITEMCOMPANYINFO_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            case myITEMPRODUCTINFO_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            case myITEMVARIANTS_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            default:
                throw new IllegalArgumentException();
        }
    }
    
    public void construct() {
    }
    
    public void setMember(int index, java.lang.Object memberValue) {
        try {
            switch(index) {
                case myITEMLISTINFOEXT_INDEX:
                    _instance.setItemListInfoExt((AllegroWebApi.ItemInfoExt)memberValue);
                    break;
                case myITEMCATPATH_INDEX:
                    _instance.setItemCatPath((AllegroWebApi.ItemCatList[])memberValue);
                    break;
                case myITEMIMGLIST_INDEX:
                    _instance.setItemImgList((AllegroWebApi.ItemImageList[])memberValue);
                    break;
                case myITEMATTRIBLIST_INDEX:
                    _instance.setItemAttribList((AllegroWebApi.AttribStruct[])memberValue);
                    break;
                case myITEMPOSTAGEOPTIONS_INDEX:
                    _instance.setItemPostageOptions((AllegroWebApi.PostageStruct[])memberValue);
                    break;
                case myITEMPAYMENTOPTIONS_INDEX:
                    _instance.setItemPaymentOptions((AllegroWebApi.ItemPaymentOptions)memberValue);
                    break;
                case myITEMCOMPANYINFO_INDEX:
                    _instance.setItemCompanyInfo((AllegroWebApi.CompanyInfoStruct)memberValue);
                    break;
                case myITEMPRODUCTINFO_INDEX:
                    _instance.setItemProductInfo((AllegroWebApi.ProductStruct)memberValue);
                    break;
                case myITEMVARIANTS_INDEX:
                    _instance.setItemVariants((AllegroWebApi.ItemVariantStruct[])memberValue);
                    break;
                default:
                    throw new java.lang.IllegalArgumentException();
            }
        }
        catch (java.lang.RuntimeException e) {
            throw e;
        }
        catch (java.lang.Exception e) {
            throw new DeserializationException(new LocalizableExceptionAdapter(e));
        }
    }
    
    public void initialize() {
    }
    
    public void setInstance(java.lang.Object instance) {
        _instance = (AllegroWebApi.AllegroWebApiPortType_doShowItemInfoExt_ResponseStruct)instance;
    }
    
    public java.lang.Object getInstance() {
        return _instance;
    }
}
