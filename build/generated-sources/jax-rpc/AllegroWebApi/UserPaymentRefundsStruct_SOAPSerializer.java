// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;

import com.sun.xml.rpc.encoding.*;
import com.sun.xml.rpc.encoding.literal.DetailFragmentDeserializer;
import com.sun.xml.rpc.encoding.simpletype.*;
import com.sun.xml.rpc.encoding.soap.SOAPConstants;
import com.sun.xml.rpc.encoding.soap.SOAP12Constants;
import com.sun.xml.rpc.streaming.*;
import com.sun.xml.rpc.wsdl.document.schema.SchemaConstants;
import javax.xml.namespace.QName;

public class UserPaymentRefundsStruct_SOAPSerializer extends ObjectSerializerBase implements Initializable {
    private static final javax.xml.namespace.QName ns1_pay$2d$refund$2d$trans$2d$id_QNAME = new QName("", "pay-refund-trans-id");
    private static final javax.xml.namespace.QName ns2_long_TYPE_QNAME = SchemaConstants.QNAME_TYPE_LONG;
    private CombinedSerializer ns2_myns2__long__long_Long_Serializer;
    private static final javax.xml.namespace.QName ns1_pay$2d$refund$2d$it$2d$id_QNAME = new QName("", "pay-refund-it-id");
    private static final javax.xml.namespace.QName ns1_pay$2d$refund$2d$seller$2d$id_QNAME = new QName("", "pay-refund-seller-id");
    private static final javax.xml.namespace.QName ns2_int_TYPE_QNAME = SchemaConstants.QNAME_TYPE_INT;
    private CombinedSerializer ns2_myns2__int__int_Int_Serializer;
    private static final javax.xml.namespace.QName ns1_pay$2d$refund$2d$value_QNAME = new QName("", "pay-refund-value");
    private static final javax.xml.namespace.QName ns2_float_TYPE_QNAME = SchemaConstants.QNAME_TYPE_FLOAT;
    private CombinedSerializer ns2_myns2__float__float_Float_Serializer;
    private static final javax.xml.namespace.QName ns1_pay$2d$refund$2d$reason_QNAME = new QName("", "pay-refund-reason");
    private static final javax.xml.namespace.QName ns2_string_TYPE_QNAME = SchemaConstants.QNAME_TYPE_STRING;
    private CombinedSerializer ns2_myns2_string__java_lang_String_String_Serializer;
    private static final javax.xml.namespace.QName ns1_pay$2d$refund$2d$date_QNAME = new QName("", "pay-refund-date");
    private static final int myPAYREFUNDTRANSID_INDEX = 0;
    private static final int myPAYREFUNDITID_INDEX = 1;
    private static final int myPAYREFUNDSELLERID_INDEX = 2;
    private static final int myPAYREFUNDVALUE_INDEX = 3;
    private static final int myPAYREFUNDREASON_INDEX = 4;
    private static final int myPAYREFUNDDATE_INDEX = 5;
    
    public UserPaymentRefundsStruct_SOAPSerializer(QName type, boolean encodeType, boolean isNullable, String encodingStyle) {
        super(type, encodeType, isNullable, encodingStyle);
    }
    
    public void initialize(InternalTypeMappingRegistry registry) throws java.lang.Exception {
        ns2_myns2__long__long_Long_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, long.class, ns2_long_TYPE_QNAME);
        ns2_myns2__int__int_Int_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, int.class, ns2_int_TYPE_QNAME);
        ns2_myns2__float__float_Float_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, float.class, ns2_float_TYPE_QNAME);
        ns2_myns2_string__java_lang_String_String_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, java.lang.String.class, ns2_string_TYPE_QNAME);
    }
    
    public java.lang.Object doDeserialize(SOAPDeserializationState state, XMLReader reader,
        SOAPDeserializationContext context) throws java.lang.Exception {
        AllegroWebApi.UserPaymentRefundsStruct instance = new AllegroWebApi.UserPaymentRefundsStruct();
        AllegroWebApi.UserPaymentRefundsStruct_SOAPBuilder builder = null;
        java.lang.Object member;
        boolean isComplete = true;
        javax.xml.namespace.QName elementName;
        
        reader.nextElementContent();
        for (int i=0; i<6; i++) {
            elementName = reader.getName();
            if (reader.getState() == XMLReader.END) {
                break;
            }
            if (elementName.equals(ns1_pay$2d$refund$2d$trans$2d$id_QNAME)) {
                member = ns2_myns2__long__long_Long_Serializer.deserialize(ns1_pay$2d$refund$2d$trans$2d$id_QNAME, reader, context);
                instance.setPayRefundTransId(((Long)member).longValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_pay$2d$refund$2d$it$2d$id_QNAME)) {
                member = ns2_myns2__long__long_Long_Serializer.deserialize(ns1_pay$2d$refund$2d$it$2d$id_QNAME, reader, context);
                instance.setPayRefundItId(((Long)member).longValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_pay$2d$refund$2d$seller$2d$id_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_pay$2d$refund$2d$seller$2d$id_QNAME, reader, context);
                instance.setPayRefundSellerId(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_pay$2d$refund$2d$value_QNAME)) {
                member = ns2_myns2__float__float_Float_Serializer.deserialize(ns1_pay$2d$refund$2d$value_QNAME, reader, context);
                instance.setPayRefundValue(((Float)member).floatValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_pay$2d$refund$2d$reason_QNAME)) {
                member = ns2_myns2_string__java_lang_String_String_Serializer.deserialize(ns1_pay$2d$refund$2d$reason_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.UserPaymentRefundsStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myPAYREFUNDREASON_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setPayRefundReason((java.lang.String)member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_pay$2d$refund$2d$date_QNAME)) {
                member = ns2_myns2__long__long_Long_Serializer.deserialize(ns1_pay$2d$refund$2d$date_QNAME, reader, context);
                instance.setPayRefundDate(((Long)member).longValue());
                reader.nextElementContent();
                continue;
            } else {
                throw new DeserializationException("soap.unexpectedElementName", new Object[] {ns1_pay$2d$refund$2d$date_QNAME, elementName});
            }
        }
        
        XMLReaderUtil.verifyReaderState(reader, XMLReader.END);
        return (isComplete ? (java.lang.Object)instance : (java.lang.Object)state);
    }
    
    public void doSerializeAttributes(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws java.lang.Exception {
        AllegroWebApi.UserPaymentRefundsStruct instance = (AllegroWebApi.UserPaymentRefundsStruct)obj;
        
    }
    
    public void doSerializeInstance(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws java.lang.Exception {
        AllegroWebApi.UserPaymentRefundsStruct instance = (AllegroWebApi.UserPaymentRefundsStruct)obj;
        
        ns2_myns2__long__long_Long_Serializer.serialize(new Long(instance.getPayRefundTransId()), ns1_pay$2d$refund$2d$trans$2d$id_QNAME, null, writer, context);
        ns2_myns2__long__long_Long_Serializer.serialize(new Long(instance.getPayRefundItId()), ns1_pay$2d$refund$2d$it$2d$id_QNAME, null, writer, context);
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getPayRefundSellerId()), ns1_pay$2d$refund$2d$seller$2d$id_QNAME, null, writer, context);
        ns2_myns2__float__float_Float_Serializer.serialize(new Float(instance.getPayRefundValue()), ns1_pay$2d$refund$2d$value_QNAME, null, writer, context);
        ns2_myns2_string__java_lang_String_String_Serializer.serialize(instance.getPayRefundReason(), ns1_pay$2d$refund$2d$reason_QNAME, null, writer, context);
        ns2_myns2__long__long_Long_Serializer.serialize(new Long(instance.getPayRefundDate()), ns1_pay$2d$refund$2d$date_QNAME, null, writer, context);
    }
}
