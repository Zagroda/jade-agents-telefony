// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class AllegroWebApiPortType_doGetSellRatingReasons_ResponseStruct {
    protected AllegroWebApi.SellRatingInfoStruct[] sellRatingInfo;
    
    public AllegroWebApiPortType_doGetSellRatingReasons_ResponseStruct() {
    }
    
    public AllegroWebApiPortType_doGetSellRatingReasons_ResponseStruct(AllegroWebApi.SellRatingInfoStruct[] sellRatingInfo) {
        this.sellRatingInfo = sellRatingInfo;
    }
    
    public AllegroWebApi.SellRatingInfoStruct[] getSellRatingInfo() {
        return sellRatingInfo;
    }
    
    public void setSellRatingInfo(AllegroWebApi.SellRatingInfoStruct[] sellRatingInfo) {
        this.sellRatingInfo = sellRatingInfo;
    }
}
