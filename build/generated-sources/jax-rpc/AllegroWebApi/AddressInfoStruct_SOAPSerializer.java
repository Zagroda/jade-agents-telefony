// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;

import com.sun.xml.rpc.encoding.*;
import com.sun.xml.rpc.encoding.literal.DetailFragmentDeserializer;
import com.sun.xml.rpc.encoding.simpletype.*;
import com.sun.xml.rpc.encoding.soap.SOAPConstants;
import com.sun.xml.rpc.encoding.soap.SOAP12Constants;
import com.sun.xml.rpc.streaming.*;
import com.sun.xml.rpc.wsdl.document.schema.SchemaConstants;
import javax.xml.namespace.QName;

public class AddressInfoStruct_SOAPSerializer extends ObjectSerializerBase implements Initializable {
    private static final javax.xml.namespace.QName ns1_address$2d$type_QNAME = new QName("", "address-type");
    private static final javax.xml.namespace.QName ns2_int_TYPE_QNAME = SchemaConstants.QNAME_TYPE_INT;
    private CombinedSerializer ns2_myns2__int__int_Int_Serializer;
    private static final javax.xml.namespace.QName ns1_address$2d$user$2d$data_QNAME = new QName("", "address-user-data");
    private static final javax.xml.namespace.QName ns3_AddressUserDataStruct_TYPE_QNAME = new QName("urn:AllegroWebApi", "AddressUserDataStruct");
    private CombinedSerializer ns3_myAddressUserDataStruct_SOAPSerializer;
    private static final int myADDRESSTYPE_INDEX = 0;
    private static final int myADDRESSUSERDATA_INDEX = 1;
    
    public AddressInfoStruct_SOAPSerializer(QName type, boolean encodeType, boolean isNullable, String encodingStyle) {
        super(type, encodeType, isNullable, encodingStyle);
    }
    
    public void initialize(InternalTypeMappingRegistry registry) throws java.lang.Exception {
        ns2_myns2__int__int_Int_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, int.class, ns2_int_TYPE_QNAME);
        ns3_myAddressUserDataStruct_SOAPSerializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, AllegroWebApi.AddressUserDataStruct.class, ns3_AddressUserDataStruct_TYPE_QNAME);
    }
    
    public java.lang.Object doDeserialize(SOAPDeserializationState state, XMLReader reader,
        SOAPDeserializationContext context) throws java.lang.Exception {
        AllegroWebApi.AddressInfoStruct instance = new AllegroWebApi.AddressInfoStruct();
        AllegroWebApi.AddressInfoStruct_SOAPBuilder builder = null;
        java.lang.Object member;
        boolean isComplete = true;
        javax.xml.namespace.QName elementName;
        
        reader.nextElementContent();
        for (int i=0; i<2; i++) {
            elementName = reader.getName();
            if (reader.getState() == XMLReader.END) {
                break;
            }
            if (elementName.equals(ns1_address$2d$type_QNAME)) {
                member = ns2_myns2__int__int_Int_Serializer.deserialize(ns1_address$2d$type_QNAME, reader, context);
                instance.setAddressType(((java.lang.Integer)member).intValue());
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_address$2d$user$2d$data_QNAME)) {
                member = ns3_myAddressUserDataStruct_SOAPSerializer.deserialize(ns1_address$2d$user$2d$data_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new AllegroWebApi.AddressInfoStruct_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myADDRESSUSERDATA_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setAddressUserData((AllegroWebApi.AddressUserDataStruct)member);
                }
                reader.nextElementContent();
                continue;
            } else {
                throw new DeserializationException("soap.unexpectedElementName", new Object[] {ns1_address$2d$user$2d$data_QNAME, elementName});
            }
        }
        
        XMLReaderUtil.verifyReaderState(reader, XMLReader.END);
        return (isComplete ? (java.lang.Object)instance : (java.lang.Object)state);
    }
    
    public void doSerializeAttributes(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws java.lang.Exception {
        AllegroWebApi.AddressInfoStruct instance = (AllegroWebApi.AddressInfoStruct)obj;
        
    }
    
    public void doSerializeInstance(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws java.lang.Exception {
        AllegroWebApi.AddressInfoStruct instance = (AllegroWebApi.AddressInfoStruct)obj;
        
        ns2_myns2__int__int_Int_Serializer.serialize(new java.lang.Integer(instance.getAddressType()), ns1_address$2d$type_QNAME, null, writer, context);
        ns3_myAddressUserDataStruct_SOAPSerializer.serialize(instance.getAddressUserData(), ns1_address$2d$user$2d$data_QNAME, null, writer, context);
    }
}
