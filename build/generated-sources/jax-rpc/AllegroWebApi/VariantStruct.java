// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class VariantStruct {
    protected int fid;
    protected AllegroWebApi.VariantQuantityStruct[] quantities;
    
    public VariantStruct() {
    }
    
    public VariantStruct(int fid, AllegroWebApi.VariantQuantityStruct[] quantities) {
        this.fid = fid;
        this.quantities = quantities;
    }
    
    public int getFid() {
        return fid;
    }
    
    public void setFid(int fid) {
        this.fid = fid;
    }
    
    public AllegroWebApi.VariantQuantityStruct[] getQuantities() {
        return quantities;
    }
    
    public void setQuantities(AllegroWebApi.VariantQuantityStruct[] quantities) {
        this.quantities = quantities;
    }
}
