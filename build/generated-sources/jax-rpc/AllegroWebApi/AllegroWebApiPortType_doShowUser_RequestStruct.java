// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class AllegroWebApiPortType_doShowUser_RequestStruct {
    protected java.lang.String webapiKey;
    protected int countryId;
    protected long userId;
    protected java.lang.String userLogin;
    
    public AllegroWebApiPortType_doShowUser_RequestStruct() {
    }
    
    public AllegroWebApiPortType_doShowUser_RequestStruct(java.lang.String webapiKey, int countryId, long userId, java.lang.String userLogin) {
        this.webapiKey = webapiKey;
        this.countryId = countryId;
        this.userId = userId;
        this.userLogin = userLogin;
    }
    
    public java.lang.String getWebapiKey() {
        return webapiKey;
    }
    
    public void setWebapiKey(java.lang.String webapiKey) {
        this.webapiKey = webapiKey;
    }
    
    public int getCountryId() {
        return countryId;
    }
    
    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }
    
    public long getUserId() {
        return userId;
    }
    
    public void setUserId(long userId) {
        this.userId = userId;
    }
    
    public java.lang.String getUserLogin() {
        return userLogin;
    }
    
    public void setUserLogin(java.lang.String userLogin) {
        this.userLogin = userLogin;
    }
}
