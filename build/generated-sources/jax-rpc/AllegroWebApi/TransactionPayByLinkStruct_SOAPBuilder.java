// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;

import com.sun.xml.rpc.encoding.*;
import com.sun.xml.rpc.util.exception.LocalizableExceptionAdapter;

public class TransactionPayByLinkStruct_SOAPBuilder implements SOAPInstanceBuilder {
    private AllegroWebApi.TransactionPayByLinkStruct _instance;
    private java.lang.String actionHttpMethod;
    private java.lang.String actionUrl;
    private AllegroWebApi.ActionDataStruct[] actionData;
    private static final int myACTIONHTTPMETHOD_INDEX = 0;
    private static final int myACTIONURL_INDEX = 1;
    private static final int myACTIONDATA_INDEX = 2;
    
    public TransactionPayByLinkStruct_SOAPBuilder() {
    }
    
    public void setActionHttpMethod(java.lang.String actionHttpMethod) {
        this.actionHttpMethod = actionHttpMethod;
    }
    
    public void setActionUrl(java.lang.String actionUrl) {
        this.actionUrl = actionUrl;
    }
    
    public void setActionData(AllegroWebApi.ActionDataStruct[] actionData) {
        this.actionData = actionData;
    }
    
    public int memberGateType(int memberIndex) {
        switch (memberIndex) {
            case myACTIONHTTPMETHOD_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            case myACTIONURL_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            case myACTIONDATA_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            default:
                throw new IllegalArgumentException();
        }
    }
    
    public void construct() {
    }
    
    public void setMember(int index, java.lang.Object memberValue) {
        try {
            switch(index) {
                case myACTIONHTTPMETHOD_INDEX:
                    _instance.setActionHttpMethod((java.lang.String)memberValue);
                    break;
                case myACTIONURL_INDEX:
                    _instance.setActionUrl((java.lang.String)memberValue);
                    break;
                case myACTIONDATA_INDEX:
                    _instance.setActionData((AllegroWebApi.ActionDataStruct[])memberValue);
                    break;
                default:
                    throw new java.lang.IllegalArgumentException();
            }
        }
        catch (java.lang.RuntimeException e) {
            throw e;
        }
        catch (java.lang.Exception e) {
            throw new DeserializationException(new LocalizableExceptionAdapter(e));
        }
    }
    
    public void initialize() {
    }
    
    public void setInstance(java.lang.Object instance) {
        _instance = (AllegroWebApi.TransactionPayByLinkStruct)instance;
    }
    
    public java.lang.Object getInstance() {
        return _instance;
    }
}
