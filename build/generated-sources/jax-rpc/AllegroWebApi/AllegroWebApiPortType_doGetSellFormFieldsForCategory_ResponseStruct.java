// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class AllegroWebApiPortType_doGetSellFormFieldsForCategory_ResponseStruct {
    protected AllegroWebApi.SellFormFieldsForCategoryStruct sellFormFieldsForCategory;
    
    public AllegroWebApiPortType_doGetSellFormFieldsForCategory_ResponseStruct() {
    }
    
    public AllegroWebApiPortType_doGetSellFormFieldsForCategory_ResponseStruct(AllegroWebApi.SellFormFieldsForCategoryStruct sellFormFieldsForCategory) {
        this.sellFormFieldsForCategory = sellFormFieldsForCategory;
    }
    
    public AllegroWebApi.SellFormFieldsForCategoryStruct getSellFormFieldsForCategory() {
        return sellFormFieldsForCategory;
    }
    
    public void setSellFormFieldsForCategory(AllegroWebApi.SellFormFieldsForCategoryStruct sellFormFieldsForCategory) {
        this.sellFormFieldsForCategory = sellFormFieldsForCategory;
    }
}
