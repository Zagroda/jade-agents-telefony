// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class FeedbackManyStruct {
    protected long feItemId;
    protected int feUseCommentTemplate;
    protected int feToUserId;
    protected java.lang.String feComment;
    protected java.lang.String feCommentType;
    protected int feOp;
    protected AllegroWebApi.SellRatingEstimationStruct[] feRating;
    
    public FeedbackManyStruct() {
    }
    
    public FeedbackManyStruct(long feItemId, int feUseCommentTemplate, int feToUserId, java.lang.String feComment, java.lang.String feCommentType, int feOp, AllegroWebApi.SellRatingEstimationStruct[] feRating) {
        this.feItemId = feItemId;
        this.feUseCommentTemplate = feUseCommentTemplate;
        this.feToUserId = feToUserId;
        this.feComment = feComment;
        this.feCommentType = feCommentType;
        this.feOp = feOp;
        this.feRating = feRating;
    }
    
    public long getFeItemId() {
        return feItemId;
    }
    
    public void setFeItemId(long feItemId) {
        this.feItemId = feItemId;
    }
    
    public int getFeUseCommentTemplate() {
        return feUseCommentTemplate;
    }
    
    public void setFeUseCommentTemplate(int feUseCommentTemplate) {
        this.feUseCommentTemplate = feUseCommentTemplate;
    }
    
    public int getFeToUserId() {
        return feToUserId;
    }
    
    public void setFeToUserId(int feToUserId) {
        this.feToUserId = feToUserId;
    }
    
    public java.lang.String getFeComment() {
        return feComment;
    }
    
    public void setFeComment(java.lang.String feComment) {
        this.feComment = feComment;
    }
    
    public java.lang.String getFeCommentType() {
        return feCommentType;
    }
    
    public void setFeCommentType(java.lang.String feCommentType) {
        this.feCommentType = feCommentType;
    }
    
    public int getFeOp() {
        return feOp;
    }
    
    public void setFeOp(int feOp) {
        this.feOp = feOp;
    }
    
    public AllegroWebApi.SellRatingEstimationStruct[] getFeRating() {
        return feRating;
    }
    
    public void setFeRating(AllegroWebApi.SellRatingEstimationStruct[] feRating) {
        this.feRating = feRating;
    }
}
