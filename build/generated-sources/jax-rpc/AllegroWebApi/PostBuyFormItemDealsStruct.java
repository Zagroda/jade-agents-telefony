// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class PostBuyFormItemDealsStruct {
    protected long dealId;
    protected float dealFinalPrice;
    protected int dealQuantity;
    protected java.util.Calendar dealDate;
    protected boolean dealWasDiscounted;
    protected AllegroWebApi.PostBuyFormItemDealsVariantStruct dealVariant;
    
    public PostBuyFormItemDealsStruct() {
    }
    
    public PostBuyFormItemDealsStruct(long dealId, float dealFinalPrice, int dealQuantity, java.util.Calendar dealDate, boolean dealWasDiscounted, AllegroWebApi.PostBuyFormItemDealsVariantStruct dealVariant) {
        this.dealId = dealId;
        this.dealFinalPrice = dealFinalPrice;
        this.dealQuantity = dealQuantity;
        this.dealDate = dealDate;
        this.dealWasDiscounted = dealWasDiscounted;
        this.dealVariant = dealVariant;
    }
    
    public long getDealId() {
        return dealId;
    }
    
    public void setDealId(long dealId) {
        this.dealId = dealId;
    }
    
    public float getDealFinalPrice() {
        return dealFinalPrice;
    }
    
    public void setDealFinalPrice(float dealFinalPrice) {
        this.dealFinalPrice = dealFinalPrice;
    }
    
    public int getDealQuantity() {
        return dealQuantity;
    }
    
    public void setDealQuantity(int dealQuantity) {
        this.dealQuantity = dealQuantity;
    }
    
    public java.util.Calendar getDealDate() {
        return dealDate;
    }
    
    public void setDealDate(java.util.Calendar dealDate) {
        this.dealDate = dealDate;
    }
    
    public boolean isDealWasDiscounted() {
        return dealWasDiscounted;
    }
    
    public void setDealWasDiscounted(boolean dealWasDiscounted) {
        this.dealWasDiscounted = dealWasDiscounted;
    }
    
    public AllegroWebApi.PostBuyFormItemDealsVariantStruct getDealVariant() {
        return dealVariant;
    }
    
    public void setDealVariant(AllegroWebApi.PostBuyFormItemDealsVariantStruct dealVariant) {
        this.dealVariant = dealVariant;
    }
}
