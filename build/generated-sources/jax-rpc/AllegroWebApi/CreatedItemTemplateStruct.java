// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, build R1)
// Generated source version: 1.1.3

package AllegroWebApi;


public class CreatedItemTemplateStruct {
    protected int itemTemplateId;
    
    public CreatedItemTemplateStruct() {
    }
    
    public CreatedItemTemplateStruct(int itemTemplateId) {
        this.itemTemplateId = itemTemplateId;
    }
    
    public int getItemTemplateId() {
        return itemTemplateId;
    }
    
    public void setItemTemplateId(int itemTemplateId) {
        this.itemTemplateId = itemTemplateId;
    }
}
